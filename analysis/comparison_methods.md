---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Comparison different approximations

In this notebook we compare the results of the following methods for the same problem:
* The linear helmholtz solver ('lhh')
* The piecewise linear solver for the bulk ldos (step function) ('pi_bulk')
* The Newton-Raphson solver with lattice (i)ldos ('nr_lat')
* The full self consistent solver using Newton Raphson. A recalculation of the complete ldos is done in each iteration. ('nr_fsc')

```python
import pickle
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as c
from scipy.interpolate import interp1d
import warnings

from qpc_exp2sim.pescado_kwant import plotting_pes, extension_pes
import qpc_exp2sim.tools.data_handling as dh
%matplotlib notebook
```

## 0) Reading input data

```python
#file_geom = 'a4_grid10'
#files_sim = ['V64_a4_lhh', 'V64_a4_pi_bulk', 'V64_a4_nr_lat', 'V64_a4_fsc_nr']

qpc = 'a2'
file_geom = '{}_grid10'.format(qpc)
files_sim = [file.format(qpc) for file in 
             ['V128_{}_pi_bulk', 'V128_{}_nr_lat_m100_it5', 'V128_{}_fsc_nr_m100_it5']]

# files_sim = ['V3_lhh', 'V3_pi_bulk', 'V3_nr_lat', 'V3_fsc_nr_m100_it10']

files = [file_geom + '_' + file_sim for file_sim in files_sim]
labels = ['lhh', 'pi_bulk', 'nr_lat', 'nr_fsc']
```

```python
datas = []
coords = []
V_gates_lists= []
ldoss = []
voltages= []
charges = []
conductances = []

for file in files:
    data_elem = dh.read_file(file)
    datas.append(data_elem)
    
    # If there is no voltage/charge/ldos stored raise a warning
    if data_elem.voltage is None:
        voltages.append(None)
        charges.append(None)
        ldoss.append(None)
        warnings.warn("File {} doesn't have any voltage, charge or ldos stored.".format(file))
    
    # We only consider the last iteration if the iteration data is stored
    elif isinstance(data_elem.voltage[0], (list, np.ndarray)):
        voltages.append(data_elem.voltage[:,-1])
        charges.append(data_elem.charge[:,-1])
        ldoss.append(data_elem.ldos[:,-1])
    else:
        voltages.append(data_elem.voltage)
        charges.append(data_elem.charge)
        ldoss.append(data_elem.ldos)
    coords.append(data_elem.coord)
    conductances.append(data_elem.conductance)
    V_gates_lists.append(data_elem.p_sim['V_gates_list'])
    
V_gates_list = data_elem.p_sim['V_gates_list']
helmholtz_coef = data_elem.p_sim['helmholtz_coef']
conversion_unit = c.elementary_charge/data_elem.p_geom['t']
cell_area = data_elem.p_geom['grid'][0] * data_elem.p_geom['grid'][1] * 1e-18
index_v = 40
print('The gate voltage is {} V'.format(V_gates_list[index_v]))
```

## 1) LDOS and ILDOS 

```python
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10,7))


# Obtaining the correct ldos data
margin = 0.999
ldos_nr_lat = ldoss[-2]
bottom_band_nr_lat = (-ldos_nr_lat.a + ldos_nr_lat.b) * margin
top_band_nr_lat = (ldos_nr_lat.a + ldos_nr_lat.b)  * margin
energy_nr_lat = np.linspace(bottom_band_nr_lat, top_band_nr_lat, 1000)

ldos_nr_fsc = ldoss[-1][index_v]
bottom_band_nr_fsc = (-ldos_nr_fsc.a + ldos_nr_fsc.b) * margin
top_band_nr_fsc = (ldos_nr_fsc.a + ldos_nr_fsc.b) * margin
energy_nr_fsc = np.linspace(bottom_band_nr_fsc, top_band_nr_fsc, 1000)

# Linear helmholtz case
ldos_lhh = np.array([[-1, 1], [helmholtz_coef, helmholtz_coef]])
ildos_lhh = np.array([[-1, 0, 1], [-helmholtz_coef, 0, helmholtz_coef]])

ax1.plot(ldos_lhh[0], ldos_lhh[1])
ax2.plot(ildos_lhh[0], ildos_lhh[1])

# Piecewise bulk lattice case
ldos_pi_bulk = np.array([[-1, 0, 0, 1], [0, 0, helmholtz_coef, helmholtz_coef ]])
ildos_pi_bulk = np.array([[-1, 0, 1], [0, 0, helmholtz_coef]])

ax1.plot(ldos_pi_bulk[0], ldos_pi_bulk[1])
ax2.plot(ildos_pi_bulk[0], ildos_pi_bulk[1])

# Newton Raphson Lattice
ildos_nr_lat = 2 * ldos_nr_lat.integrate(energy_nr_lat)
ldos_nr_lat = 2 * conversion_unit * ldos_nr_lat(energy_nr_lat)
voltage_nr_lat = energy_nr_lat/conversion_unit
voltage_nr_lat = np.insert(voltage_nr_lat, 0, -1)
ildos_nr_lat = np.insert(ildos_nr_lat, 0, 0)

ax1.plot(voltage_nr_lat[1:], ldos_nr_lat)
ax2.plot(voltage_nr_lat, ildos_nr_lat)

# Newton Raphson Full Self Consistent
index = extension_pes.indices_from_coordinates(coords[-1], np.array([[0,0,0]]))[0]

assert (index in ldos_nr_fsc.indices), ('The asked index is not in the '
    'ExactLdos object, this might be due to the fact that the specific '
    'location is depleted for the asked gate voltega (index_v).')

ildos_nr_fsc = 2 * ldos_nr_fsc.integrate(energy_nr_fsc, index)
ldos_nr_fsc = 2 * conversion_unit * ldos_nr_fsc(energy_nr_fsc, index)
voltage_nr_fsc = energy_nr_fsc/conversion_unit + voltages[-1][index_v][index]
voltage_nr_fsc = np.insert(voltage_nr_fsc, 0, -1)
ildos_nr_fsc = np.insert(ildos_nr_fsc, 0, 0)

ax1.plot(voltage_nr_fsc[1:], ldos_nr_fsc)
ax2.plot(voltage_nr_fsc, ildos_nr_fsc)

ax1.set_xlim([-0.02, 0.05])
ax2.set_xlim([-0.02, 0.05])
ax3.set_xlim([-0.02, 0.05])
ax4.set_xlim([-0.02, 0.05])

ax2.set_ylim([-0.3, 2.1])

ax2.set_xlabel('voltage [V]')
ax2.set_ylabel('ildos')

# To make the difference between Newton Raphson Lattice and Full Self consistent
# we need to interpolate one of the 2 curves since they are not defined in the 
# same points
f_ldos_nr_fsc = interp1d(voltage_nr_fsc[1:], ldos_nr_fsc[:,0], 'cubic')
ax3.plot(voltage_nr_lat[1:], f_ldos_nr_fsc(voltage_nr_lat[1:]) - ldos_nr_lat[:,0])
ax3.set_ylabel(r'$\Delta$ldos')

f_ildos_nr_fsc = interp1d(voltage_nr_fsc, ildos_nr_fsc, 'cubic')
ax4.plot(voltage_nr_lat, f_ildos_nr_fsc(voltage_nr_lat) - ildos_nr_lat)
ax4.set_ylabel(r'$\Delta$ildos')

ax2.legend(labels)

plt.tight_layout()
plt.show()

```

## 2) Voltage and charge

```python
section = [None, 0, 0]

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=[10,7], gridspec_kw={'height_ratios': [1, 1]})

for voltage, charge, coord in zip(voltages, charges, coords):
    coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage[index_v], section=section)
    coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge[index_v], section=section)
    
    ax1.plot(coord_1D[:,0], voltage_1D[:,0,0])
    ax2.plot(coord_1D[:,0], charge_1D[:,0,0]/cell_area)
    
    label = ['x', 'y']
    n = section.index(None)
    m = (n + 1) % 2
    ax1.set_title('Voltage, {} = {} nm'.format(label[m], section[m]))
    ax2.set_title('Density, {} = {} nm'.format(label[m], section[m]))
    
    ax1.set_xlabel('{} [nm]'.format(label[n]))
    ax2.set_xlabel('{} [nm]'.format(label[n]))
    
    ax1.set_ylabel('voltage [V]')
    ax2.set_ylabel('n [/m²]')

ax2.legend(labels)

coord_1D_nr_lat, voltage_1D_nr_lat = plotting_pes.data_cross_section(coords[-2], voltages[-2][index_v], section=section)
coord_1D_nr_lat, charge_1D_nr_lat = plotting_pes.data_cross_section(coords[-2], charges[-2][index_v], section=section)

coord_1D_nr_fsc, voltage_1D_nr_fsc = plotting_pes.data_cross_section(coords[-1], voltages[-1][index_v], section=section)
coord_1D_nr_fsc, charge_1D_nr_fsc = plotting_pes.data_cross_section(coords[-1], charges[-1][index_v], section=section)

assert np.all(coord_1D_nr_fsc == coord_1D_nr_lat)

ax3.plot(coord_1D_nr_lat[:,0], (voltage_1D_nr_fsc - voltage_1D_nr_lat)[:,0,0], 'k', linewidth = 0.8)
ax4.plot(coord_1D_nr_lat[:,0], (charge_1D_nr_fsc - charge_1D_nr_lat)[:,0,0]/cell_area, 'k',  linewidth = 0.8)

label = ['x', 'y']
n = section.index(None)
m = (n + 1) % 2

ax3.set_xlabel('{} [nm]'.format(label[n]))
ax4.set_xlabel('{} [nm]'.format(label[n]))

ax3.set_ylabel(r'$\Delta$V [V]')
ax4.set_ylabel(r'$\Delta$n [/m²]')

plt.tight_layout()
plt.show()
```

## 3) Conductance
* Overall slope of pi bulk is a little bit higher than the one of nr_lat and nr_fsc
* nr_fsc and nr_lat seem to be just shifted a little bit in voltage?


## 3.1) Original conductance curves different methods

```python

```

```python
fig, ax = plt.subplots()

ax.plot([], [])
for i, (V_gates_list, conductance) in enumerate(zip(V_gates_lists, conductances)):
    ax.plot(V_gates_list, conductance, label = labels[i+1])
    
    # plotting_sim.linear_fit_conductance(V_gates_list, conductance.flatten())
    
ax.legend()
    
ax.set_xlabel(r'$V_g$ [V]')
ax.set_ylabel('conductance [2e²/h]')
plt.show()
```

## 3.2) Conductance curves different methods shifted with respect to pinch of voltage

```python
# Note: The resolution of the conductance ifo V_g should be good enough if a nice shift is to be obtained
fig, ax = plt.subplots()

ax.plot([], [])
for i, (V_gates_list, conductance) in enumerate(zip(V_gates_lists, conductances)):
    #print(datas[i].V3)
    ax.plot(np.array(V_gates_list) - datas[i].V3[1], conductance, label = labels[i+1])
ax.legend()
    
ax.set_xlabel(r'$V_g$ [V]')
ax.set_ylabel('conductance [2e²/h]')
plt.show()
```

## 3.3) Conductance different methods in comparison to experiment
See the file 'da_exp_1D.ipynb'
