Master thesis 

# Development of quantum simulation techniques for quantum nano-electronics

### A study of quantum point contacts

The overleaf project can be found using the following links for respectively editing and viewing:

[Overleaf project for editing](https://www.overleaf.com/9822854592mxvcjgvwbpmq) (will be deleted after submitting master thesis)

[Overleaf project for for viewing](https://www.overleaf.com/read/xgddzzrbntsd)
