---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from __future__ import print_function
from qpc_exp2sim.tools import plotting_sim_old as plotting_sim
from sympy import symbols, diff, linsolve, lambdify
from scipy import constants
import numpy as np
import matplotlib.pyplot as plt
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from types import SimpleNamespace
```

```python
# Input values
epsilon_r = 13

# Make sure this is loaded from a data file
d0_val = 30 * 1e-9                                   # Thickness GaAs layer bottom
d_2DEG_val = 10 * 1e-9                               # Thickness 2DEG 
d1_val = 30 * 1e-9                                   # Thickness AlGaAs above 2DEG
d2_val = 60 * 1e-9                                   # Thickness AlGaAs, Si doped
d3_val = 10 * 1e-9                                  # Thickness AlGaAs 
d4_val = 10 * 1e-9                                  # Thickness GaAs
d_contact_val = 10 * 1e-9 # Thickness contact layer

D1_val = d1_val + d_2DEG_val/2
Dd_val = d2_val
D2_val = d3_val + d4_val + d_contact_val/2

Nd_val = 2.8e15/Dd_val
nn_val = constants.elementary_charge * Nd_val / (2* constants.epsilon_0 * epsilon_r)
rho_val = constants.electron_mass * 0.067 / (constants.pi * constants.hbar**2)
rhorho_val = constants.elementary_charge **2 * rho_val / (constants.epsilon_0 * epsilon_r) 
```

```python
# Problem statement
z, nn, rhorho, A, B, C, D, E, F, D1, D2, Dd, Vg =  symbols('z nn rhorho A B C D E F D1, D2, Dd, Vg')

U1 = A*z + B
U2 = -nn * (z**2 + C*z + D)
U3 = E*z + F

# Setting the boundary conditions
eq = []
eq.append(U3.subs(z, D1 + Dd + D2) - Vg)
eq.append(diff(U1,z).subs(z, 0) - rhorho*U1.subs(z, 0))

eq.append(U1.subs(z, D1) - U2.subs(z,D1))
eq.append(diff(U1,z).subs(z,D1) - diff(U2,z).subs(z,D1))

eq.append(U2.subs(z,D1 + Dd) - U3.subs(z,D1 + Dd))
eq.append(diff(U2,z).subs(z,D1 + Dd) - diff(U3,z).subs(z,D1 + Dd))

# Solve the problem with boundary conditions:
sol = linsolve(eq, A,B,C,D,E,F)
display(sol)
```

```python
A_val = list(sol)[0][0]
B_val = list(sol)[0][1]
C_val = list(sol)[0][2]
D_val = list(sol)[0][3]
E_val = list(sol)[0][4]
F_val = list(sol)[0][5]
```

```python
substitution_set = {nn:nn_val, 
                    rhorho:rhorho_val, 
                    D1:D1_val, 
                    D2:D2_val, 
                    Dd:Dd_val}

sol_val = sol.subs(substitution_set)

A_val = list(sol_val)[0][0]
B_val = list(sol_val)[0][1]
C_val = list(sol_val)[0][2]
D_val = list(sol_val)[0][3]
E_val = list(sol_val)[0][4]
F_val = list(sol_val)[0][5]

subs_set = {A:A_val, B:B_val, C:C_val, D:D_val, E:E_val, F:F_val, nn:nn_val}

# display(A_val, B_val, C_val, D_val, E_val, F_val)

U1_val = lambdify([z, Vg], U1.subs(subs_set), 'numpy')
U2_val = lambdify([z, Vg], U2.subs(subs_set), 'numpy')
U3_val = lambdify([z, Vg], U3.subs(subs_set), 'numpy')
```

```python
import qpc_exp2sim.tools.data_handling as dh

data_po = dh.read_file('../../data_files/system_1D/po_fine_mesh')

p_geom_po = data_po.p_geom
p_sim_po = data_po.p_sim
V_gates_list_po = p_sim_po.V_gates_list
coord_po = data_po.coord
voltage_po = data_po.voltage
charge_po = data_po.charge
voltage_shift_po = data_po.voltage_shift
V3_po = data_po.V3
```

```python
V_gates_list_po = np.linspace(-2,0)
widget_v = widgets.Dropdown(options=V_gates_list_po, description ='V_gates:' )
widget_figsize = widgets.IntSlider(value=7, min=0, max=15, step=1, continuous_update=False, description='Figure size:')
```

```python
%matplotlib notebook
def f(V_gates, figsize):
    ######################## POISSON MODEL ################################
    z1 = np.linspace(0, d_2DEG_val/2 + d1_val)
    z2 = np.linspace(d_2DEG_val/2 + d1_val, d_2DEG_val/2 + d1_val + d2_val)
    z3 = np.linspace(d_2DEG_val/2 + d1_val + d2_val, d_2DEG_val/2 + d1_val + d2_val + d3_val +  d4_val  + d_contact_val/2)
    coord = np.hstack((z1, z2, z3))*1e9
    
    data_U1 = U1_val(z1, V_gates)
    data_U2 = U2_val(z2, V_gates)
    data_U3 = U3_val(z3, V_gates)
    voltage = np.hstack((data_U1, data_U2, data_U3))
    # print(voltage)
    
    # display(data_U1, data_U2, data_U3)

    vertical_lines = SimpleNamespace(d_2DEG = d_2DEG_val*1e9, 
                                    d1 = d1_val*1e9,
                                    d2 = d2_val*1e9,
                                    d3 = d3_val*1e9,
                                    d4 = d4_val*1e9)

    print(voltage.shape, coord.shape)
    # Plotting the data
    plotting_sim.plot_line(coord, voltage, setting_x = 2, setting_y = 0, p=vertical_lines, figsize=figsize, show=True)
    
    ######################## PESCADO 1D ################################
#     # Getting the sparse vectors containing the data for a certain gate voltage
#     index = np.where(V_gates_list_po == V_gates)[0][0]
#     voltage_sparse_vec = voltage_po[index]
#     charge_sparse_vec = charge_po[index]     
    
#     coord_1D, voltage_1D = plotting_sim.data_cross_section(coord_po, voltage_sparse_vec)
    
#     # Sort the data such that we don't get crossing lines through the figure
#     sort_indices = np.argsort(coord_1D[:,0])
#     coord_1D = coord_1D[sort_indices]
#     voltage_1D = voltage_1D[sort_indices]
        
#     #print("{:.2e}".format(np.max(charge_1D)), "{:.2e}".format(np.min(charge_1D)))
    
#     # Plotting the data
#     plotting_sim.plot_line(coord_1D, voltage_1D, setting=0, ax=2, p=p_geom_po, fig_size=figsize, show=False)
    
    
    ######################## PESCADO 3D ################################
#     # Getting the sparse vectors containing the data for a certain gate voltage
#     index = np.where(V_gates_list_3D == V_gates)[0][0]
#     voltage_sparse_vec = voltage_3D[index]
#     charge_sparse_vec = charge_3D[index]     
    
#     coord_1D, voltage_1D = plotting_sim.data_cross_section(coord_3D, voltage_sparse_vec)
    
#     # Sort the data such that we don't get crossing lines through the figure
#     sort_indices = np.argsort(coord_1D[:,0])
#     coord_1D = coord_1D[sort_indices]
#     voltage_1D = voltage_1D[sort_indices]
        
#     #print("{:.2e}".format(np.max(charge_1D)), "{:.2e}".format(np.min(charge_1D)))
    
#     # Plotting the data
#     plotting_sim.plot_line(coord_1D, voltage_1D, setting=0, ax=2, p=p_geom_po, fig_size=figsize, show=False)
#     plt.legend(['model', 'pescado 1D'])
#     plt.show()
    
    
    plt.legend(['model', 'pescado 1D'])
    plt.show()
    
    
    
interact(f, V_gates=widget_v, 
            figsize = widget_figsize)
```

```python

```

```python

```

```python

```

```python

```
