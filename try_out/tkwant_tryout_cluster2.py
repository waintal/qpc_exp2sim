#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 17 10:33:26 2022

@author: thellemans
"""
from mpi4py import MPI
import kwant
import tkwant
import numpy as np
import pickle

#-------------------- enable logging --------------------------------
#import logging
#tkwant.logging.level = logging.DEBUG
#--------------------------------------------------------------------

# Use default communicator. No need to complicate things.
comm = MPI.COMM_WORLD

def onsite(site, potential):
    return 4 - float(potential[tuple(site.pos)])


def onsite_lead_left(site, potential):
    """
    This function is necessary because kwant takes as sites for the lead sites
    in the center of the 2DEG
    """
    _,y = site.pos
    x = np.min(np.asarray(list(potential.keys()))[:,0])
    
    return 4 - float(potential[(x,y)])


def onsite_lead_right(site, potential):
    """
    This function is necessary because kwant takes as sites for the lead sites
    in the center of the 2DEG
    """
    _,y = site.pos
    x = np.max(np.asarray(list(potential.keys()))[:,0])
    
    return 4 - float(potential[(x,y)])


def make_kwant_system(grid=[20,20], L=1000, W=1000):
    
    # First, define the tight-binding system
    sys = kwant.Builder()

    # Here, we are only working with square lattices
    # This only works for 3D, or 2D cross sections xz, yz
    # Anyway, a 2D xy or 1D cross section is meaningless in this case
    lat = kwant.lattice.Monatomic(np.diag(grid), norbs=1)
    
    def scat_reg(pos):
        x,y = pos
        return -L/2 <= x <= L/2 and -W/2 <= y <= W/2
    
    ## On site hamiltonian
    sys[lat.shape(scat_reg, (0,0))] = onsite

    ## Hopping in x and y direction
    sys[lat.neighbors(1)] = -1

    # Adding the leads 
    ## Lead on the left    
    sym_left = kwant.TranslationalSymmetry((-grid[0], 0))
    lead_left = kwant.Builder(sym_left)
    # At the leads the voltage of the gates should have gone to zero but 
    # this can't be ensured: add  onsite potential at edge of scattering region
    lead_left[lat.shape(scat_reg, (0,0))] = onsite_lead_left
    lead_left[lat.neighbors()] = -1
    sys.attach_lead(lead_left)
    
    ## Lead on the right
    sym_right = kwant.TranslationalSymmetry((grid[0], 0))
    lead_right = kwant.Builder(sym_right)
    # At the leads the voltage of the gates should have gone to zero but 
    # this can't be ensured: add  onsite potential at edge of scattering region
    lead_right[lat.shape(scat_reg, (0,0))] = onsite_lead_right
    lead_right[lat.neighbors()] = -1
    sys.attach_lead(lead_right)

    # Finalize the system.
    sys = sys.finalized()

    return sys


syst = make_kwant_system()

print('Kwant system is made')

with open('potential_dict_grid20', 'rb') as inp: #'r' for reading, 'b' for 'binary'
    potential = pickle.load(inp)

print('Potential is loaded')

density_operator = kwant.operator.Density(syst)

occup = tkwant.manybody.lead_occupation(chemical_potential=0, temperature=0)
state = tkwant.manybody.State(syst, tmax=1, occupations=occup, comm=comm, 
                              params = {'potential':potential})
state.refine_intervals(rtol=1e-3, atol=1e-3)
density = state.evaluate(density_operator)
error = state.estimate_error()
print('Density at T=0K: ', density, ' with error ', error)
