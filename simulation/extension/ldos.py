#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 11:04:35 2022

@author: thellemans
"""
from mpi4py import MPI
import sys
import pickle
import scipy.constants as c
from qpc_exp2sim.simulation.solver import split_job, combine_job

# The inserted file should be a data file, for which the geometry file exists
# with an included kp_problem
comm = MPI.COMM_WORLD

if comm.rank == 0:
    file_data = sys.argv[1]
    print(file_data)

    with open(file_data, 'rb') as inp:
        data = pickle.load(inp)
        
    with open(data.file_geom, 'rb') as inp:
        _, pk_problem = pickle.load(inp)

    potential = data.voltage * (c.elementary_charge/pk_problem.t)

    potential_job, energy_job = split_job(potential, data.p_sim['energy_list'], 
                                          comm.size)
    
    print('Calculating kwant ldos')
else:
    potential_job = None
    energy_job = None
    pk_problem = None
    
pk_problem = comm.bcast(pk_problem, root=0)
potential_job = comm.scatter(potential_job, root=0)
energy_job = comm.scatter(energy_job, root=0)
    
ldos_kwant = pk_problem.ldos_kwant(potential = potential_job, 
                                   energy = energy_job)

ldos_kwant = comm.gather(ldos_kwant)

if comm.rank == 0:
    ldos_kwant = combine_job(ldos_kwant, potential, 
                           data.p_sim['energy_list'], comm.size, True)
    
    data.ldos_kwant = ldos_kwant
    
    with open(file_data, 'wb') as outp:
        pickle.dump(data, outp)
    
    print('successful')