#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:23:40 2022

@author: thellemans
"""

import numpy as np
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.simulation import solver, builder
import scipy.constants as c
from mpi4py import MPI
import sys


# Setting the default for serialization to dill instead of pickle
# This is still necesarry for the kwant_sys (onsite potential function)
import dill
MPI.pickle.__init__(dill.dumps, dill.loads)

# Use default communicator. No need to complicate things.
COMM = MPI.COMM_WORLD

# Collect whatever has to be done in a list. Here we'll just collect a list of
# numbers. Only the first rank has to do this.
# We also perform the calibration since we can do this once for all ranks
if COMM.rank == 0:
    data = dh.read_file(sys.argv[1], cluster=True)
    
    # unpacking the data
    p_geom = data.p_geom
    p_sim = data.p_sim
    coord = data.coord
    voltage = data.voltage
    p_geom.calibration_device = False
        
    print(sys.argv[1])
    print(p_geom.__dict__)
    print(p_sim.__dict__)
    
    _, kwant_sys = builder.problem_builder(p_geom, pescado_sim=False)
    voltage = np.array_split(voltage, COMM.size)
else:
    coord = None
    kwant_sys = None
    voltage = None

# Broadcast the necessary variables to the other ranks
coord, kwant_sys, voltage = COMM.bcast((coord, kwant_sys, voltage), root=0)

# Scatter jobs across the ranks.
voltage = COMM.scatter(voltage, root=0)
print('V_gates_list rank {}: {}'.format(COMM.rank, voltage))

#Vsd = 500e-6
#Esd = Vsd * c.elementary_charge
#energy_list = np.linspace(-Esd/2, Esd/2, 3)
energy_list = np.linspace(-2.5,7.5,1001)

conductance = solver.conductance_solver(coord, kwant_sys, voltage, energy_list)

print('Conductance rank {}: {}'.format(COMM.rank, conductance))

# Gather results on rank 0.
conductance = MPI.COMM_WORLD.gather(conductance, root=0)


if COMM.rank == 0:
    conductance = np.concatenate(conductance)
    
    data.conductance = conductance
    data.p_sim.energy_list = energy_list
    
    # Write the necessary output to a file that can be interpreted later on
    input_file = sys.argv[1][sys.argv[1].rfind('/')+1:]
    dh.write_file(data, 'data_files/' + input_file, cluster=True)#'_grid{}'.format(p_geom.grid_fine[0]))
