# -*- coding: utf-8 -*-
"""
Created on Tue May 10 22:56:13 2022

@author: helle
"""
import kwant
import tkwant
import numpy as np
from scipy import integrate, constants as c
import matplotlib.pyplot as plt
from qpc_exp2sim.tools import plotting_sim, data_handling as dh
from qpc_exp2sim.simulation import builder, solver
import time

from mpi4py import MPI

import dill
MPI.pickle.__init__(dill.dumps, dill.loads)

from mpi4py import MPI

#-------------------- enable logging --------------------------------
#import logging
#tkwant.logging.level = logging.DEBUG
#--------------------------------------------------------------------

# Use default communicator. No need to complicate things.
comm = MPI.COMM_WORLD

syst = kwant.Builder()

# Here, we are only working with square lattices
a = 1
lat = kwant.lattice.square(a, norbs=1)

t = 1.0
W = 10
L = 10
pot= 2.1

# Define the scattering region

for i in range(L):
    for j in range(W):
        # On-site Hamiltonian
        syst[lat(i, j)] = 4 * t - pot

        # Hopping in y-direction
        if j > 0:
            syst[lat(i, j), lat(i, j - 1)] = -t

        # Hopping in x-direction
        if i > 0:
            syst[lat(i, j), lat(i - 1, j)] = -t

# Then, define and attach the leads:

# First the lead to the left
# (Note: TranslationalSymmetry takes a real-space vector)
sym_left_lead = kwant.TranslationalSymmetry((-a, 0))
left_lead = kwant.Builder(sym_left_lead)

for j in range(W):
    left_lead[lat(0, j)] = 4 * t-pot
    if j > 0:
        left_lead[lat(0, j), lat(0, j - 1)] = -t
    left_lead[lat(1, j), lat(0, j)] = -t

syst.attach_lead(left_lead)

# Then the lead to the right
sym_right_lead = kwant.TranslationalSymmetry((a, 0))
right_lead = kwant.Builder(sym_right_lead)

for j in range(W):
    right_lead[lat(0, j)] = 4 * t-pot
    if j > 0:
        right_lead[lat(0, j), lat(0, j - 1)] = -t
    right_lead[lat(1, j), lat(0, j)] = -t

syst.attach_lead(right_lead)

# Plot it, to make sure it's OK
kwant.plot(syst)

# Finalize the system
syst = syst.finalized()


density_operator = kwant.operator.Density(syst)
chemical_potential=0 
T=0
occup = tkwant.manybody.lead_occupation(chemical_potential=0, temperature=0)
state = tkwant.manybody.State(syst, tmax=1, occupations=occup, comm=comm)
state.refine_intervals(rtol=1e-3, atol=1e-3)
density = state.evaluate(density_operator)
error = state.estimate_error()
print('Density at T=0K: ', density, ' with error ', error)
