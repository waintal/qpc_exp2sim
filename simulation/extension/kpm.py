#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 25 17:09:03 2022

@author: thellemans
"""
from mpi4py import MPI
import sys
import pickle
import scipy.constants as c
from qpc_exp2sim.simulation.solver import split_job, combine_job

# The inserted file should be a data file, for which the geometry file exists
# with an included kp_problem
comm = MPI.COMM_WORLD

if comm.rank == 0:
    file_data = sys.argv[1]
    print(file_data)
    
    with open(file_data, 'rb') as inp:
        data = pickle.load(inp)
        
    with open(data.file_geom, 'rb') as inp:
        _, pk_problem = pickle.load(inp)

    potential = data.voltage * (c.elementary_charge/pk_problem.t)

    potential_job, energy_job = split_job(potential, data.p_sim['energy_list'], 
                                          comm.size)
    
    print('Calculating kpm dens & kpm ldos')
else:
    potential_job = None
    energy_job = None
    pk_problem = None
    
pk_problem = comm.bcast(pk_problem, root=0)
potential_job = comm.scatter(potential_job, root=0)
energy_job = comm.scatter(energy_job, root=0)
    
dens_kpm, ldos_kpm = pk_problem.density_kpm(potential = potential_job, 
                                            energy = energy_job, 
                                            num_vectors=200, num_moments=100, 
                                            mu=0, temperature=0, local=True)

dens_kpm = comm.gather(dens_kpm)
ldos_kpm = comm.gather(ldos_kpm)

if comm.rank == 0:

    dens_kpm = combine_job(dens_kpm, potential, 
                           data.p_sim['energy_list'], comm.size, False)
    ldos_kpm = combine_job(ldos_kpm, potential, 
                           data.p_sim['energy_list'], comm.size, True)
    
    data.dens_kpm = dens_kpm
    data.ldos_kpm = ldos_kpm
    
    with open(file_data, 'wb') as outp:
        pickle.dump(data, outp)
