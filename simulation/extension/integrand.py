# -*- coding: utf-8 -*-
"""
Created on Tue May 17 17:10:23 2022

@author: helle
"""
from mpi4py import MPI
import kwant
import tkwant
import numpy as np
from scipy import constants as c
from qpc_exp2sim.tools import data_handling as dh
from qpc_exp2sim.simulation import builder
import sys


# Setting the default for serialization to dill instead of pickle
# This is still necesarry for the kwant_sys (onsite potential function)
import dill
MPI.pickle.__init__(dill.dumps, dill.loads)

# Use default communicator. No need to complicate things.
comm = MPI.COMM_WORLD

# Read the data file & recreate the kwant system on rank 0
if comm.rank == 0:

    data = dh.read_file(sys.argv[1], cluster=True)
    
    # unpacking the data
    p_geom = data.p_geom
    p_sim = data.p_sim
    coord = data.coord
    voltage = data.voltage
        
    print(sys.argv[1])
    print(p_geom.__dict__)
    print(p_sim.__dict__)
    
    kwant_sys = builder.make_kwant_system(p_geom)
    
    density_operator = kwant.operator.Density(kwant_sys)
    occup = tkwant.manybody.lead_occupation(chemical_potential=0, temperature=0) 
    
    # Get the kwnat_indices in rpp_problem format
    kwant_indices = builder.get_kwant_indices_rpp(kwant_sys, coord)
    
    t=p_geom.t
else:
    voltage = None
    kwant_sys = None
    density_operator = None
    occup = None
    kwant_indices = None
    t = None
    
# Broadcast the necessary variables to the other ranks:
t, voltage, kwant_sys, density_operator, occup, kwant_indices = comm.bcast(
        (t, voltage, kwant_sys, density_operator, occup, kwant_indices), root=0)

# define the momentum range in between which the integrand will be analyzed
kmin = 0.01
kmax = np.pi
momenta = np.linspace(kmin, kmax, 100)

band_energies = []
integrand_values_v = []

if comm.rank == 0:
    print('for loop starts')

for index_v, voltage_elem in enumerate(voltage):
    if comm.rank == 0:
        print('Working on voltage {} form {}'.format(index_v+1, len(voltage)))
    potential_elem = voltage_elem * (c.elementary_charge/t)
    
    integrand_values_vl = []
    for lead in range(len(kwant_sys.leads)):
        if comm.rank == 0:
            print('Calculating band structure for lead {}'.format(lead))

        bands = kwant.physics.Bands(kwant_sys.leads[lead], 
                                    params={'potential':potential_elem, 
                                            'kwant_indices':kwant_indices})   
        
        band_energies_vl = np.array([bands(k) for k in momenta])
        
        integrand_values_vlb = []
        for band in range(band_energies_vl.shape[1]):
            if comm.rank == 0:
                print('Calculating integrand for lead {}, band {}'.format(lead, band))
            # specify the lead and band index
            interval = tkwant.manybody.Interval(lead=lead, band=band, 
                                                kmin=kmin, kmax=kmax)

            # define a time where to evaluate the integrand
            integrand = tkwant.manybody.ManybodyIntegrand(kwant_sys, interval, 
                density_operator, time=0, params={'potential':potential_elem, 
                                                  'kwant_indices':kwant_indices})
            
            integrand_values_vlb.append(integrand.vecfunc(momenta))
            
        integrand_values_vl.append(integrand_values_vlb)
    
    integrand_values_v.append(integrand_values_vl)
    band_energies.append(band_energies_vl)
    
if comm.rank == 0:
    data.integrand = integrand_values_v
    data.bands = band_energies
    # Write the necessary output to a file that can be interpreted later on
    input_file = sys.argv[1][sys.argv[1].rfind('/')+1:]
    dh.write_file(data, 'data_files/' + input_file +'_integrand', cluster=True)