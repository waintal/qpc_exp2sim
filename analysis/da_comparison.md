---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.tools import plotting_sim
from qpc_exp2sim.simulation import solver
from qpc_exp2sim.pescado_kwant import plotting_pes
import numpy as np
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
from types import SimpleNamespace
%matplotlib notebook

files_a = ['{}_grid10_V128_full_pi_bulk'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_a_fine = ['{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_b = ['{}_grid10_V128_full_pi_bulk'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]
files_b_fine = ['{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]
```

## Influence grid

```python
files = ['b3_grid{}_V128_b3_pi_bulk'.format(grid) for grid in [6,8,10,12,15,20]]

conductance = []
V_gates_list = []
labels = []

fig, ax = plt.subplots()

for file in files:
    data = dh.read_file(file)
    V_gates_list.append(data.p_sim['V_gates_list'])
    conductance.append(data.conductance)
    labels.append('{} nm'.format(data.p_geom['grid'][0]))

plotting_sim.plot_conductance(V_gates_list, conductance, label = labels, linestyle='-', 
                 hlines=np.arange(6), ax=ax)

plt.show()
```

# Overview conductance for small qpc's

```python
for files in [files_a, files_b, files_a_fine, files_b_fine]:
    conductance = []
    V_gates_list = []
    labels = []

    for file in files:
        data = dh.read_file(file)
        V_gates_list.append(data.p_sim['V_gates_list'])
        conductance.append(data.conductance)
        labels.append(data.p_geom['qpc_name'])

    plotting_sim.plot_conductance(V_gates_list, conductance, linestyle='-', label=labels,
                                  hlines=np.arange(7), limits_v=None)
```

```python
for files in [files_a_fine, files_b_fine]:
    conductance = []
    V_gates_list = []
    labels = []

    for file in files:
        data = dh.read_file(file)
        V_gates_list.append(data.p_sim['V_gates_list'] - data.V3[0][0])
        conductance.append(data.conductance)
        labels.append(data.p_geom['qpc_name'])

    plotting_sim.plot_conductance(V_gates_list, conductance, linestyle='.-', label=labels,
                     hlines=np.arange(6), limits_v=None)
```

## Comparison V3 simulations and experiments

```python
plotting_sim.plot_V3([files_a_fine, files_b_fine])
```

```python
import scipy.constants as c
wx = 1e15
wy = 3e15
V0 = 0
V = np.linspace(0,7,100)
n = 0

en = 2 * (c.elementary_charge*(V-V0) -c.hbar * wy * (n+1/2)) / (c.hbar*wx)
T = 1/(1+ np.exp(-c.pi * en))

plt.figure()
plt.plot(c.elementary_charge*(V-V0)/(c.hbar*wx),T)
plt.show()
```

```python
c.hbar * wy * (n+1/2)
```

```python
c.elementary_charge*(V-V0)
```

```python
np.exp(1)
```

```python
300/12
```


