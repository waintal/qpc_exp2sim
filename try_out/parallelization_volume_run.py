# -*- coding: utf-8 -*-
"""
Created on Sun Jun  5 19:32:06 2022

@author: helle
"""
from mpi4py import MPI
import numpy as np
from pescado import poisson
from pescado.mesher import shapes, patterns
from qpc_exp2sim.pescado_kwant.extension_pes import Mesh_copy, volume, finalized


if __name__ == '__main__':
    ############## 0. Define the geometry parameters ###############
    
    system_size, bottom_gate, radius = [100, 50, 30]
    
    ############## 1. Define the geometry regions ###############
    
    circ_shape = shapes.General(
        func=lambda x: np.sum(x ** 2, axis=1) <= (radius ** 2),
        bbox=np.array([[-1.1, -1.1], [1.1, 1.1]]) * radius)
    
    gate = shapes.Box(
        lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, 6])
    
    space = shapes.Box(
        lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, ] * 2)
    
    ############## 2. Define the meshing pattern ###############
    
    coarse_pattern = patterns.Rectangular.constant(element_size=[2, 2])
    fine_pattern = patterns.Rectangular.constant(element_size=[1,1])
    
    ############## 3. Build the Poisson Problem ###############
    
    pp_builder = poisson.PoissonProblemBuilder()
    
    pp_builder.add_sub_mesh(shape_inst=space, pattern_inst=coarse_pattern)
    pp_builder.refine(shape_inst=gate, pattern_inst=fine_pattern)
    
    ## Set the site types (Neumann by default)
    pp_builder.set_dirichlet(shape=gate, setup_tag='gate')
    
    # Add linear dependance on the potential
    pp_builder.set_helmholtz(shape=circ_shape, setup_tag='circ') # Set the sites to Helmhotlz
    
    # pp_problem = pp_builder.finalized(parallelize=True)
    
    # pp_problem = finalized(pp_builder, parallelize = 'fork')
    
    comm = MPI.COMM_WORLD
    pp_problem = finalized(pp_builder, parallelize = 'mpi', comm=comm)
    # print('Reference volume:', pp_problem.volume)
    
    # mesh = Mesh_copy(pp_problem.mesh_inst.sub_meshes, 
    #                  pp_problem.mesh_inst.neighbours)
    
    # volume_fork = volume(mesh_inst = mesh,
    #     indices=np.arange(mesh.npoints), parallelize='fork')
    
    # print(volume_fork)
    
    # comm = MPI.COMM_WORLD
    # volume_mpi = volume(mesh_inst = mesh,
    #     indices=np.arange(mesh.npoints), parallelize='mpi', comm=comm)
    
    # print(volume_mpi)
    
    # print(np.all(pp_problem.volume == volume_fork))
    # print(np.all(pp_problem.volume == volume_mpi))