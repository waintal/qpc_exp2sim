# -*- coding: utf-8 -*-
"""
Created on Sat May 21 11:31:35 2022

@author: helle
"""

import numpy as np
import scipy.constants as c
import kwant
from copy import deepcopy
    
from pescado import poisson
from pescado.mesher import shapes, patterns

from .QPC_geometry import qpc
from ..pescado_kwant.extension_pes import reflect #, add_kwant_system
import tinyarray as ta


def gates_shape(W_pes, L_narrow_gate, qpc_center, qpc_name, d_contact, d_up, 
                **kwargs):    
    # Create the upper narrow gate by using a rectangular (Box) shape
    narrow_gate_up = shapes.Box(lower_left=[qpc_center[0]-L_narrow_gate/2, 
                                            qpc_center[1], d_up-d_contact], 
                                size=[L_narrow_gate, W_pes/2 - qpc_center[1], 
                                      d_contact])
    
    # Create the quantum point contact by using an circle (Ellipsoid)
    qpc_shape = qpc(qpc_name, 
                center = qpc_center, 
                interval=[d_up-d_contact, d_up])
    
    # The full upper gate is the union of gate_up, narrow_gate_up, and qpc
    gate_up_shape = narrow_gate_up | qpc_shape
    
    # Use 2 reflections to define the down gate
    gate_down_shape = reflect(gate_up_shape, [0,0,0], [1,0,0])
    gate_down_shape = reflect(gate_down_shape, [0,0,0], [0,1,0])
    
    shape = gate_up_shape | gate_down_shape

    return shape


def test_gates_shape(W_pes, L_narrow_gate, qpc_center, d_contact, d_up, **kwargs):
    # Create the upper narrow gate by using a rectangular (Box) shape
    gate_up = shapes.Box(lower_left=[qpc_center[0]-L_narrow_gate/2, 
                                     qpc_center[1], d_up-d_contact], 
                                size=[L_narrow_gate, W_pes/2 - qpc_center[1], 
                                      d_contact])
    
    gate_down = reflect(gate_up, [0,0,0], [0,1,0])
    
    return gate_up | gate_down


def layer0_shape(L_pes, W_pes, d0, d_2DEG, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, -d_2DEG/2 - d0], 
                      size=[L_pes, W_pes, d0])
    return shape

    
def DEG_shape(L_pes, W_pes, d_2DEG, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, -d_2DEG/2], 
                      size=[L_pes, W_pes, d_2DEG])
    return shape
    
def layer1_shape(L_pes, W_pes, d_2DEG, d1, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, d_2DEG/2], 
                      size=[L_pes, W_pes, d1])
    return shape
   
def layer2_shape(L_pes, W_pes, d_2DEG, d1, d2, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, d_2DEG/2+d1], 
                      size=[L_pes, W_pes, d2])
    return shape
   
def layer3_shape(L_pes, W_pes, d_2DEG, d1, d2, d3, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, d_2DEG/2+d1+d2], 
                      size=[L_pes, W_pes, d3])
    return shape
    

def layer4_shape(L_pes, W_pes, d_2DEG, d1, d2, d3, d4, **kwargs):
    shape = shapes.Box(lower_left=[-L_pes/2, -W_pes/2, d_2DEG/2+d1+d2+d3], 
                      size=[L_pes, W_pes, d4])
    return shape


def mesh_points(interfaces, zones, w, size):
    """
    Parameters
    ----------
    
    interfaces: list or np.ndarray of floats
        Location where the interfaces have to be
    
    zones : list or np.ndarray of bools
        Specify whether there can be only a single point (True) in the zone or 
        more (False). No correct result is given when single point zones with 
        different sizes are next to each other.
        The zones next to a single point zone should have mathematically at 
        least half of the size of the single point zone but a larger margin 
        is recommended.
    
    w : float
        Number that specifies the distance between to voronoi points around an 
        interface if the voronoi points around the interface can be chosen 
        freely (no limitations from single point zones). 
        e.g.:
                    point       interface     point
                      x             |           x
                      <-------------------------> = w
    size : float
        size of a voronoi cell (guideline, not strictly respected)
        
    Returns
    -------
    mesh_points : np.ndarray
        Location of the necessary mesh points that respect the interfaces
        
    info : np.ndarray
        In column 0: a reference point
        In column 1: the periodicity
    """
    # w en size moeten kleiner zijn dan de helft van de kleinste multi point 
    # zone
    zone_size = np.diff(interfaces)
    assert np.all(zone_size[np.logical_not(zones)] >= max(w*2, size*2)), (
        'w & min_size need to be smaller than half the size of the smallest '
        'multi point zone')
    
    mesh_points = np.array([])
    info = np.empty((len(zones), 2))
    
    # First handle the zones in which there may only be one mesh point
    single_point_zones = np.where(zones)[0]
    for zone in single_point_zones:
        dx = interfaces[zone+1] - interfaces[zone]
        if zone == 0:
            new_points = np.array([interfaces[zone] + dx/2, 
                                   interfaces[zone+1] + dx/2])
        elif zone == len(zones)-1:
            new_points = np.array([interfaces[zone] -dx/2, 
                                   interfaces[zone] + dx/2])
        else:        
            new_points = np.array([interfaces[zone] -dx/2, 
                                   interfaces[zone] + dx/2, 
                                   interfaces[zone+1] + dx/2])
        mesh_points = np.append(mesh_points, new_points)
        info[zone] = np.array([interfaces[zone] + dx/2, dx])
        
    # Unique is necessary since several single point zones with the same size 
    # may be laying next to each other
    mesh_points = np.unique(mesh_points)
    
    # Add mesh points around the other interfaces
    multi_point_zones = np.where(np.logical_not(zones))[0]
    
    for zone in set(multi_point_zones) - {0}:
        if zone - 1 in multi_point_zones:
            new_points = np.array([interfaces[zone] - w/2, 
                                   interfaces[zone] + w/2])
            mesh_points = np.append(mesh_points, new_points)
    
    mesh_points = np.sort(mesh_points)
    mesh_points2 = mesh_points
        
    # Fill the multi point zones up
    for zone in set(multi_point_zones):
        zone_boundaries = [interfaces[zone], interfaces[zone+1]]
        
        limitation_l = np.where(np.all(
            [interfaces[zone] < mesh_points, 
             mesh_points<interfaces[zone+1] - zone_size[zone]/2], axis=0))[0]
        
        limitation_r = np.where(np.all(
            [interfaces[zone] + zone_size[zone]/2 < mesh_points, 
             mesh_points < interfaces[zone+1]], axis=0))[0]    

        
        boundary_l = max(np.hstack([mesh_points[limitation_l], 
                                    zone_boundaries[0]]))
        boundary_r = min(np.hstack([mesh_points[limitation_r], 
                                    zone_boundaries[1]]))
        
        # Distance between the 2 mesh points bounding the zone
        d = boundary_r - boundary_l
        
        # Number of points that should be added
        n = int(d/size) - 1
        
        if n == 0:
            if d > size:
                spacing = (boundary_r - boundary_l)/2
                new_points = np.array([boundary_l + spacing])
            else:
                spacing = boundary_r - boundary_l
                new_points = np.array([])
        elif n == 1:
            spacing = (boundary_r - boundary_l)/2
            new_points = np.array([boundary_l + spacing])
        else:
            spacing = d/(n+1)
            new_points = np.arange(boundary_l+spacing, boundary_r, spacing)
        
        mesh_points2 = np.append(mesh_points2, new_points)
        
        ref_point = boundary_l if zone != 0 else boundary_r
        info[zone] = np.array([ref_point, spacing])
    
    return (np.sort(mesh_points2), info)


def simplify_zones(info):
    zone = 0
    zone_tracker = [[zone] for zone in range(len(info))]
    while zone < len(info)-1:
        if info[zone, 1] == info[zone + 1, 1]:
            info = np.delete(info, zone+1, axis=0)
            zone_tracker[zone].append(zone_tracker[zone+1][0])
            del zone_tracker[zone+1]
        else:
            zone += 1
    return info, zone_tracker


def _union_box_shapes(shapes_list):
    """ Union of box shapes, returns a single box
    """
    total_bbox = shapes.add_bbox([shape.bbox for shape in shapes_list])
    size = np.diff(total_bbox, axis=0)[0]
    return shapes.Box(lower_left = total_bbox[0], 
                      size = size)


def union(shapes_list, index):
    """Union of a list of shapes
    
    shapes_list : sequence of shapes
        The shape on the last position may be another Union instance, 
        The shape on the other positions should be of type shapes.Box
        and should only differ in one dimension.
    
    index : list, np.ndarray
        The indices of the shapes in the list for which the union has to be made
    """
    # The gates have the only special shape, they are treated separately
    if len(shapes_list) - 1 in index:
        return (_union_box_shapes(np.asarray(shapes_list)[index][:-1]) 
                | shapes_list[-1])
    else:
        return _union_box_shapes(np.asarray(shapes_list)[index])


def make_pescado_system(grid, L_pes, W_pes, L_narrow_gate, qpc_center, 
                        qpc_name, d0, d_2DEG, 
                        d1, d2, d3, d4, d_contact, d_up, d_tot, eps_GaAs, 
                        eps_AlGaAs, eps_gates, calibration_device,
                        zones, w, size, test_07, **kwargs):
    
    pp_builder = poisson.PoissonProblemBuilder()
    
    # Defining the several shape objects relevant for the problem
    layer0 = layer0_shape(L_pes, W_pes, d0, d_2DEG)
    layer1 = layer1_shape(L_pes, W_pes, d_2DEG, d1)
    layer2 = layer2_shape(L_pes, W_pes, d_2DEG, d1, d2)
    layer3 = layer3_shape(L_pes, W_pes, d_2DEG, d1, d2, d3)
    layer4 = layer4_shape(L_pes, W_pes, d_2DEG, d1, d2, d3, d4)

    DEG = DEG_shape(L_pes, W_pes, d_2DEG)
    
    interfaces = [-d0 - d_2DEG/2, 
                  -d_2DEG/2, 
                  d_2DEG/2, 
                  d_2DEG/2 + d1, 
                  d_2DEG/2 + d1 + d2, 
                  d_2DEG/2 + d1 + d2 + d3, 
                  d_2DEG/2 + d1 + d2 + d3 + d4,
                  d_2DEG/2 + d1 + d2 + d3 + d4 + d_contact]
    
    if test_07: 
        gates =  test_gates_shape(W_pes, L_narrow_gate, qpc_center, d_contact,
                                  d_up)
        interfaces = interfaces[:-1]
        zones = zones[:-1]
        gate_up = shapes.Box(lower_left=[qpc_center[0]-L_narrow_gate/2, 
                                     qpc_center[1], d_up-d_contact], 
                                size=[L_narrow_gate, W_pes/2 - qpc_center[1], 
                                      d_contact])
    
        gate_down = reflect(gate_up, [0,0,0], [0,1,0])
        
        local_grid = [*grid, d_contact]
        local_center_up = [*qpc_center, d_up-d_contact/2]
        local_center_down = [qpc_center[0], -qpc_center[1], d_up-d_contact/2]

        pattern_gate_up = patterns.Rectangular.constant(
                    element_size = local_grid, center = local_center_up)
        pattern_gate_down = patterns.Rectangular.constant(
                    element_size = local_grid, center = local_center_down)
        
        pp_builder.add_sub_mesh(shape_inst = gate_up, 
                                pattern_inst = pattern_gate_up)
        pp_builder.add_sub_mesh(shape_inst = gate_down, 
                                pattern_inst = pattern_gate_down)
        
    else:
        gates = gates_shape(W_pes, L_narrow_gate, qpc_center, qpc_name, 
                            d_contact, d_up)
    
    GaAs = layer0 | layer4
    AlGaAs_undoped = layer1 | layer3
    AlGaAs_doped = layer2
    
    shapes_list= np.array([layer0, DEG, layer1, layer2, layer3, layer4, gates])
    
    # Get the position of the mesh points in the z direction such that
    # the interfaces are respected
    
    
    print('The interfaces are located at: \n', interfaces)
    
    _, info = mesh_points(interfaces, zones, w, size)
    info, zone_tracker = simplify_zones(info)   
    
    print('Info (ref point, grid size z direction):\n', info)
    
    print('Zone tracker:\n', zone_tracker)
    
    # Set for every layer/set of layers the correct mesh
    for (ref_point, spacing), shapes_index in zip(info, zone_tracker):
        local_shape = union(shapes_list, shapes_index)
        local_grid = [*grid, spacing]
        local_center = [0,0,ref_point]
        local_pattern = patterns.Rectangular.constant(
                    element_size = local_grid, center = local_center)
        pp_builder.add_sub_mesh(shape_inst = local_shape, 
                                pattern_inst = local_pattern)
            
    # Set the site types (Neumann by default)
    pp_builder.set_dirichlet(shape=gates, setup_tag='gates')
    pp_builder.set_flexible(shape=DEG, setup_tag='2DEG')

    if calibration_device:
        pp_builder.set_dirichlet(shape=AlGaAs_doped, setup_tag='dopant')
    else:
        pp_builder.set_neumann(shape=AlGaAs_doped, setup_tag='dopant')
    
    # Set the relative dielectric permittivity (1 by default)
    pp_builder.set_relative_permittivity(val = eps_AlGaAs, 
                                         shape_inst = AlGaAs_undoped)
    pp_builder.set_relative_permittivity(val = eps_AlGaAs, 
                                         shape_inst = AlGaAs_doped)
    pp_builder.set_relative_permittivity(val = eps_GaAs, shape_inst = GaAs)
    pp_builder.set_relative_permittivity(val = eps_GaAs, shape_inst = DEG)
    pp_builder.set_relative_permittivity(val = eps_gates, shape_inst = gates)
    
    return pp_builder


def onsite(site, potential, conversion_index, lead_left, lead_right):
    return 4 - float(potential[conversion_index[site.pos]])


def onsite_lead_left(site, potential, conversion_index, 
                     lead_left, lead_right):
    """
    This function is necessary because kwant takes as site for the leads sites
    in the center of the 2DEG
    """
    _,y = site.pos
    x = lead_left
    
    return 4 - float(potential[conversion_index[ta.array([x,y])]])


def onsite_lead_right(site, potential, conversion_index, 
                      lead_left, lead_right):
    """
    This function is necessary because kwant takes as site for the leads sites
    in the center of the 2DEG
    """
    _,y = site.pos
    x = lead_right
    
    return 4 - float(potential[conversion_index[ta.array([x,y])]])


def _make_system(grid, L, W):
    """
    returns a builder object without leads
    """
    # First, define the tight-binding system
    sys = kwant.Builder()

    # Here, we are only working with square lattices
    # This only works for 3D, or 2D cross sections xz, yz
    # Anyway, a 2D xy or 1D cross section is meaningless in this case
    lat = kwant.lattice.Monatomic(np.diag(grid), norbs=1)
    
    def scat_reg(pos):
        x,y = pos
        return -L/2 <= x <= L/2 and -W/2 <= y <= W/2
        
    ## On site hamiltonian
    sys[lat.shape(scat_reg, [0,0])] = onsite

    ## Hopping in x and y direction
    sys[lat.neighbors(1)] = -1

    return sys, (lat, scat_reg)


def make_kwant_system(grid, L_kwant, W_kwant, **kwargs):
    sys, (lat, scat_reg) = _make_system(grid, L_kwant, W_kwant)
    
    # Adding the leads 
    ## Lead on the left    
    sym_left = kwant.TranslationalSymmetry((-grid[0], 0))
    lead_left = kwant.Builder(sym_left)
    # At the leads the voltage of the gates should have gone to zero but 
    # this can't be ensured: add  onsite potential at edge of scattering region
    lead_left[lat.shape(scat_reg, (0,0))] = onsite_lead_left
    lead_left[lat.neighbors()] = -1
    sys.attach_lead(lead_left)
    
    ## Lead on the right
    sym_right = kwant.TranslationalSymmetry((grid[0], 0))
    lead_right = kwant.Builder(sym_right)
    # At the leads the voltage of the gates should have gone to zero but 
    # this can't be ensured: add  onsite potential at edge of scattering region
    lead_right[lat.shape(scat_reg, (0,0))] = onsite_lead_right
    lead_right[lat.neighbors()] = -1
    sys.attach_lead(lead_right)
    
    return sys


def make_kpm_system(grid, L_kpm, W_kpm, **kwargs):
    sys, _ = _make_system(grid, L_kpm, W_kpm)
    return sys
    

def _check_conventions(grid, L_pes, W_pes, L_kwant, 
                       W_kwant, calibration_device, **kwargs):
    
    assert L_kwant <= L_pes, 'L_pes should be equal to or larger than L_kwant'
    assert W_kwant <= W_pes, 'W_pes should be equal to or larger than W_kwant'

        
def _extend_params(grid, d0, d_2DEG, d1, d2, d3, d4, 
                   d_contact, effective_mass, **kwargs):

    # Extend the geometrical parameters
    d_up = d_2DEG/2 + d1 + d2 + d3 + d4 + d_contact
    d_down = d_2DEG/2 + d0
    d_tot = d_up + d_down
    
    # Extend the kwant problem parameters
    # 1e-18 necessary because t should be expresssed in SI units
    t = c.hbar**2/(2* effective_mass * c.electron_mass * 
                     grid[0] * grid[1] * 1e-18)
        
    return {'d_up': d_up,
            'd_down': d_down,
            'd_tot': d_tot,
            't': t}
    

def extend_params(params):
    _check_conventions(**params)
    new_params = _extend_params(**params)
    return {**params, **new_params}


def problem_builder(p, pescado_sim=True):

    # Start kwant problem (only if the dimension of the problem is not 1)
    if len(p.grid_fine) != 1:
        kwant_sys = make_kwant_system(p)