<!-- #region -->
# QPC_Exp2Sim

Simulate g(Vg) for actual quantum point contacts belonging to the Juliang experimental data set.
Comparing with experiments.

## (Un)Installing package

### Using pip

For installation go to the qpc_exp2sim (top) directory and run the following command for and installation in development mode:

	pip install -e .

This allows qpc_exp2sim to be easily imported when doing simulations or data analysis in other python files.

For removing run:

	pip uninstall qpc_exp2sim

This doesn't remove the existing qpc_exp2sim.egg_info directory

### Using conda (doesn't work properly)

For installation go to the qpc_exp2sim directory and run:
	
    conda develop .

This adds the qpc_exp2sim path to conda.pth.
This can be checked by opening python and running: 


	import sys
	for p in sys.path: print(p)


For removing run

    conda develop . --uninstall

## Dependency on Kwant and Pescado

Kwant and pescado are necessary for qpc_exp2sim to function:
Installing Kwant can be done by following the instructions in the kwant documentation.
Installing Pescado on Windows (using conda) can be done as follows:

* Clone the pescado repository from gitlab into the Users/user_name/miniconda3/pkgs directory
* In the pescado (top) directory run the commands:

        python setup.py build_ext --inplace
	
        conda develop .

**Note**: In pescado\pescado\poisson\builder.py: line 78 in sparse_capa_matrix_parallel

    with multiprocessing.get_context('fork')

causes and error due to the fact that 'fork' is only available for Unix systems, changed to None.

* On linux pescado can be installed using

	pip install -e .
