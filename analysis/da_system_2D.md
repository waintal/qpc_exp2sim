---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import programs.tools.data_handling as dh
import programs.tools.plotting_sim as plotting
import programs.system_2D as s
import numpy as np
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
```

```python
p, coord, voltage, charge, conductance = dh.read_file('system_2D/trash')
```

```python
# Defining the several shape objects relevant for the problem
gates = s.gate_down_shape(p) | s.gate_up_shape(p)
ohmic_left = s.ohmic_left_shape(p)
ohmic_right = s.ohmic_right_shape(p)
device = s.device_shape(p)
device_region = s.device_region_shape(p)
silicon = s.silicon_shape(p)
    
# Plotting shapes for control
plotting.plot_shape(shapes_list=[device_region, silicon, ohmic_left, ohmic_right, gates],
                       colors=['c', 'y','b', 'b', 'g'], 
                       aspect_equal = True)
```

```python
charge_val = np.array([sparse_vec[np.arange(len(coord))] for sparse_vec in charge])

max_colorbar_n = np.max(np.abs(charge_val))
max_colorbar_v = np.max(np.abs(p.V_gates_list))
```

```python
def f(V_gates):
    index = np.where(p.V_gates_list == V_gates)[0][0]
    plotting.plot_colormap(coord, charge[index], p, setting=1,  max_colorbar=max_colorbar_n),\
            
interact(f, V_gates=p.V_gates_list)
```

```python
# Note: There are points located exactly at the edge
def f(V_gates):
    index = np.where(p.V_gates_list == V_gates)[0][0]
    plotting.plot_colormap(coord, voltage[index], p, setting=0, max_colorbar=max_colorbar_v),\
            
interact(f, V_gates=p.V_gates_list)
```

```python
plotting.plot_conductance(conductance, p)
```
