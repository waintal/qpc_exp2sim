---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import numpy as np
from qpc_exp2sim.tools import plotting_sim, data_handling as dh
import scipy.constants as c
import warnings
import matplotlib.pyplot as plt
import matplotlib as mpl

%matplotlib notebook
```

```python
data = dh.read_file('system_3D/data_files/b3_grid10_fine')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
V_gates_list = p_sim.V_gates_list
coord = data.coord
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
V3 = data.V3
p_geom.calibration_device = False
```

```python
plotting_sim.plot_cond_energy(conductance, V_gates_list, p_sim.energy_list, filtered=False, derivative=False)
plotting_sim.plot_cond_energy(conductance, V_gates_list, p_sim.energy_list, filtered=False, derivative=True)
plotting_sim.plot_cond_energy(conductance, V_gates_list, p_sim.energy_list, filtered=True, derivative=False)

# Check how these energy levels correspond to the energy level spacing according to the buttiker model

# From this graph we see that there's a non linear relationship between energy and V_g, this enforces a non constant 
# lever arm alpha
```

```python
def cond_vsd(conductance, energy_list, t, Vsd=500e-6):

    Esd = Vsd * c.elementary_charge/t
        
    index = np.where(np.all([-Esd/2 <= energy_list, energy_list<= Esd/2], axis=0))[0]
    conductance = np.average(conductance[:,index], axis=1)
    
    return conductance

# Not ideal since the edge of the data points migth not be equal to Vsd -> introduce weigth in the average function
single_cond = cond_vsd(conductance, p_sim.energy_list, p_geom.t)
```

```python
conductance_vsd = []
Vsd_list = np.linspace(0,0.005,1000)
for Vsd in Vsd_list:
    conductance_vsd.append(cond_vsd(conductance, p_sim.energy_list, p_geom.t, Vsd=Vsd))

conductance_vsd = np.array(conductance_vsd).transpose()

plotting_sim.plot_cond_energy(conductance_vsd, V_gates_list, Vsd_list*1000, filtered=False, derivative=False, ylabel=r'$V_{sd} [mV]$')
plotting_sim.plot_cond_energy(conductance_vsd, V_gates_list, Vsd_list*1000, filtered=False, derivative=True, ylabel=r'$V_{sd} [mV]$')
```

```python
%matplotlib notebook
from qpc_exp2sim.tools import plotting_sim, plotting_exp
filename_exp = 'exp/data_1D_mK.csv'
data = dh.read_file('system_3D/fine/b3_grid10_fine')

V_gates_list= data.p_sim.V_gates_list
conductance = data.conductance
V3 = data.V3[1]
labels = data.p_geom.qpc_name

limits = [0,1]

X = 'X2'

plotting_exp.plot_1D_map(filename_exp, X, 'Y3', qpc_n = 3, hlines=np.arange(8), subtract_resistance=False,
            limits_v=limits, label=['exp with series R'], V3_zero = True)
plotting_exp.plot_1D_map(filename_exp, X, 'Y3', qpc_n = 3, hlines=np.arange(8), subtract_resistance=True, 
            limits_v=limits, label=['exp series R subtracted'], show=False, V3_zero = True)
plotting_sim.plot_conductance(V_gates_list, conductance, V3=V3, linestyle='.--', 
                 hlines=np.arange(6), limits_v=limits, label=[r'sim $V_{sd} = 0 \mu V$'], show=False)

data = dh.read_file('system_3D/data_files/b3_grid10_fine')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
V_gates_list = p_sim.V_gates_list
conductance = data.conductance
single_cond = cond_vsd(conductance, p_sim.energy_list, p_geom.t, Vsd = 500e-6)

plotting_sim.plot_conductance(V_gates_list, single_cond, V3=V3, linestyle='--', 
                 hlines=np.arange(6), limits_v=limits, label=[r'sim $V_{sd} = 500 \mu V$'], show=False)

data = dh.read_file('system_3D/data_files/b3_grid10_fine')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
V_gates_list = p_sim.V_gates_list
conductance = data.conductance
single_cond = cond_vsd(conductance, p_sim.energy_list, p_geom.t, Vsd = 1000e-6)

V_gates_list = p_sim.V_gates_list

plotting_sim.plot_conductance(V_gates_list, single_cond, V3=V3, linestyle='--', 
                 hlines=np.arange(6), limits_v=limits, label=[r'sim $V_{sd} = 2000 \mu V$'], show=False)

```

```python
data = dh.read_file('b3_grid20')

```

```python
data.__dict__.keys()
```

```python
data.rpp_problem._kwant_indices
```

```python
1000/30
```

```python
import kwant
```

```python
sys = kwant.Builder
```

```python
lat = kwant.lattice.Monatomic(np.diag([20,20]), norbs=1)
```

```python
sites = list(data.rpp_problem._kwant_indices.keys())
```

```python
lat(0,0) in sites
```

```python
for elem in data.rpp_problem._kwant_indices.keys():
    print(type(elem))
```

```python
print(lat(0,0))
```

```python

```

```python

```
