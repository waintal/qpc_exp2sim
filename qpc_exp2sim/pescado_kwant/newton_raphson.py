#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 11:35:46 2022

@author: thellemans
"""
import copy
import numpy as np

from collections import Sequence
from functools import partial

from pescado.tools import SparseVector
from pescado.tools import error
from scipy.optimize import approx_fprime

class NewtonRaphson():
    """
    This class is based on the NaiveSchrodingerPoisson class in Pescado. 
    
    It is adapted to the needs of PescadoKwantProblem and extended in some
    ways. 
    
    The following adaptions/extensions are made:
    1)  __init__() 
        *   extended with the possibility to provide an ildos_dos/
            quantum_functional callable, returning respectively the origin and 
            slope for a given site and potential (ildos_dos) or immediately the 
            origin and slope for all corresponding sites in sites_ildos 
            (quantum_functional). 
            This forces also the adaption of initialize().
        *   tolerance is removed as parameter, it was never used.
        *   poisson_problem_inst is also allowed to be a ReducedPoissonProblem
    
    2)  iterate() 
        *   adapted to avoid resetting of the flexible sites each time as
            helmholtz. Now, this is done once in initialize().
    
    3)  convergence() 
        *   New function.
        *   Checks if the problem has converged
    
    4)  _solve_poisson()
        *   Default solver for the poisson problem set to 'mumps'                
    """
    
    def __init__(self, poisson_problem_inst, ildos= None, ildos_dos=None, 
                 quantum_functional = None, sites_ildos=None, 
                 potential_precision=1e-6):
        ''' 
        Copied from pescado with adaptions
        '''
        assert ((ildos is not None) ^ (ildos_dos is not None)
                 ^ (quantum_functional is not None)), (
                'Only one out of these may be provided: ildos, ildos_dos, '
                'quantum_functional')
        
        if isinstance(quantum_functional, (tuple, list)):
            self.quantum_functional_list = list(quantum_functional)
        elif callable(quantum_functional):
            self.quantum_functional_list = [quantum_functional,]
        else:
            if isinstance(ildos, (tuple, list)):
                self.ildos_list = ildos
                self.ildos_dos_list = None
            elif callable(ildos):
                self.ildos_list = [ildos, ]
                self.ildos_dos_list = None
            elif isinstance(ildos_dos, (tuple, list)):
                self.ildos_dos_list = ildos_dos
                self.ildos_list = None
            elif callable(ildos_dos):
                self.ildos_dos_list = [ildos_dos, ]
                self.ildos_list = None
            else:
                raise NotImplementedError(
                    ('tuple, list or callable was expected for "ildos", '
                     '"ildos_dos" or "quantum_functional", however '
                     '{0} was given for "ildos", '
                     '{1} was given for "ildos_dos" and'
                     '{2} was given for "quantum_functional"'
                     .format(ildos, ildos_dos, quantum_functional)))
            
            self.quantum_functional_list = list()

        self.sites_ildos = sites_ildos
        self.pp_inst = poisson_problem_inst
        self.potential_precision = potential_precision
        self.poisson_input = None # For initialization purposes
    
    
    def initialize(self,
        poisson_problem_input, initial_potential,
        return_poisson_output=False):
        ''' Initialize self consistency
        
        Adapted from pescado such that the cases with ildos_dos or 
        quantum_functional are treated correctly.

        Parameters
        -----------

        initial_guess: instance of 'pescado.tools.sparsevector.SparseVector'

            If 'self.ildos' is discrete

                Contains the Ildos interval used to initialize the points in
                the self consistent SP problem

            If 'self.ildos' is continuous

                Contains the 'chemical potential' used to initialize the
                points in the self consistent SP problem.

        poisson_problem_input: dict

            Elements are passed as input to poisson_problem_inst.solve()

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        self.input = None
        self.output = None

        self.poisson_input = poisson_problem_input
        self.iteration_data = list()

        if self.sites_ildos is None:
            self.sites_ildos = SparseVector(
                values=np.zeros(len(initial_potential.indices)),
                indices=initial_potential.indices)
        
        if self.quantum_functional_list == []:
            pot_pre = self.potential_precision
            
            if self.ildos_list is not None:
                for ildos in self.ildos_list:
        
                    quant_fun = partial(_quant_fun_ildos, 
                                        ildos=ildos, 
                                        pot_pre = pot_pre)
                    
                    self.quantum_functional_list.append(quant_fun)
            else:
                for ildos_dos in self.ildos_dos_list:
                    
                    quant_fun = partial(_quant_fun_ildos_dos, 
                                        ildos_dos=ildos_dos)
                    
                    self.quantum_functional_list.append(quant_fun)
        
        
        # Reset flexible sites configuration
        self.pp_inst.linear_problem_inst.reset_flexible_configuration()
        
        # Assign the sites for which we have an ildos as Hemholtz
        self.pp_inst.assign_flexible(
                helmholtz_index=self.sites_ildos.indices)
        
        # Other sites can be assigned based on the given input
        flexible_index = self.pp_inst.linear_problem_inst.regions_index[3]
        flexible_index = np.setdiff1d(flexible_index, self.sites_ildos.indices, 
                                      assume_unique=True)
        
        # Set the flexible indices that are in the voltage input to dirichlet
        self.pp_inst.assign_flexible(dirichlet_index = np.intersect1d(
            flexible_index, poisson_problem_input['voltage'].indices, 
            assume_unique=True))
        
        # Set the flexible indices that are not in the voltage input to neumann
        self.pp_inst.assign_flexible(neumann_index = np.setdiff1d(
            flexible_index, poisson_problem_input['voltage'].indices, 
            assume_unique=True))

        self.pp_inst.freeze()
        
        return self.iterate(
            initial_potential=initial_potential,
            return_iteration_data=return_poisson_output)
    
    
    def iterate(
        self, initial_potential=None,
        return_iteration_data=False):
        ''' Self consistent iteration

        Parameters
        ----------

        condition_fun: function (default None)

            condition_fun(error, wrong_interval) -> new_interval

            input
            ------
                error: instance of 'pescado.tools.sparsevector.SparseVector'
                    Contains the error from what is expected of the ILDOS and
                    the result from the poisson_problem_inst.solve()

                wrong_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the value of the wrong interval

            Returns
            -------
                new_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the new interval for the quantum sites in
                    'wrong_interval'

            Default behaviour: Moves the interval up if the sign of error
            is positive, down if negative

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        # Recover the origin / slope from the continuous ildos
        origin_sv = SparseVector()
        slope_sv = SparseVector()

        if initial_potential is None:
            self.input = self.output
        else:
            self.input = initial_potential

        origin_sv, slope_sv = self._get_dos_ildos(potential=self.input)

        # New poisson problem
        ps_pot_out, (voltage_output, charge_output) = self._solve_poisson(
            origin_sv=origin_sv,
            slope_sv=slope_sv,
            return_full_output=True)

        self.output = ps_pot_out

        try:
            self.iteration_data.append(
                {'input':self.input.values,
                 'output':self.output.values,
                 'indices':self.sites_ildos.indices})
        except IndexError:
            pass

        if return_iteration_data:
            return ps_pot_out, (voltage_output, charge_output), (
                    origin_sv, slope_sv)


    def iteration_error(self, ite1=None, ite2=None):
        ''' Calculates the absolute difference between 'ite1' and 'ite2'

        Parameters
        ----------
            ite1: integer, default None
                Defaults to the last iteration (-1)

            ite2: integer, default None
                Defaults to the 2nd last iteration (-2)

        Returns
        --------
            err_pot: pescado.SparseVector instance
                np.abs(potential_of_ite1 - potential_of_ite2)

            err_char: pescado.SparseVector instance
                np.abs(charge_of_ite1 - charge_of_ite2)
        '''

        return error.iteration_error(instance_1=self, ite1=ite1, ite2=ite2)

    
    def _solve_poisson(self,
        origin_sv, slope_sv, return_full_output=False):
        ''' Solves the poisson problem
        
        Adapted from pescado such that the sites are not reset to helmholtz
        each time a new iteration is done. These lines are commented out here, 
        but included in the initialize() function. If the user wants to be so
        smart to change sites in between iterations, he/she should be smart 
        enough to set them correctly.
        '''

        # Set charge sign convetion to pescado
        origin_sv.values = origin_sv.values * -1
        slope_sv.values = slope_sv.values * -1

        # Reset flexible sites configuration
        # self.pp_inst.linear_problem_inst.reset_flexible_configuration()

        # self.pp_inst.assign_flexible(
        #      helmholtz_index=self.sites_ildos.indices)

        # self.pp_inst.freeze()

        voltage_sv = SparseVector()
        for key, input_sv in zip(
                ['voltage', 'charge_electrons', 'helmholtz_density'],
                [voltage_sv, origin_sv, slope_sv]):
            if key in self.poisson_input.keys():
                s_vect = self.poisson_input[key]
                if s_vect is not None:
                    input_sv.extend(s_vect)

        default = {'charge_density':None,
                   'method':'mumps',
                   'kwargs':dict()}

        for key in ['charge_density', 'method', 'kwargs']:
            if key not in self.poisson_input.keys():
                self.poisson_input.update({key:default[key]})

        # Change charge sign so it respects pescado convention
        voltage_output, charge_output = self.pp_inst.solve(
            voltage=voltage_sv,
            charge_electrons=origin_sv,
            charge_density=self.poisson_input['charge_density'],
            helmholtz_density=slope_sv,
            method=self.poisson_input['method'],
            **self.poisson_input['kwargs'])

        output = SparseVector(
            values=voltage_output[self.sites_ildos.indices],
            indices=self.sites_ildos.indices)

        if return_full_output:
            return output, (voltage_output, charge_output)
        else:
            return output
    
    
    def convergence(self, abs_errv=1e-10, abs_errc=1e-10, rel_err=1e-6, 
                    verbose=False):
        """
        Check if the last iteration has converged.
        
        From the moment the absolute error OR the relative error is satisfied 
        for all flexible sites (respecting absolute or relative error is site 
        specific), 'True' is returned. Whenever the charge or voltage result
        at a site is exactly equal to zero the absolute error has to be 
        respected in order to be converged and return 'True'. 
        This because a relative error can't be calculated.
        
        abs_errv: float
            Maximal absolute error allowed for the voltage
        
        abs_errc: float
            Maximal absolute error allowed for the charge
                 
        rel_err: float
            Maximal relative error allowed for the voltage and charge
        """
        
        abs_err_voltage, abs_err_charge = self.iteration_error()
        
        # Finding the indices that violate the absolute error
        ind_voltage = abs_err_voltage.indices[
                    np.abs(abs_err_voltage.values) > abs_errv]
        
        ind_charge = abs_err_charge.indices[
                    np.abs(abs_err_charge.values) > abs_errc]
        
        # If there are no indices violating the absolute error we are converged
        if np.size(ind_voltage) + np.size(ind_charge) == 0:
            return True
        
        # Otherwise we check the relative error for the indices that violate 
        # the absolute error
        voltage_res = self.potential(iteration=-1)[ind_voltage]
        charge_res = self.charge(iteration=-1)[ind_charge]
        
        # If there is a zero value for the voltage or charge in the last 
        # iteration and we don't respect the absolute error here, we return 
        # False. This avoids a division by zero in the last check.
        if (0 in voltage_res) or (0 in charge_res):
            return False
        
        rel_err_voltage = abs_err_voltage[ind_voltage]/voltage_res
        rel_err_charge = abs_err_charge[ind_charge]/charge_res
        
        # Find from the indices that violate the absolute error the ones that
        # also violate the relative error.
        ind_voltage = ind_voltage[np.where(np.abs(rel_err_voltage) > rel_err)]
        ind_charge = ind_charge[np.where(np.abs(rel_err_charge) > rel_err)]
        
        if np.size(ind_voltage) + np.size(ind_charge) == 0:
            return True
        
        if verbose:
            print('{} indices violate the relative error on voltage: {},'
                  '\n{} indices violate the relative error on charge {}'
                  .format(len(ind_voltage), ind_voltage, 
                          len(ind_charge), ind_charge))
        
        return False
    
    
    def _get_dos_ildos(self, potential):
        ''' Gets the DOS and the origin for a given 'potential' SV

        Parameters
        -----------
            potential: instance of SparseVector

        Returns
        --------

            origin_sv: instance of SparseVector
                containing the origin

            slope_sv: instance of SparseVector
                containing the slope (DOS)

        '''

        origin_sv = SparseVector()
        slope_sv = SparseVector()

        for num, quant_fun in enumerate(self.quantum_functional_list):

            sites = self.sites_ildos.find(value=num)
            try:
                origin, slope = quant_fun(potential[sites])
            except AttributeError:
                raise RuntimeError(
                    ('No continuous Ildos for sites {0}, '.format(sites)
                    + 'hence can"t perform stupid iteration '))

            origin_sv.extend(SparseVector(values=origin, indices=sites))
            slope_sv.extend(SparseVector(values=slope, indices=sites))

        return origin_sv, slope_sv


    def charge(self, iteration):
        ''' Gets the charge value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''

        input_ = self.iteration_data[iteration]['input']

        all_sites = self.sites_ildos.indices

        pot = SparseVector(values=copy.deepcopy(input_), indices=all_sites)

        origin_sv, slope_sv = self._get_dos_ildos(potential=pot)

        char = origin_sv[all_sites] + slope_sv[all_sites] * pot[all_sites]

        return SparseVector(values=char, indices=all_sites)


    def potential(self, iteration):
        ''' Gets the potential value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        output_ = self.iteration_data[iteration]['output']

        all_sites = self.sites_ildos.indices

        return SparseVector(values=copy.deepcopy(output_), indices=all_sites)

        
def _quant_fun_ildos(point, ildos, pot_pre):
    """
    Helper function for NaiveSchrodingerPoisson_copy.initialize()
    
    The advantage of not using nested functions is that the object is 
    pickleable. 
    """
    if isinstance(point, (np.ndarray, Sequence)):
        slope = np.empty(len(point))
        origin = np.empty(len(point))
        for i, pt in enumerate(point):
            slope[i] = approx_fprime(
                xk=np.array([pt]), f=ildos,
                epsilon=pot_pre)[0]
            origin[i] = (ildos(pt) - slope[i] * pt)[0]
    
    else:
        slope = approx_fprime(
                xk=np.array([point]), f=ildos,
                epsilon=pot_pre)[0]
        origin = (ildos(point) - slope * point)[0]
    
    return origin, slope


def _quant_fun_ildos_dos(point, ildos_dos):
    """
    Helper function for NaiveSchrodingerPoisson_copy.initialize()
    
    The advantage of not using nested functions is that the object is 
    pickleable.
    """
    if isinstance(point, (np.ndarray, Sequence)):
        slope = np.empty(len(point))
        origin = np.empty(len(point))
        for i, pt in enumerate(point):
            origin[i], slope[i] = ildos_dos(pt)
    
    else:
        origin, slope = ildos_dos(point)
    
    return origin, slope
