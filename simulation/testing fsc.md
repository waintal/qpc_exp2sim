---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import pickle
import kwant
import scipy.constants as c
import numpy as np
import matplotlib.pyplot as plt
import qpc_exp2sim.tools.plotting_sim_old as plotting_sim
from qpc_exp2sim.pescado_kwant import plotting_pes, PescadoKwantProblem, extension_pes, ReducedPoissonProblem
from qpc_exp2sim.simulation import builder, solver
from copy import deepcopy
from pescado.tools import SparseVector
import time
from functools import partial
import tinyarray as ta
%matplotlib notebook
```

```python
geom_file = 'b3_grid50'
sim_file = 'V64_nr_lat_low_dens'

with open('files_geom/'+ geom_file, 'rb') as inp:
    p_geom, pk_problem = pickle.load(inp)
    
with open('files_data/' + geom_file + '_' + sim_file, 'rb') as inp:
    data = pickle.load(inp)
    
# unpacking the data for compatibility with old plotter
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3

cell_area = p_geom['grid_fine'][0] * p_geom['grid_fine'][1] * 1e-18

x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
params = {'lead_left': min(x_coord_kwant), 
          'lead_right': max(x_coord_kwant)}

index_v = 1
voltage_res = voltage[index_v]
charge_res = charge[index_v]
print('The gate voltage is {} V'.format(V_gates_list[index_v]))

threshold_ldos_val = p_sim['helmholtz_coef']/3
num_moments = 50
voltage = {'gates': V_gates_list[index_v] + voltage_shift}
charge_density = {'dopant': dopant_dens}
charge_electrons = {}
initial_potential = {'2DEG': 0}
helmholtz_coef = {}
center = np.array([0,0])
save_iteration = True

poisson_input = {'voltage' : voltage, 
                 'charge_electrons' : charge_electrons, 
                 'charge_density' : charge_density, 
                 'helmholtz_coef' : helmholtz_coef}
```

```python
dopant_dens
```

## Option in which we reconstruct and expand the calculated ildos
No setting of neumann sites

Lattice ILDOS only used in the very first iteration

```python
flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]
flexible_index = set(flexible_index)

voltage_vec, charge_vec, ldos_vec = (
        [voltage_res], [charge_res], [None])
```

```python
# Calculation of the potential in kwant energy units
potential = voltage_res * pk_problem.conversion_unit 

# Based on this potential we reduce the kpm/kwant system
pk_problem.reduce_system(potential, threshold=-4)

# With the reduced system we calculate the ldos using kpm       
ldos = pk_problem.exact_ldos(potential = potential,
                       num_moments = num_moments, 
                       where = pk_problem.kwant_sys.sites, 
                       params = params)

# Set the depleted sites to Neumann, by default the charge
# is zero so we should not set it in the poisson_input
helmholtz_subtype_index = list(flexible_index - pk_problem.depleted)
pk_problem.rpp_problem.assign_flexible(
    neumann_index=list(pk_problem.depleted), 
    helmholtz_index=helmholtz_subtype_index)
pk_problem.rpp_problem.freeze()

# Adapt sites_ildos to the possible Neumann sites.
sites_ildos = SparseVector(
    indices = helmholtz_subtype_index, 
    values = np.zeros(len(helmholtz_subtype_index)))

# As initial potential we can use voltage_res since it is always
# defined on all flexible sites
voltage_res, charge_res = pk_problem.rpp_problem.solve_sc_continuous(
        initial_potential = voltage_res,
        quantum_functional = [ldos.quantum_functional_kwant],
        sites_ildos = sites_ildos, 
        **poisson_input)


voltage_vec.append(voltage_res)
charge_vec.append(charge_res)
ldos_vec.append(ldos)
```

```python
voltage_vec, charge_vec, ldos_vec = pk_problem.solve_fsc_nr(voltage = voltage,
                        charge_density = charge_density,
                        initial_potential = initial_potential,
                        num_moments=50,
                        iterations=5, 
                        params=params, 
                        threshold_ldos=threshold_ldos_val, 
                        center = np.array([0,0]))
```

```python
len(voltage_vec)
```

```python
iteration = 3
coord_2D, voltage_2D = plotting_pes.data_cross_section(coord, voltage_vec[iteration], section=[None, None, 0])
coord_2D, charge_2D = plotting_pes.data_cross_section(coord, charge_vec[iteration], section=[None, None, 0])

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,5))
plotting_pes.colormap(coord_2D, voltage_2D, aspect_equal=True, ax=ax1, title='voltage')
plotting_pes.colormap(coord_2D, charge_2D/cell_area, aspect_equal=True, ax=ax2, title='charge')

plt.tight_layout()
plt.show()

if iteration != 0:
    fig, ax = plt.subplots()
    coord_2D, ldos_2D = plotting_pes.data_cross_section(coord[ldos_vec[iteration].indices], 
                                                        [ldos_vec[iteration](energy=0)], section = [None, None, 0])
    plotting_pes.colormap(coord_2D, ldos_2D, ax=ax)
    plt.show()
```

```python
ldos_vec[iteration].indices
```

```python
energies_to_plot = []
ldos_to_plot = []
ildos_to_plot = []

energies_fine_to_plot = np.linspace(-0.4,0.4, 1000)
ildos_fine_to_plot = []
for i, (ldos, sites) in enumerate(zip(ldos_vec, kwant_indices)):
    
    coord = np.array([site.pos for site in sites])
    index = PescadoKwantProblem.indices_from_coordinates(coord, np.array([0,0]))
    
    energies_to_plot.append(ldos.energies)
    ldos_to_plot.append(ldos.densities[:,index][:,0])
    
    ildos = PescadoKwantProblem.integrate_ldos(ldos, ldos.energies)
    ildos_fine = PescadoKwantProblem.integrate_ldos(ldos, energies_fine_to_plot)
    
    ildos_to_plot.append(ildos[:,index][:,0])
    ildos_fine_to_plot.append(ildos_fine[:,index][:,0])
    
energies_to_plot = np.array(energies_to_plot).transpose() *pk_problem.t/ c.elementary_charge
ldos_to_plot = np.array(ldos_to_plot).transpose() *2
ildos_to_plot = np.array(ildos_to_plot).transpose() *2

energies_fine_to_plot = energies_fine_to_plot *pk_problem.t/ c.elementary_charge
ildos_fine_to_plot = np.array(ildos_fine_to_plot).transpose() *2
```

```python
ldos_to_plot.shape
```

```python
fig, ax = plt.subplots()
ax.plot(energies_to_plot, ildos_to_plot)
plt.show()
```

```python
ildos_to_plot.shape
```

```python
fig, (ax1, ax2) = plt.subplots(1,2, figsize=(8,4))

ax1.plot(energies_to_plot, ldos_to_plot)
ax2.plot(energies_to_plot, ildos_to_plot)
ax1.legend([str(i) for i in range(len(energies_to_plot[0]))], loc='lower right')
ax1.set_xlabel(r'$\mu$ [V]')
ax2.set_xlabel(r'$\mu$ [V]')

ax1.set_ylabel('e-/(site V)')
ax2.set_ylabel('e-/site')
ax1.set_title('ldos')
ax2.set_title('ildos')

plt.tight_layout()
```

```python
index = pk_problem.pes_indices(np.array([0,0,0])[None,:])

voltage_convergence = [voltage_sv[index] for voltage_sv in voltage_sol]
charge_convergence = [charge_sv[index] for charge_sv in charge_sol]

iterations = np.arange(len(voltage_sol))

fig, ax = plt.subplots()
ax.plot(iterations, voltage_convergence, 'x-', label='voltage')
ax2 = ax.twinx()
ax2.plot(iterations, charge_convergence/cell_area, 'o-', color='tab:orange', label='density')
ax.set_title('Convergence curve')
ax.set_ylabel('voltage V')
ax2.set_ylabel('density n [/m²]')
ax.set_xlabel('iterations')
ax.legend()
ax2.legend()
plt.show()
```

```python
fig, ax = plt.subplots()
ax.plot(energies_fine_to_plot, ildos_fine_to_plot/cell_area)
ax.legend([str(i) for i in range(len(energies_to_plot[0]))], loc='lower right')
ax.set_xlabel(r'$\mu$ [V]')
ax.set_ylabel('ildos [/m²]')
#ax.set_xlim(-0.00002, max(voltage_convergence)*1.5)
#ax.set_ylim(0,max(charge_convergence)*4/cell_area)

for i in range(1,len(voltage_convergence)):
    ax.scatter(voltage_convergence[i] - voltage_convergence[i-1], charge_convergence[i]/cell_area)
```

## Option in which we set the ildos in the kpm region and neumann region equal to the lattice ildos

Problems with convergence!!

```python
geom_file = 'b3_grid50'
sim_file = 'V3_nr_lat_low_dens'

with open('files_geom/'+ geom_file, 'rb') as inp:
    p_geom, pk_problem = pickle.load(inp)
    
with open('files_data/' + geom_file + '_' + sim_file, 'rb') as inp:
    data = pickle.load(inp)
    
# unpacking the data for compatibility with old plotter
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3

cell_area = p_geom['grid_fine'][0] * p_geom['grid_fine'][1] * 1e-18

x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
params = {'lead_left': min(x_coord_kwant), 
          'lead_right': max(x_coord_kwant)}

index_v = 0
voltage_res = voltage[index_v]
charge_res = charge[index_v]
print('The gate voltage is {} V'.format(V_gates_list[index_v]))

threshold_ldos_val = p_sim['helmholtz_coef']/3
num_moments = 100
voltage = {'gates': V_gates_list[index_v] + voltage_shift}
charge_density = {'dopant': dopant_dens}
charge_electrons = {}
initial_potential = {'2DEG': 0}
helmholtz_coef = {}
```

```python
voltage_sol, charge_sol = [voltage_res], [charge_res]

ldos = pk_problem._spectral_density(num_moments=num_moments, 
                              where = lambda s: s.pos == np.array([0,0]), 
                              params=params)
        
quantum_functional_lat_f = partial(ldos.quantum_functional_lat, 
                                   t = pk_problem.t)

flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]
sites_ildos = SparseVector(indices = flexible_index, 
                           values = np.zeros(len(flexible_index)))

for iteration in range(4):    
    # Calculation of the potential in kwant energy units
    potential = voltage_res * (c.elementary_charge / pk_problem.t)    

    # Based on this potential we reduce the kpm/kwant system
    pk_problem.reduce_system(potential, threshold=-4)

    # With the reduced system we calculate the ldos using kpm       
    ldos = pk_problem._spectral_density(potential = potential,
                                  num_moments = num_moments, 
                                  where = pk_problem.kwant_sys.sites, 
                                  params = params)

    # We build the sites_ildos sparsevector in combination with the 
    # quantum functional
    coord_kwant = [site.pos for site in pk_problem.kwant_sys.sites]
    
    plotting_pes.colormap(np.array(coord_kwant), ldos(energy=0))
    plt.show()

    kwant.plot(pk_problem.kwant_sys)
    
    # Be very carefull now, sites_ildos must always remain ordered in kwant order!
    # Also in the NewtonRaphson solver it may not be reordered!
    # Create a seperate class that acts as ImmutableSparseVector (?)
    sites_ildos_kwant = [pk_problem.conversion_indices[coord]
                         for coord in coord_kwant]

    sites_ildos_lat = [pk_problem.conversion_indices[site.pos]
                       for site in pk_problem.kpm_sys.sites
                       if site not in pk_problem.kwant_sys.sites]
    
    sites_depleted = [pk_problem.conversion_indices[coord]
                      for coord in pk_problem.depleted]
    
    sites_ildos = SparseVector(
        indices = sites_ildos_lat + sites_ildos_kwant, 
        values = np.concatenate([np.zeros(len(sites_ildos_lat)), 
                                 np.ones(len(sites_ildos_kwant))]), 
        assume_sorted = True)

    initial_pot = SparseVector(
        indices = sites_ildos.indices, 
        values = voltage_res[sites_ildos.indices])
    
    charge_electrons_sv = pk_problem.rpp_problem._starting_values(charge_electrons)
    print(charge_electrons_sv)
    print(charge_electrons_sv.indices)
    charge_electrons_sv[sites_depleted] = np.zeros(len(sites_depleted))

    # Creating the quantum functional for the newly calculated ildos
    mu_ref = voltage_res[sites_ildos_kwant]
    quantum_functional_kwant_f = partial(ldos.quantum_functional_kwant, 
                                 mu_ref = mu_ref,
                                 t = pk_problem.t)
    
    origin, slope = quantum_functional_kwant_f(mu_ref)
    plotting_pes.colormap(np.array(coord_kwant), slope)
    plt.show()
    
    plotting_pes.colormap(np.array(coord_kwant), origin+slope*mu_ref)
    plt.show()

    voltage_res, charge_res = pk_problem.rpp_problem.solve_sc_continuous(
            voltage = voltage,
            charge_electrons = charge_electrons_sv,
            charge_density = charge_density,
            helmholtz_coef = helmholtz_coef,
            initial_potential = initial_pot,
            quantum_functional = [quantum_functional_lat_f,
                                  quantum_functional_kwant_f],
            sites_ildos = sites_ildos)

    # We only keep the results for the flexible region
    voltage_sol.append(voltage_res)
    charge_sol.append(charge_res)
```

```python
iteration=4
coord_2D, voltage_2D = plotting_pes.data_cross_section(coord, voltage_sol[iteration], section=[None, None, 0])
coord_2D, charge_2D = plotting_pes.data_cross_section(coord, charge_sol[iteration], section=[None, None, 0])

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,5))
plotting_pes.colormap(coord_2D, voltage_2D, aspect_equal=True, ax=ax1, title='voltage')
plotting_pes.colormap(coord_2D, charge_2D/cell_area, aspect_equal=True, ax=ax2, title='charge')

plt.tight_layout()
plt.show()
```

```python
'{:e}'.format(3.5e-5 * p_sim['helmholtz_coef']/cell_area)
```

## Option in which we set some sites to neumann

```python
geom_file = 'b3_grid50'
sim_file = 'V3_nr_lat_low_dens'

with open('files_geom/'+ geom_file, 'rb') as inp:
    p_geom, pk_problem = pickle.load(inp)
    
with open('files_data/' + geom_file + '_' + sim_file, 'rb') as inp:
    data = pickle.load(inp)
    
# unpacking the data for compatibility with old plotter
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3

cell_area = p_geom['grid_fine'][0] * p_geom['grid_fine'][1] * 1e-18

x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
params = {'lead_left': min(x_coord_kwant), 
          'lead_right': max(x_coord_kwant)}

index_v = 0
voltage_res = voltage[index_v]
charge_res = charge[index_v]
print('The gate voltage is {} V'.format(V_gates_list[index_v]))

threshold_ldos_val = p_sim['helmholtz_coef']/3
num_moments = 100
voltage = {'gates': V_gates_list[index_v] + voltage_shift}
charge_density = {'dopant': dopant_dens}
charge_electrons = {}
initial_potential = {'2DEG': 0}
helmholtz_coef = {}
```

```python
ldos = pk_problem._spectral_density(num_moments=num_moments, 
                                    where = lambda s: s.pos == np.array([0,0]), 
                                    params=params)

quantum_functional_lat_f = partial(ldos.quantum_functional_lat, 
                                   t = pk_problem.t)

flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]
sites_ildos = SparseVector(indices = flexible_index, 
                           values = np.zeros(len(flexible_index)))

voltage_sol, charge_sol = [voltage_res], [charge_res]

ldos_flex_sol, ildos_flex_sol = [], None

# Set the treshold for the ldos based filtering 
threshold_ldos = SparseVector(
        indices = flexible_index, 
        values = threshold_ldos_val + np.zeros(len(flexible_index)))

for iteration in range(5):
    pk_problem.rpp_problem.reset()
    
    # Calculation of the potential in kwant energy units
    potential = voltage_res * (c.elementary_charge / pk_problem.t)    

    # Based on this potential we reduce the kpm/kwant system
    pk_problem.reduce_system(potential, threshold=-8)

    # With the reduced system we calculate the ldos using kpm       
    ldos = pk_problem._spectral_density(potential = potential,
                                  num_moments = num_moments, 
                                  where = pk_problem.kwant_sys.sites, 
                                  params = params)
    ldos_kpm = ldos(energy=0).real

    # Reconstruct data to original kwant coordinates and extend to 
    # pescado coordinates
    flexible_index, ldos_flex = pk_problem.recext_data(ldos_kpm[:,None])

    ldos_flex *= 2 #2 for spin
    ldos_flex_sol.append(ldos_flex)

    ldos_flex_pes_units = SparseVector(
            indices = flexible_index, 
            values = ldos_flex * (c.elementary_charge / pk_problem.t)) 

    # We build the sites_ildos sparsevector in combination with the 
    # quantum functional
    coord_kpm_only = [site.pos for site in pk_problem.kpm_sys.sites
                      if site not in pk_problem.kwant_sys.sites]

    coord_kwant = [site.pos for site in pk_problem.kwant_sys.sites]

    sites_ildos_kwant = list(map(lambda coord: 
        pk_problem.conversion_indices[coord], coord_kwant))

    sites_ildos_lat = list(map(lambda coord: 
        pk_problem.conversion_indices[coord], coord_kpm_only))

    sites_ildos[sites_ildos_lat] = np.zeros(len(sites_ildos_lat))
    sites_ildos[sites_ildos_kwant] = np.ones(len(sites_ildos_kwant))

    moments = ldos.moments.transpose()
    _, moments = PescadoKwantProblem.sort_pescado(np.asarray(coord_kwant), moments)

    # This filter will alow us to remove the sites that are set to 
    # Neumann from the moments list before we enter the new nr solver
    filter_moments = SparseVector(indices = sites_ildos_kwant, 
                                  values = np.arange(len(sites_ildos_kwant)))

    # Filtering the flexible sites for sites that become neumann 
    # (depleted) or sites that stay flexible
    # Update the initial conditions correspondingly
    pk_problem.rpp_problem.filter_flexible(charge_res, ldos_flex_pes_units,
                                           threshold_ldos=threshold_ldos)

    initial_pot = {}
    for region in pk_problem.rpp_problem.regions['flexible']:
        region_index = pk_problem.rpp_problem.sub_region_index[region]
        if '_depleted' in region:
            charge_electrons.update({region : 0})
            #pk_problem.rpp_problem.assign_flexible(neumann_index=region_index)
            sites_ildos.delete(region_index)
            filter_moments.delete(region_index)
        else:
            #pk_problem.rpp_problem.assign_flexible(helmholtz_index=region_index)
            initial_pot[region] = voltage_res[region_index]
    
    kwant.plot(pk_problem.kwant_sys)
    plotting_pes.colormap(pk_problem.coord[sites_ildos.indices][:,0:2], 
                          np.ones(len(pk_problem.coord[sites_ildos.indices])))

    # Creating the quantum functional for the newly calculated ildos

    # Due to the calculation of the SpectralDensity at a certain 
    # potential, the ldos is shifted in energy. We need to take this
    # into account when evaluating the quantum functional.
    mu_ref = voltage_res[sites_ildos.find(value=1)]
    ldos.moments = moments[filter_moments.values].transpose()

    quantum_functional_kwant_f = partial(ldos.quantum_functional_kwant, 
                                 mu_ref = mu_ref,
                                 t = pk_problem.t)

    # Solving the nr problem
    # pk_problem.rpp_problem.freeze()

    #print(pk_problem.coord[sites_ildos.indices], len(sites_ildos.indices))

    (voltage_res, charge_res), nsp_problem = pk_problem.rpp_problem.solve_sc_continuous(
            voltage = voltage,
            charge_electrons = charge_electrons,
            charge_density = charge_density,
            helmholtz_coef = helmholtz_coef,
            initial_potential = initial_pot,
            quantum_functional = [quantum_functional_lat_f,
                                  quantum_functional_kwant_f],
            sites_ildos = sites_ildos)

    # We only keep the results for the flexible region
    voltage_sol.append(voltage_res)
    charge_sol.append(charge_res)
```

```python
display(pk_problem.rpp_problem.sub_region_index['2DEG_depleted'].shape)
display(pk_problem.rpp_problem.sub_region_index['2DEG'].shape)
```

```python
iteration=1
coord_2D, voltage_2D = plotting_pes.data_cross_section(coord, voltage_sol[iteration], section=[None, None, 0])
coord_2D, charge_2D = plotting_pes.data_cross_section(coord, charge_sol[iteration], section=[None, None, 0])

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,5))
plotting_pes.colormap(coord_2D, voltage_2D, aspect_equal=True, ax=ax1, title='voltage')
plotting_pes.colormap(coord_2D, charge_2D/cell_area, aspect_equal=True, ax=ax2, title='charge')

plt.tight_layout()
plt.show()
```

```python
ldos
```

```python

```

```python

```

```python

```

```python

```

```python

```
