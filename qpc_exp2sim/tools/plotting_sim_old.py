#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 16:07:29 2022

@author: thellemans
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pescado import tools
from pescado.mesher import shapes
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D # This import is necessary!
try:
    import mpl_axes_aligner
except:
    pass    
from .plotting_sim import crossings
from . import data_handling
from ..simulation.QPC_geometry import qpc_data
from ..pescado_kwant.extension_pes import cross_section
import warnings 
from ..simulation import builder
from ..pescado_kwant import plotting_pes
from types import SimpleNamespace

def plot_overview_points(p_geom, points, ax=None, margin=30, color=None):
    """
    check if the shapes can be pickled now that the definition of General.shape is adapted, 
    if so:
        1) adapt the definition of the rpp_problem to store the shapes
        2) adapt the definition make_pescado_system
        3) load the gate shape here from the data_file
    """
    if isinstance(p_geom, dict):
        gates = builder.gates_shape(**p_geom)
        p_geom = SimpleNamespace(**p_geom)
    else:
        gates = builder.gates_shape(**p_geom.__dict__)


    plotting_pes.shape(shapes_list=[gates], 
                            colors=['lightgray'],  
                            grid_cell = [10, 10, 40], 
                            section = [None, None, 120], 
                            ax=ax, pcolormesh=False)
    
    ax.scatter(*points.transpose(), color=color)
    ax.set_xlim((-p_geom.L_kwant/2-margin, p_geom.L_kwant/2+margin))
    ax.set_ylim((-p_geom.W_kwant/2-margin, p_geom.W_kwant/2+margin))
    ax.plot([-p_geom.L_kwant/2, -p_geom.L_kwant/2, p_geom.L_kwant/2, p_geom.L_kwant/2, -p_geom.L_kwant/2], 
             [-p_geom.W_kwant/2, p_geom.W_kwant/2, p_geom.W_kwant/2, -p_geom.W_kwant/2, -p_geom.W_kwant/2],
            color = 'k', linestyle = '--', linewidth = 0.5)
    ax.set_xticks([-p_geom.L_kwant/2, -p_geom.L_kwant/4, 0 , p_geom.L_kwant/4, p_geom.L_kwant/2])
    ax.set_yticks([-p_geom.W_kwant/2, -p_geom.W_kwant/4, 0 , p_geom.W_kwant/4, p_geom.W_kwant/2])
    ax.set_ylabel('y (nm)', labelpad=-13)
    

def plot_shape(shapes_list, colors, grid_cell=10, coord=None, section=None, 
               aspect_equal=True, figsize=None, legend_label=None, ax=None,
               ax_labels=None, show=True):
    
    dim_shape = len(shapes_list[0].bbox[0])
    assert np.all([len(shape.bbox[0]) == dim_shape for shape in shapes_list]), \
     'All shapes must have the same dimension.'
    assert len(shapes_list) == len(colors),\
     'Not all shapes have a specified color'
    

    def create_grid(shapes_list, grid_cell, section, dim_plot, dim_shape):
        # Setting the grid cell
        assert isinstance(grid_cell, (int, float)) or \
         len(grid_cell) == dim_plot or len(grid_cell)==dim_shape, \
            'The length of the grid_cell must be equal to the dimension of the plot, \n\
            the dimension of the shapes or be a float/int'
        
        if isinstance(grid_cell, (int, float)):
            grid_cell = [grid_cell] * dim_plot
        elif len(grid_cell) == dim_shape and dim_plot != dim_shape:
            grid_cell = np.delete(grid_cell, [i for i, val in enumerate(section) if val is not None])             

        # Creating the grid
        grid_bbox = shapes.add_bbox([shape.bbox for shape in shapes_list])
        coord = tools.meshing.grid(bbox=grid_bbox, step=grid_cell, center=[0]*dim_plot)
        return coord
    
    assert section is None or len(section) == dim_shape,\
     'The section needs to be None (for no cross section) or needs to have as many elements as the dimension of the shapes'
    
    # Check if the provided section matches with the dimension and make a cross section of the coordinates if necessary
    if section is None or section == [None]*dim_shape:
        # No cross section has to be made, coord remains coord
        section = [None] * dim_shape
        dim_plot = dim_shape
        if coord is None:
            coord = create_grid(shapes_list, grid_cell, section, dim_plot, dim_shape)
    else:
        # A cross section has to be made
        dim_plot = section.count(None)
        
        shapes_list = [cross_section(shape, section) for shape in shapes_list]
        
        # If the coordinates are provided we use these points, otherwise we create a grid ourselves
        if coord is None:
            coord = create_grid(shapes_list, grid_cell, section, dim_plot, dim_shape)
        else:
            boolean = np.array([coord[:,i] == val for i, val in enumerate(section) if val is not None])
            index = np.where(np.all(boolean, axis=0))[0]
            coord = np.delete(coord, [i for i, val in enumerate(section) if val is not None], 1) 
            coord = coord[index]
    
    # Check if the provided ax_labels match with the dimension
    if ax_labels is None:
        ax_labels = ['x (nm)', 'y (nm)', 'z (nm)']
        ax_labels = [ax_label for i, ax_label in enumerate(ax_labels) if section[i] is None]
    else:
        assert len(ax_labels) == dim_plot, \
        'If the "ax_labels" are provided manually, they should have the same length as the dimension of the plot'
    
    
    # Initialise the plot
    if dim_plot == 1:
        if ax is None:
            figsize = [figsize,1] if figsize is not None else None
            plt.figure(figsize=figsize)
            ax = plt.axes()
        ax.set_xlabel(ax_labels[0])
        ax.set_yticklabels([])
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(coord[index, 0],
                       np.zeros(len(coord[index, 0])), marker='.', c=color)
    elif dim_plot == 2:
        if ax is None:
            figsize = [figsize]*2 if figsize is not None else None
            plt.figure(figsize=figsize)
            ax = plt.axes()
        ax.set_xlabel(ax_labels[0])
        ax.set_ylabel(ax_labels[1])
        # Setting the aspect ratio of the plot equal or not
        if aspect_equal:
            ax.set_aspect('equal', adjustable='box')
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(coord[index, 0], 
                       coord[index, 1], marker='.', c=color)
    elif dim_plot == 3:
        if ax is None:
            figsize = [figsize]*2 if figsize is not None else None
            plt.figure(figsize=figsize)
            ax = plt.axes(projection="3d")
        ax.set_xlabel(ax_labels[0])
        ax.set_ylabel(ax_labels[1])
        ax.set_zlabel(ax_labels[2])
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(coord[index, 0], 
                        coord[index, 1], 
                        coord[index, 2], marker='.', c=color)  
    
    if legend_label is not None:
        ax.legend(legend_label, loc='center right', bbox_to_anchor=(1.35, 0.5))
    
    if show:
        plt.show()


def data_cross_section(coord, data, section=None):
    """
    Parameters
    ----------
    coord: np.ndarray
    
    data: sparse_vector or list of sparse_vectors
    
    section: list, same shape as coord[0] or list of lists
    
    Returns
    -------
    coord_filtered: np.ndarray
        shape (m, n, o) with
            m = len(indices) with indices the indices of the coordinates within the section
            n = dimension cross section
            # o = len(data_list) -> Not included, could be implemented
            # p = len(section_list) -> Not included, could be implemented
    
    data_filtered: np.ndarray
        shape (m,p) with
            m = len(indices) with indices the indices of the coordinates within the section
            q = dimension cross section

    """
        
    # If only a single section or single sparse vectored is entered, we put them in a list
    if section is None:
        section = [[None]*len(coord[0])]
    elif isinstance(section[0], (int, float)) or section[0] is None:
        section = [section]
        assert len(coord[0]) == len(section[0])
    if isinstance(data, tools.sparse_vector.SparseVector):
        data = [data]
    
    data_filtered = []
    coord_filtered = []
    
    # Check if the asked sections are valid sections for the provided coordinates
    if not np.all(np.array(section) == None):
        boolean = [np.all(np.any([coord[:,i] == val \
            for i, val in enumerate(section_elem) if val is not None], axis=1)) \
            for section_elem in section]
        assert np.all(boolean), \
        'The following sections can not be found in the coordinate array: \n\
        {}'.format(section[np.logical_not(boolean)])
    
    # Making the asked sections
    for section_elem in section:
        # If all the data is asked (section is completely None)
        if np.all(section_elem == [None]*len(section_elem)):
            index = np.arange(len(coord))
        # If we need to make a real cross section:
        else:
            boolean = np.array([coord[:,i] == val for i, val in enumerate(section_elem) if val is not None])
            index = np.where(np.all(boolean, axis=0))[0]
        # Find the data corresponding to these indices
        data_filtered.append([data_elem[index].astype(float) for data_elem in data])
    
    data_filtered = np.array(data_filtered).transpose()
    
    # Find the coordinates corresponding to these indices
    coord_dim_reduced = np.array([coord[:,i] for i, val in enumerate(section_elem) if val is None]).transpose()
    #np.delete(coord, [i for i, val in enumerate(section_elem) if val is not None], 1) 
    # If we evaluate to a single point in the data set
    if len(coord_dim_reduced) == 0:
        coord_filtered = np.empty((0,0))
    # If there would still be some coordinate depency of the data
    else:
        coord_filtered.append(coord_dim_reduced[index])
        coord_filtered = np.array(coord_filtered)[0]
    
    return coord_filtered, data_filtered


def plot_line(coord, data, p, setting_x, setting_y, ax=None, legend_label=None, 
              figsize=None, vline=False, show=True):
    """
    setting_y: 
        input value to choose the right y axis label
            0 for voltage
            1 for density (charge/cell)
            2 for density (/m²)
    setting_x: 
        input value to choose the right x axis label
            0,1,2 for x,y,z
            3 for Vg
    legend: 
        list containing the legend 
    """ 
    
    assert show is True or ax is not None, "Show has to be True or "+\
    "ax has to be an matplotlib.axes._subplots.AxesSubplot object"
    
    if ax is None:
        figsize = [figsize]*2 if figsize is not None else None
        fig, ax = plt.subplots(figsize=figsize)
           
    if setting_y == 2:
        # np.product(np.array(p.grid)) for conversion from charge/cell to charge/nm²
        # 1e18 for charge/nm² to charge/m²
        data = data / np.product(np.array(p.grid)) * 1e18
    
    data = data.reshape([size for size in data.shape if size != 1])
    ax.plot(coord, data)
    
    x_labels = ['x(nm)', 'y(nm)', 'z(nm)', r'$V_g [V]$']
    y_labels = ['Voltage [V]', 'Charge per cell', r'$n [/m^2]$', r'$V/V_{ref}$ [-]', r'$n/n_{ref}$ [-]']
    ax.set_xlabel(x_labels[setting_x])
    ax.set_ylabel(y_labels[setting_y])
    
    if legend_label is not None:
        ax.legend(legend_label)

    if vline:
        if setting_x == 0:
            vertical_lines = []#[- p.L_narrow_gate/2, p.L_narrow_gate/2]
        elif setting_x == 1:
            vertical_lines = []# [-(p.W - p.W_narrow_gate)/2, (p.W - p.W_narrow_gate)/2]
        else:
            vertical_lines = [-p.d_2DEG/2, 
                              p.d_2DEG/2, 
                              p.d_2DEG/2 + p.d1, 
                              p.d_2DEG/2 + p.d1 + p.d2, 
                              p.d_2DEG/2 + p.d1 + p.d2 + p.d3,
                              p.d_2DEG/2 + p.d1 + p.d2 + p.d3 + p.d4]
        for x in vertical_lines:
            ax.axvline(x, color = 'k', linestyle = '--', linewidth =0.5)
    
    if show:
        plt.show()
    

def plot_colormap(coord, data, cell_area=None, setting=0, cbar_range=[None,None], 
                  labels=['x(nm)', 'y(nm)'], title=None, aspect_equal = False, 
                  ax=None, figsize=7, sim='3D', show=True, pcolormesh=False):
    """
    setting: 
        input value to choose the right colorbar label
            0 for voltage
            1 for density
    
    sim: str, '2D' or '3D'
        To spefify if the data comes from a 1D or 2D simulation, this changes
        the conversion from charge to cell to density per square meter
    
    cell_area: int, np.ndarray or list
        The area of the voronoi cells perpendicular to the plane of the plot
        expressed in m^2 
        For a 2D simulation this was originally: data = data / (p.grid[0] *  1e-18)  
    
        p.grid[0] * p.grid[1] * 1e-18
    
    TO DO: Don't use scatter to create colormap
    """     
    assert setting in range(5), \
     r"Setting has to be an int: \n\
     * 0 (voltage V) \n\
     * 1 (charge per cell) \n\
     * 2 (density [/$m^2$]) \n\
     * 3 (relative voltage [-]) \n\
     * 4 (relative density [-])"     
    
    if len(data.shape) != 1:
        data = data.flatten()
        
    if ax is None:
        figsize = [figsize]*2 if isinstance(figsize, (int,float)) else figsize
        fig, ax = plt.subplots(figsize=figsize)   
    
    if setting == 2:
        if isinstance(cell_area, list):
            cell_area = np.array(cell_area)    
        data = data/cell_area
    
    # Settings for the limits
    ax.set_xlim((np.min(coord[:,0]), np.max(coord[:,0])))
    ax.set_ylim((np.min(coord[:,1]), np.max(coord[:,1])))
    
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    
    if aspect_equal:
        ax.set_aspect('equal', adjustable='box')
    
    if title is not None: 
        ax.set_title(title)
    
    # Settings for the colorbar
    cbar_label = ['Voltage (V)', 'Charge per cell', r'$n [/m^2]$', \
                  r'$V/V_{ref}$ [-]', r'$n/n_{ref}$ [-]'][setting]
    
    colormap = [plt.cm.viridis_r, plt.cm.plasma, plt.cm.plasma, 
                plt.cm.bwr, plt.cm.bwr][setting]
    
    if cbar_range[0] is None:
        val_min = np.min(data) #- max_colorbar
    else:
        val_min = cbar_range[0]
        
    if cbar_range[1] is None:
        val_max = np.max(data)
    else:
        val_max = cbar_range[1]
    abs_max = max(abs(val_min), abs(val_max))
    
    if setting in [0,1,2]:
        normalize = mpl.colors.Normalize(vmin=val_min, vmax=val_max)
    else:
        normalize = mpl.colors.SymLogNorm(linthresh=abs_max/1e3, linscale=1e-3, vmin = - abs_max, vmax = abs_max)

    scalarmappable = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
    scalarmappable.set_array(len(data))

    # Makes sure the colorbar is equally high as the figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    #cax = divider.new_horizontal(size="5%", pad=0.5, pack_start=True)
    #fig.add_axes(cax)

    if setting in [0,1,2]:
        cbar = plt.colorbar(scalarmappable, cax=cax) #cax=cax extra as argument
    else: 
        cbar = plt.colorbar(scalarmappable, cax=cax) #cax=cax extra as argument
    cbar.set_label(cbar_label)
    
    # Plotting of the data:
    if pcolormesh:
        X,Y = cmap_mesh(coord[:,0], coord[:,1])
        ax.pcolormesh(X,Y, cmap_data(coord, data), cmap=colormap, norm=normalize)
    else:
        ax.scatter(coord[:, 0], coord[:, 1],
                c=colormap(normalize(data)), s=10)
    
    if show:
        plt.show()
    

def plot_conductance(V_gates_list, conductance, V3=0, energy_list=[0], label = None, 
                     linestyle = '.--', limits_v=None, hlines=None, show=True):

    # Plotting the conductance in function of V_g
    #for index in range(len(energy_list)):
    if show:
        plt.figure()
    
    plt.xlabel("V_g [V]")
    plt.ylabel("conductance [2e^2/h]")
    
    y_min, y_max = 0, -float('inf')
    

    
    if isinstance(V_gates_list[0], (int, float)):
        V_gates_list = [V_gates_list]
    if isinstance(conductance[0], (int, float)):
        conductance = [conductance]
    
    for i, V_gates in enumerate(V_gates_list):
        V_gates -= V3
        plt.plot(V_gates, conductance[i], linestyle, label=label[i] \
                 if label is not None else None)
        
        if limits_v is not None:
            # For setting the limits in the y-direction
            indices = np.where(np.logical_and(limits_v[0] <= V_gates, V_gates <= limits_v[1]))
            conductance_values = np.array(conductance[i])[indices]
            y_min = min(np.min(conductance_values), y_min)
            y_max = max(np.max(conductance_values), y_max)
    
    if limits_v is not None:
        plt.xlim(limits_v)
        plt.ylim((y_min, y_max))
    
    if label is not None:
        if len(label) <= 10:
            loc = 'best'
            bbox = None
        else:
            loc = 'center right'
            bbox = (1.35, 0.5)

        plt.gca().legend(loc=loc, bbox_to_anchor=bbox)

    if isinstance(hlines, (list, np.ndarray)):
        for line in hlines:
            plt.axhline(line, color = 'k', linestyle = '--', linewidth =0.5)
    
    if show:
        plt.show()

    
    # Plotting the conductance in function of energy
#    if len(energy_list) > 1:
#        plt.figure()
#        plt.plot(energy_list, np.transpose(conductance))
#        plt.xlabel("energy [t]")
#        plt.ylabel("conductance [e^2/h]")
#        plt.gca().legend(["V_gates = {}".format(round(v, 2)) for v in V_gates_list], 
#                loc='center right', bbox_to_anchor=(1.35, 0.5))
#        if show:
#            plt.show()


def linear_fit_conductance(V_gates_list, conductance, show_slope = False):
    slope, cross_points, infliction_points = crossings(V_gates_list, conductance)

    fig,ax1 = plt.subplots()
    # Create two separate axes, one for the derivative and one for the conductance steps
    if show_slope:
        ax2=ax1.twinx()
        ax2.plot(V_gates_list, slope, '.-', color='tab:orange')

    ax1.plot(V_gates_list, conductance, '.-', color='tab:blue')

    for x in infliction_points[:,0]:
        plt.axvline(x, color = 'k', linestyle = '--', linewidth =0.5)

    for x in np.unique(cross_points[:,1]):
        ax1.axhline(x, color = 'k', linestyle = '--', linewidth =0.5)  

    ax1.plot(cross_points[:,0], cross_points[:,1], color = 'tab:red', linestyle = '-', linewidth =2)

    if show_slope:
        mpl_axes_aligner.align.yaxes(ax1, 0, ax2, 0)
        ax2.set_ylabel('slope [Vh/2e²]')
        
    ax1.set_xlabel("V_g [V]")
    ax1.set_ylabel("conductance [2e²/h]")
    plt.show()
    
    
def cmap_mesh(x,y):
    """
    Only for rectangular meshes
    """
    def cmap_points(data):
        data = np.unique(data)
        diff = np.diff(data)
        X = data - np.insert(diff, 0, diff[0])/2
        X = np.append(X, data[-1]+diff[-1]/2)
        return X
    return np.meshgrid(cmap_points(x), cmap_points(y))


def cmap_data(coord, data):    
    """
    if coord = [[-1, -1], 
                [-1, 1], 
                [1, -1], 
                [1, 1]]
    
    data = [1, 2, 3, 4]
    
    The result is: [[1, 3], [2, 4]]
    
    """    
    X, Y = np.meshgrid(np.unique(coord[:,0]), np.unique(coord[:,1]))
    mesh_shape = X.shape
    
    lookup_dict = dict(zip([tuple(elem) for elem in coord], data.ravel()))
    data_map = np.array(list(map(lambda xx, yy : lookup_dict[(xx,yy)], X.ravel(), Y.ravel())))
    data_map = data_map.reshape(mesh_shape)
    
    return data_map

def cmap_data_inverse(data):
    return data.transpose().reshape(1, data.size)[0]


def plot_cond_energy(conductance, V_gates_list, energy_list, nb_steps=7, filtered = False, derivative=False, ylabel=None):
    fig, ax = plt.subplots()
    ax.set_xlabel(r'$V_g [V]$')
    if ylabel is None:
        ax.set_ylabel('energy [t]')
    else:
        ax.set_ylabel(ylabel)
    label = r'Conductance $2e^2/h$'

    cmap = plt.get_cmap('hot')
    X,Y = cmap_mesh(V_gates_list, energy_list)

    if filtered:
        if derivative:
            warnings.warn('Plotting derivative only possible for "filtered=False", normal conductance plotted')
        levels = np.insert(np.arange(0.5, nb_steps+0.5, 1), 0, 0)
        norm = mpl.colors.BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)
        cf = ax.contourf(V_gates_list, energy_list, conductance.transpose(), levels=levels, cmap=cmap)
        fig.colorbar(cf, ax=ax, ticks=np.arange(nb_steps), label = label)
    else:
        if derivative:
            conductance = np.gradient(conductance, V_gates_list[1] - V_gates_list[0], axis=0)
            label = r'$dG/dV_g$'
        norm = mpl.colors.Normalize(vmin=0, vmax=np.nanmax(conductance))
        scalarmappable = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
        fig.colorbar(scalarmappable, label = label)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)


def plot_V3(files, figsize=(10,7)):
    
    if not isinstance(files[0], list):
        files = [files]
    
    fig, axs = plt.subplots(len(files), figsize=figsize)
    
    for i, files_elem in enumerate(files):
        # Read the data
        
        if len(files) == 1:
            axs = [axs]
        
        V3_exp = []
        V3_exp_std = []
        V3_sim_dens = []
        V3_sim_cond = []
        V3_sim_el = []
        x = []
    
        for file in files_elem:
            data = data_handling.read_file(file)
            V3_sim_dens.append(data.V3[0][0])
            V3_sim_cond.append(data.V3[1])
            qpc = qpc_data[data.p_geom.qpc_name]
            x_param = {'a':'L', 'b':'R', 'c':'L'}[data.p_geom.qpc_name[0]]
            x.append(qpc[x_param])
            V3_sim_el.append(np.average(qpc['V3_sim_el']))
            V3_exp.append(np.average(qpc['V3_exp']))
            V3_exp_std.append(np.std(qpc['V3_exp']))
    
        # Plot the data
            
        hlines = [-2.5, -2.25, -2,-1.75, -1.5, -1.25, -1]
        for line in hlines:
            axs[i].axhline(line, color = 'k', linestyle = '--', linewidth =0.3)
    
    
        axs[i].errorbar(x[0], V3_exp[0], yerr=V3_exp_std[0], color='tab:blue', label='exp: W = 250 nm')
        axs[i].errorbar(x[1:], V3_exp[1:], yerr=V3_exp_std[1:],  color='tab:orange', label='exp: W = 300 nm')
    
        axs[i].plot(x[0], V3_sim_dens[0], marker='o', markerfacecolor='none', color='k', label="sim: dens")
        axs[i].plot(x[1:], V3_sim_dens[1:], marker='o', markerfacecolor='none', color='k', linestyle='--')
    
        axs[i].plot(x[0], V3_sim_cond[0], marker='d', markerfacecolor='none', color='k', label="sim: cond")
        axs[i].plot(x[1:], V3_sim_cond[1:], marker='d', markerfacecolor='none', color='k', linestyle='--')
        
        axs[i].plot(x[0], V3_sim_el[0], marker='x', markerfacecolor='none', color='r', label="sim: eleni")
        axs[i].plot(x[1:], V3_sim_el[1:], marker='x', markerfacecolor='none', color='r', linestyle='--')
    
        axs[i].legend(loc='lower right')
        xlabel = {'L':'Length L [nm]', 'R': 'Radius R [nm]'}[x_param]
        axs[i].set_xlabel(xlabel)
        axs[i].set_ylabel(r'$V_3 [V]$')
        axs[i].set_title("Pinch off voltage V3 for QPC shape '{}'".format(data.p_geom.qpc_name[0]))
        axs[i].set_xscale('log')
    plt.subplots_adjust(hspace=0.5)
    plt.show
    
    
    
def compare_geom_abs(V_gates, vec, coordx, coordy, figsize, files, legend_label):
    # Getting the sparse vectors containing the data for a certain gate voltage
    
    section = [coordx,coordy,0]
    section[vec] = None
    
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        voltage_sparse_vec = data.voltage[index]
        
        # Making a cross section of the data
        coord_1D, voltage_1D = data_cross_section(coord, voltage_sparse_vec, section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        voltage_1D = voltage_1D[:,:,0][sort_indices]

        # Plotting the data
        plot_line(coord_1D, voltage_1D, p_geom, setting_x=vec, setting_y=0, figsize=figsize, ax=ax)
    ax.legend(legend_label)
    plt.show()
  
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        charge_sparse_vec = data.charge[index] 
        
        # Making a cross section of the data
        coord_1D, charge_1D = data_cross_section(coord, charge_sparse_vec, section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        charge_1D = charge_1D[:,:,0][sort_indices]

        # Plotting the data
        plot_line(coord_1D, charge_1D, p_geom, setting_x=vec, setting_y=2, figsize=figsize, ax=ax)
    ax.legend(legend_label)
    plt.show()


def compare_geom_rel(V_gates, vec, coordx, coordy, figsize, files, legend_label, ylimits = None):
    # Getting the sparse vectors containing the data for a certain gate voltage
       
    section = [[coordx,coordy,0]]
    section[0][vec] = None
    
    data_ref = data_handling.read_file(files[-1])
    coord = data_ref.coord
    V_gates_list_ref = data_ref.p_sim.V_gates_list
    index_ref = np.where(V_gates_list_ref == V_gates)[0][0]
    voltage_ref = data_ref.voltage[index_ref]
    charge_ref = data_ref.charge[index_ref]

    # Making a cross section of the data
    coord_ref, voltage_ref = data_cross_section(coord, voltage_ref, section)
    coord_ref, charge_ref = data_cross_section(coord, charge_ref, section)

    # Sort the data such that we don't get crossing lines through the figure
    sort_indices = np.argsort(coord_ref[:,0])
    coord_ref = coord_ref[:,0][sort_indices]
    voltage_ref = voltage_ref[:,0,0][sort_indices] 
    charge_ref = charge_ref[:,0,0][sort_indices]    
    
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files[:-1]:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        voltage_sparse_vec = data.voltage[index]
        
        # Making a cross section of the data
        coord_1D, voltage_1D = data_cross_section(coord, voltage_sparse_vec, section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        voltage_1D = voltage_1D[:,0,0][sort_indices]
        
        slice_index = int((len(coord_ref) - len(coord_1D))/2)
        if slice_index > 0:
            voltage_1D = voltage_1D / voltage_ref[slice_index : -slice_index]
        else:
            voltage_1D = voltage_1D / voltage_ref

        # Plotting the data
        plot_line(coord_1D, voltage_1D, p_geom, setting_x=vec, setting_y=3, figsize=figsize, ax=ax)

    ax.legend(legend_label)
    ax.set_ylim(ylimits)
    plt.show()
  
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files[:-1]:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        charge_sparse_vec = data.charge[index] 
        
        # Making a cross section of the data
        coord_1D, charge_1D = data_cross_section(coord, charge_sparse_vec, section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        charge_1D = charge_1D[:,0,0][sort_indices]

        slice_index = int((len(coord_ref) - len(coord_1D))/2)
        if slice_index > 0:
            charge_1D = charge_1D / charge_ref[slice_index : -slice_index]
        else:
            charge_1D = charge_1D / charge_ref
                
        # Plotting the data
        plot_line(coord_1D, charge_1D, p_geom, setting_x=vec, setting_y=4, figsize=figsize, ax=ax)
    
    ax.legend(legend_label)
    ax.set_ylim(ylimits)
    plt.show()
