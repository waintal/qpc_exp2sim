---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import kwant
import numpy as np
import matplotlib.pyplot as plt
from qpc_exp2sim.pescado_kwant.PescadoKwantProblem import ExactLdos
from qpc_exp2sim.pescado_kwant import PescadoKwantProblem as pkp
from qpc_exp2sim.pescado_kwant import plotting_pes
import scipy.constants as c
%matplotlib notebook
```

```python
def make_kwant_system(grid, L_kwant, W_kwant):
    # First, define the tight-binding system
    sys = kwant.Builder()

    # Here, we are only working with rectangular lattices
    lat = kwant.lattice.Monatomic(np.diag(grid), norbs=1)
    
    def scat_reg(pos):
        x,y = pos
        return -L_kwant/2 <= x <= L_kwant/2 and -W_kwant/2 <= y <= W_kwant/2
    
    ## On site hamiltonian
    sys[lat.shape(scat_reg, (0,0))] = 4

    ## Hopping in x and y direction
    sys[lat.neighbors(1)] = -1
    
    return sys.finalized()

sys = make_kwant_system([50, 50], 1600, 1600)

kwant.plot(sys)

center_tag = [np.array([0, 0]), np.array([10,0]), np.array([15, 0])]
where = lambda s: s.tag in center_tag
# make local vectors
vector_factory = kwant.kpm.LocalVectors(sys, where)

ldos = kwant.kpm.SpectralDensity(sys, 
                              num_vectors=None, 
                              num_moments=100,
                              vector_factory=vector_factory,
                              mean=False,
                              rng=0, 
                              accumulate_vectors=True)

energies, densities = ldos()

fermi = lambda E: kwant.kpm.fermi_distribution(energy=E, mu=0, temperature=0)
ldos.integrate(fermi)

ildos = []
mu_ = energies
for mu in mu_:
    fermi = lambda E: kwant.kpm.fermi_distribution(energy=E, mu=mu, temperature=0)
    ildos.append(ldos.integrate(fermi))

    
```

```python
fig, ax_ldos = plt.subplots()
ax_ldos.plot(energies, densities)
ax_ldos.legend([str(elem) for elem in center_tag])
    
fig, ax_ildos = plt.subplots()
ax_ildos.plot(mu_, ildos)
```

```python
coord_kwant = np.array([site.pos for site in sys.sites])
```

```python
exact_ldos = ExactLdos(ldos, potential = np.zeros(len(ldos._moments()[0])), conversion_unit= 1)
```

```python
# Evaluating the exact ldos
energy = np.linspace(0,8, 600)
ldos_val = exact_ldos(energy)

energy_site_1 = np.array([1,2,3])
ldos_val_site = exact_ldos.call_site_specific(energy=energy_site_1)

energy_site_2 = np.array([5,6,7])
origin, slope = exact_ldos.quantum_functional_kwant(mu = energy_site_2)

fig, ax_ldos = plt.subplots()
ax_ldos.plot(energy, ldos_val)
ax_ldos.scatter(energy_site_1, ldos_val_site)
ax_ldos.scatter(energy_site_2, slope/2)
```

```python
# Evaluating the exact ildos
energy = np.linspace(0,8, 600)
index_site = np.array([0,2])
ildos_val = exact_ldos.integrate(energy, index=index_site)

energy_site_1 = np.array([1,3])
ildos_val_site = exact_ldos.integrate_site_specific(energy = energy_site_1, index=index_site)

energy_site_2 = np.array([5,6,7])
origin, slope = exact_ldos.quantum_functional_kwant(mu = energy_site_2)

fig, ax_ildos = plt.subplots()
ax_ildos.plot(energy, ildos_val)
ax_ildos.scatter(energy_site_1, ildos_val_site)
ax_ildos.scatter(energy_site_2, (origin + energy_site_2 * slope)/2)
```

```python
vector_factory.__dict__
```

```python
len(sys.sites)
```

```python

```
