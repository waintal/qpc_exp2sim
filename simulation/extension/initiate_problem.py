#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 12:14:44 2022

@author: thellemans
"""
from mpi4py import MPI
import sys
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.simulation import solver

data = dh.read_file(sys.argv[1], cluster=True)

p_geom = data.p_geom
p_sim = data.p_sim

print(sys.argv[1])
print(p_geom.__dict__)
print(p_sim.__dict__)

# Initiate the problem
solver.initiate_problem(p_geom, p_sim, comm=MPI.COMM_WORLD)

