#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  4 11:06:07 2022

@author: thellemans
"""
from mpi4py import MPI
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.simulation import builder
import sys
import kwant
import kwantspectrum
import tkwant
from tkwant import leads, manybody as mb
import time
import scipy.constants as c
import functools


# Setting the default for serialization to dill instead of pickle
# This is still necesarry for the kwant_sys (onsite potential function)
import dill
MPI.pickle.__init__(dill.dumps, dill.loads)

#-------------------- enable logging --------------------------------
import logging
tkwant.logging.level = logging.INFO
#--------------------------------------------------------------------

# Use default communicator from tkwant
comm = tkwant.mpi.get_communicator()


# Read the data file & recreate the kwant system on rank 0
if comm.rank == 0:

    data = dh.read_file(sys.argv[1], cluster=True)
    
    # unpacking the data
    p_geom = data.p_geom
    p_sim = data.p_sim
    coord = data.coord
    voltage = data.voltage
        
    print(sys.argv[1])
    print(p_geom.__dict__)
    print(p_sim.__dict__)
    
    kwant_sys = builder.make_kwant_system(p_geom)
    
    density_operator = kwant.operator.Density(kwant_sys)
    occup = mb.lead_occupation(chemical_potential=0, temperature=0) 
    
    # Get the kwnat_indices in rpp_problem format
    kwant_indices = builder.get_kwant_indices_rpp(kwant_sys, coord)
    
    t=p_geom.t
else:
    voltage = None
    kwant_sys = None
    density_operator = None
    occup = None
    kwant_indices = None
    t = None
    

def mpi_print(comm, *message):
    if comm.rank == 0:
        print(*message)
        
# Broadcast the necessary variables to the other ranks:
t, voltage, kwant_sys, density_operator, occup, kwant_indices = comm.bcast(
        (t, voltage, kwant_sys, density_operator, occup, kwant_indices), root=0)

# Do the tkwant simulation:
tkwant_density = []
tkwant_error = []

tol = 1e-5 
order = 20
number_subintervals = 1
quadrature = 'kronrod'

mpi_print(comm, 'tol: ', tol)
mpi_print(comm, 'order: ', order)
mpi_print(comm, 'number_subintervals', 1)
mpi_print(comm, 'quadrature', quadrature)

for index, voltage_elem in enumerate(voltage):
    if index ==0:
        mpi_print(comm, 'For loop starts')
    
    start = time.time()
    
    potential_elem = voltage_elem * (c.elementary_charge/t)
    
    params = {'potential': potential_elem, 'kwant_indices': kwant_indices}
    
    mpi_print(comm, 'Initialisation intervals')
    start = time.time()

    # calculate the spectrum E(k) for all leads
    spectra = kwantspectrum.spectra(kwant_sys.leads, params=params)
    
    # estimate the cutoff energy Ecut from T, \mu and f(E)
    # All states are effectively empty above E_cut
    occupations = mb.lead_occupation(chemical_potential=0, temperature=0)
    emin, emax = mb.calc_energy_cutoffs(occupations)
    
    # define boundary conditions
    bdr = leads.automatic_boundary(spectra, tmax=1, emin=emin, emax=emax)
    
    # calculate the k intervals for the quadrature
    interval_type = functools.partial(mb.Interval, order=order,
                                      quadrature=quadrature)
    intervals = mb.calc_intervals(spectra, occupations, interval_type)
    intervals = mb.split_intervals(intervals, 
                                   number_subintervals=number_subintervals)
    end = time.time()
    mpi_print(comm, 'Initialisation intervals: {} s'.format(end-start))
    
    mpi_print(comm, 'Calculating state')
    start = time.time()
    state = tkwant.manybody.State(kwant_sys, tmax=1, intervals=intervals, 
                                  params=params, error_op=density_operator, 
                                  refine=False)
    end = time.time()
    mpi_print(comm, 'Calculating state: {} s'.format(end-start))
    
    mpi_print(comm, 'Refining')
    start = time.time()
    state.refine_intervals(rtol=tol, atol=tol)
    end = time.time()
    mpi_print(comm, 'Refining: {} s'.format(end-start))

    mpi_print(comm, 'Evaluating')
    start = time.time()
    tkwant_density.append(state.evaluate(density_operator))
    end = time.time()
    mpi_print(comm, 'Evaluating: {} s'.format(end-start))

    mpi_print(comm, 'Estimating error')
    start = time.time()
    tkwant_error.append(state.estimate_error())
    end = time.time()
    mpi_print(comm, 'Estimating error: {} s'.format(end-start))

    end = time.time()
    if comm.rank == 0:
        print('{}% ready, last step took {} s'.format(100*(index+1)/len(voltage), end-start))
    
if comm.rank == 0:
    data.tkwant_density = tkwant_density
    data.tkwant_error = tkwant_error
    data.intervals = state.get_intervals()
    
    # Write the necessary output to a file that can be interpreted later on
    input_file = sys.argv[1][sys.argv[1].rfind('/')+1:]
    dh.write_file(data, 'data_files/' + input_file +'_tkwant_' + str(order) +
                  '_' + str(number_subintervals) + '_' + str(tol), 
                  cluster=True)#'_grid{}'.format(p_geom.grid_fine[0]))
