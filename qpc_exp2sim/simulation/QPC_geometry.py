#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 11:46:43 2022

@author: thellemans

This file allows to recreate the shapes of several quantum point contacts (qpcs)
as described in the Eleni paper
"""

from pescado.mesher import shapes
from ..pescado_kwant.extension_pes import extrude, reflect
import numpy as np

qpc_data = {'a1' : {'W':250, 'L':50, 'V3_exp':[-2.1,-1.95,-2.2,-2], 'V3_sim_el':[-1.90830281,-1.94459188]},
            'a2' : {'W':300, 'L':100, 'V3_exp':[-2.09,-1.87,-2.02,-2.13], 'V3_sim_el':[-1.81780047,-1.82911216]},
            'a3' : {'W':300, 'L':250, 'V3_exp':[-1.41,-1.29,-1.52,-1.47], 'V3_sim_el':[-1.29816562,-1.29953884]},
            'a4' : {'W':300, 'L':500, 'V3_exp':[-1.22,-1.17,-1.22,-1.24], 'V3_sim_el':[-1.09796157,-1.09949198]},
            'a5' : {'W':500, 'L':1000, 'V3_exp':[-1.96,-1.84,-2.05,-1.97], 'V3_sim_el':[-1.69915153,-1.698]},
            'a6' : {'W':500, 'L':2500, 'V3_exp':[-1.46,-1.82,-1.97,-1.9], 'V3_sim_el':[-1.78266967,-1.78926685]},
            'a7' : {'W':500, 'L':5000, 'V3_exp':[-1.83], 'V3_sim_el':[-1.68746914]},
            'a8' : {'W':500, 'L':1e4, 'V3_exp':[-1.79], 'V3_sim_el':[-1.67901758]},
            'b1' : {'W':250, 'R':25, 'V3_exp':[-1.94,-2.43], 'V3_sim_el':[-1.93423187]},
            'b2' : {'W':300, 'R':50, 'V3_exp':[-2.35,-2.42], 'V3_sim_el':[-2.01052887]},
            'b3' : {'W':300, 'R':125, 'V3_exp':[-1.71,-1.77,-1.59], 'V3_sim_el':[-1.51401939]},
            'b4' : {'W':300, 'R':250, 'V3_exp':[-1.51,-1.57], 'V3_sim_el':[-1.30217845]},
            'b5' : {'W':500, 'R':500, 'V3_exp':[-1.71,-2.49], 'V3_sim_el':[-1.98362764]},
            'b6' : {'W':500, 'R':1250, 'V3_exp':[-1.98,-2.2], 'V3_sim_el':[-1.79580212]},
            'b7' : {'W':500, 'R':2500, 'V3_exp':[-1.97,-2.08,-2], 'V3_sim_el':[-1.79315761]},
            'b8' : {'W':500, 'R':5000, 'V3_exp':[-1.93,-2], 'V3_sim_el':[-1.67981459]}}


def disk(center, R, interval):
    """
    Parameters
    ----------
    centre : list [x,y]
        centre of the disk in 2D
    R: int or float 
        radius disk
    interval: list [min_val, max_val]
        determines thickness and location qpc layer

    Returns
    -------
    shapes instance
    """
    return extrude(shapes.Ellipsoid(center, [R]*2), 2, interval)


def right_angled_triangle(center, L1, L2):
    """
       | \
       |    \
    L2 |       \
       |          \
     center__________\
             L1
    """
    
    bbox = np.array([[center[0], center[1]], [center[0]+L1, center[1]+L2]])
    
    def func(pos_list):
        check =  []
        for pos in pos_list:
            x, y = pos
            x -= center[0]
            y -= center[1]
            check.append(0 <= x <= L1 and 0 <= y <= L2 * (1 - x/L1))
        return check
    
    return shapes.General(func, bbox)


def qpc_a_helper(center, W, L, t, interval):
    """
    Parameters
    ----------
    center: list [x,y]
        point from which the qpc starts
    W: int or float
        width channel between qpc's
    L: int or float
        length channel between qpc's 
    t: int or float
        width of the qpc line
    interval: list [min_val, max_val]
        determines thickness and location qpc layer
    
    Returns
    -------
    shapes instance
    """
    horizontal = shapes.Box(lower_left=[-L/2, W/2], 
                            size = [L,t])

    joint = [L/2 - 25, W/2 + t/2]
    length_connection = 25 + np.sqrt((joint[0]-center[0])**2 + (joint[1] - center[1])**2)
    angle_connection = np.arctan2(joint[1] - center[1], joint[0] - center[0])
    
    connection = shapes.Box(lower_left=[center[0] - 25, center[1]- 25], 
                            size=[length_connection, 50])
    
    connection = shapes.rotate(connection, angle_connection, axis=[0,0,1], origin=center)
    
    shape_2D = horizontal | connection
    
    # Extrude the 2D shape along the z direction & return
    return extrude(shape_2D, 2, interval)


def qpc_b_helper(center, W, R, alpha, t, interval):
    """
    Parameters
    ----------
    center : list [x,y]
        point from which the qpc starts.
    W: int or float
        width channel between qpc's
    R : int or float
        radius curvature qpc
    t: int or float
        width of the qpc line
    alpha: float
        angle in radians
    interval: list [min_val, max_val]
        determines thickness and location qpc layer

    Returns
    -------
    shapes instance
    """
    L = 2*R*np.sin(alpha/2)
    
    center_curve = [0, W/2 + R]
    outer_curve = shapes.Ellipsoid(center_curve, [R]*2)
    inner_curve = shapes.Ellipsoid(center_curve, [R-t]*2)
    
    full_curve = outer_curve - inner_curve

    triangle_1 = right_angled_triangle([center_curve[0], center_curve[1]-R], L, R)
    triangle_2 = reflect(triangle_1, [0,0], [1,0])
    triangle = triangle_1 | triangle_2
    
    curve = shapes.Intersection([full_curve, triangle])
    
    joint = [center_curve[0] + np.sin(alpha/2)*(R-t/2) - np.cos(alpha/2)*25,
             center_curve[1] - np.cos(alpha/2)*(R-t/2) - np.sin(alpha/2)*25]
    length_connection = 25 + np.sqrt((joint[0]-center[0])**2 + (joint[1] - center[1])**2)
    angle_connection = np.arctan2(joint[1] - center[1], joint[0] - center[0])
    
    connection = shapes.Box(lower_left=[center[0] - 25, center[1]- 25], 
                            size=[length_connection, 50])
    
    connection = shapes.rotate(connection, angle_connection, axis=[0,0,1], origin=center)
    
    shape_2D = curve | connection
    
    # Extrude the 2D shape along the z direction & return
    return extrude(shape_2D, 2, interval)


def qpc_c_helper(center, W, L, R, alpha, t, interval):
    """
    Parameters
    ----------
    center : list [x,y]
        point from which the qpc starts.
    W: int or float
        width channel between qpc's
    L: int or float
        length channel between qpc's 
    R : int or float
        radius curvature qpc
    t: int or float
        width of the qpc line
    interval: list [min_val, max_val]
        determines thickness and location qpc layer

    Returns
    -------
    shapes instance
    """
    
    # CURRENT PROBLEM: NO CENTRAL ANGLE DEFINED
    
    # Extrude the 2D shape along the z direction
    return #extrude(shape_2D, 2, interval)


def qpc(name, center, interval):
    """
    Parameters
    ----------
    name: str
        name of the qpc geometry according to Eleni paper
        
    center : list [x,y]
        point from which the qpc starts.

    interval: list [min_val, max_val]
        determines thickness and location qpc layer

    Returns
    -------
    shapes instance
    """
    t = 80
    
    assert name in qpc_data.keys() or name == 'full_gate', \
    "Please enter a valid qpc name. '{}' is not valid".format(name)
    
    ######################### Special cases ########################
    if name == 'full_gate':
        shape_2D = shapes.Box(lower_left = [-500, -500], 
                              size = [1000, 1000])
        return extrude(shape_2D, 2, interval)

    ########################## QPC series A ########################
    elif name == 'a1':
        shape_2D = shapes.Box(lower_left =  [-25, 125],
                              size = [50, center[1]-125])
    elif name == 'a2':
        block_2D = shapes.Box(lower_left =  [-50, 150],
                              size = [100, 80])
        connection_2D = shapes.Box(lower_left =  [-25, 230],
                              size = [50, center[1]-230])
        shape_2D = block_2D | connection_2D
    elif name in {'a3', 'a4', 'a5', 'a6', 'a7', 'a8'}:
        # This should be read from an external csv file
        L = qpc_data[name]['L']
        W = qpc_data[name]['W']
        return qpc_a_helper(center, W, L, t, interval)
    
    ########################## QPC series B ########################
    elif name == 'b1':
        connection_2D = shapes.Box(lower_left =  [-25, 150],
                                   size = [50, center[1]-150])
        disk_2D = shapes.Ellipsoid([0,150], [25]*2)
        shape_2D = connection_2D | disk_2D
    elif name == 'b2':
        connection_2D = shapes.Box(lower_left =  [-25, 200],
                                   size = [50, center[1]-200])
        disk_2D = shapes.Ellipsoid([0,200], [50]*2)
        shape_2D = connection_2D | disk_2D
    elif name == 'b3':
        connection_2D = shapes.Box(lower_left =  [-25, 275],
                                   size = [50, center[1]-275])
        disk_2D = shapes.Ellipsoid([0,275], [125]*2)
        bbox_half_disk_2D = shapes.Box(lower_left=[-125, 150], size = [250, 125])
        half_disk_2D = shapes.Intersection([bbox_half_disk_2D, disk_2D])
        shape_2D = connection_2D | half_disk_2D
    elif name in {'b4', 'b5', 'b6', 'b7', 'b8'}:
        # This should be read from an external csv file
        alpha = 2*np.pi/3
        W = qpc_data[name]['W']
        R = qpc_data[name]['R']

        return qpc_b_helper(center, W, R, alpha, t, interval)
    
    ########################## QPC series C ########################
    
    #################### Return extruded qpc shape #################
    return extrude(shape_2D, 2, interval)