---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim as plotting
import qpc_exp2sim.simulation.builder as s
import numpy as np
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from types import SimpleNamespace
from scipy import constants
import matplotlib.pyplot as plt
from pescado.mesher import shapes
from pescado import tools
```

```python
data_po = dh.read_file('system_1D/po_v1')

p_geom_po = data_po.p_geom
p_sim_po = data_po.p_sim
V_gates_list_po = p_sim_po.V_gates_list
coord_po = data_po.coord
voltage_po = data_po.voltage
charge_po = data_po.charge
voltage_shift_po = data_po.voltage_shift
V3_po = data_po.V3

data_sc = dh.read_file('system_1D/sc_v1')

p_geom_sc = data_sc.p_geom
p_sim_sc = data_sc.p_sim
V_gates_list_sc = p_sim_sc.V_gates_list
coord_sc = data_sc.coord
voltage_sc = data_sc.voltage
charge_sc = data_sc.charge
voltage_shift_sc = data_sc.voltage_shift
V3_sc = data_sc.V3
```

## Input paramaters

```python
print('The fermi wavelength: lambda_f = {} nm'.format(np.sqrt(2*np.pi/2.8e15) * 1e9))
display(p_geom_po.__dict__)
display(p_geom_sc.__dict__)
```

## Geometry of the device

```python
# Defining the several shape objects relevant for the problem
gates = s.gates_shape(p_geom_po)
DEG = s.DEG_shape(p_geom_po)
GaAs = s.layer0_shape(p_geom_po) | s.layer4_shape(p_geom_po)
AlGaAs_undoped = s.layer1_shape(p_geom_po) | s.layer3_shape(p_geom_po)
AlGaAs_doped = s.layer2_shape(p_geom_po)
device = s.device_shape(p_geom_po)
    
# Plotting shapes for control
plotting.plot_shape(shapes_list=[GaAs, AlGaAs_doped, AlGaAs_undoped, DEG, gates], 
                    colors=['g', 'y', 'r', 'b','k'], 
                    grid_cell = p_geom_po.grid_fine, 
                   legend_label = ['GaAs', 'AlGaAs doped', 'AlGaAs undoped', '2DEG', 'Gate'], 
                   figsize=10)
```

## Voltage and charge results (only for poisson problem -> non self consistent!)
Voltage and charge can not be plotted in function of z for the self consistent case since the self consistent solver only return these values for the flexible indices, i.e. the 2DEG.

```python
widget_v = widgets.Dropdown(options=V_gates_list_po, description ='V_gates:' )
widget_figsize = widgets.IntSlider(value=4, min=0, max=15, step=1, continuous_update=False, description='Figure size:')
```

```python
%matplotlib notebook
def f(V_gates, figsize):
    # Getting the sparse vectors containing the data for a certain gate voltage
    index = np.where(V_gates_list_po == V_gates)[0][0]
    voltage_sparse_vec = voltage_po[index]
    charge_sparse_vec = charge_po[index]     
        
    coord_1D, voltage_1D = plotting.data_cross_section(coord_po, voltage_sparse_vec)
    coord_1D, charge_1D = plotting.data_cross_section(coord_po, charge_sparse_vec)
    
    # Sort the data such that we don't get crossing lines through the figure
    sort_indices = np.argsort(coord_1D[:,0])
    coord_1D = coord_1D[sort_indices]
    voltage_1D = voltage_1D[sort_indices]
    charge_1D = charge_1D[sort_indices]
        
    print("{:.2e}".format(np.max(charge_1D)), "{:.2e}".format(np.min(charge_1D)))
    
    # Plotting the data
    plotting.plot_line(coord_1D, voltage_1D, setting_x=2, setting_y=0, p=p_geom_po, figsize=figsize)
    plotting.plot_line(coord_1D, charge_1D, setting_x=2, setting_y=2, p=p_geom_po, figsize=figsize)


interact(f, V_gates=widget_v, 
            figsize = widget_figsize)
```

```python
%matplotlib notebook
points = [0]
labels = ['poisson', 'self consistent']
coord, voltage_0D_po = plotting.data_cross_section(coord_po, voltage_po, points)
coord, charge_0D_po = plotting.data_cross_section(coord_po, charge_po, points)

coord, voltage_0D_sc = plotting.data_cross_section(coord_sc, voltage_sc, points)
coord, charge_0D_sc = plotting.data_cross_section(coord_sc,charge_sc, points)

plotting.plot_line(V_gates_list_po, np.stack((voltage_0D_po, voltage_0D_sc), axis=-1), p=p_geom_po, setting_x=3, setting_y=0, vline=False, legend_label = labels, figsize=4)
plotting.plot_line(V_gates_list_sc, np.stack((-charge_0D_po, charge_0D_sc), axis=-1), p=p_geom_sc, setting_x=3, setting_y=2, vline=False, legend_label = labels, figsize=4)
```
