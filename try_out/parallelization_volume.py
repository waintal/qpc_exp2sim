# -*- coding: utf-8 -*-
"""
Created on Sun Jun  5 17:15:49 2022

@author: helle
"""

import numpy as np

from pescado import mesher
import functools
import copy
#import collections

# Below in:
# self._update_voronoi_neighbour(
#    index=index, vor_cell=vor_cell, sub_mesh_number=None)
# something weird happens, sub_mesh_number is normally not an 
# argument of _update_voronoi_neighbour, but without setting it
# python gives an erros


def voronoi_property(indices, mesh_inst, _property, neigs=None):
    print(indices, _property, neigs)
    return mesh_inst._voronoi_property(indices, _property, neigs)

def volume_fork(mesh_inst, indices):
    print(indices)
    print(mesh_inst.npoints)
    import os
    import multiprocessing

    # Identical behaviour in linux or windows
    npros = os.cpu_count()
    
    print(npros)
    
    with multiprocessing.get_context().Pool(processes=npros) as pool:

        volume = np.concatenate(pool.map(
            functools.partial(
                voronoi_property,
                mesh_inst=mesh_inst,
                _property='volume', 
                neigs=None),
            np.split(
                np.arange(0, mesh_inst.npoints),
                np.arange(
                    mesh_inst.npoints // npros,
                    mesh_inst.npoints,
                    mesh_inst.npoints // npros)))) 
    return volume


class Mesh_copy(mesher.mesh.Mesh):
    
    def sub_mesh_number(self, indices):
        ''' Return the sub meshes number to which the index in 'indices' belong to

        Parameters
        -----------
        indices: sequence of integers
            The index of the meshes points

        Returns
        --------
        numpy.array of integers with shape(m, )
            with m = len(indices)

        '''

        indices = np.asarray(indices)
        sorting_map = np.argsort(indices)

        meshes_num = np.searchsorted(
            self.sub_meshes_cumul_size - 1, indices[sorting_map]) - 1
        return meshes_num[np.argsort(sorting_map)]


    def tag(self, indices, return_pattern=True):
        ''' Return a the (tag, pattern) tuple associated with a given index

        Parameters
        -----------
        indices: sequence n of integers
            The index of the meshes points

        return_pattern: boolean, default True
            If True also returns which pattern the index belongs to

        Returns
        --------
        list of of n tuples with 2 elements:
            1st: tag in the sub mesh
            2nd: sub mesh pattern
        '''

        indices = np.asarray(indices)
        meshes_num = self.sub_mesh_number(indices)

        tag_list = np.empty((len(indices),), dtype=object)
        # TODO: Implement this more efficiently
        for i, ele in enumerate(meshes_num):
            pts = (indices[i] - (self.sub_meshes_cumul_size[ele]))

            info = self.sub_meshes[ele][0][pts]

            if return_pattern:
                info = (info, self.sub_meshes[ele][1])

            tag_list[i] = info

        return tag_list


    def coordinates(self, indices=None):
        ''' Return a the real space coordinates for a given index

        Parameters
        -----------
            indices: sequence of integers
            The index of the meshes points
        '''
        indices = np.asarray(indices)
        meshes_num = self.sub_mesh_number(indices)
        coordinates = np.empty((len(indices), self.dim), dtype=float)

        # TODO: Implement this more efficiently
        unique_ele = np.unique(meshes_num)

        for ele in unique_ele:
            pts = indices[meshes_num == ele] - (self.sub_meshes_cumul_size[ele])
            coordinates[meshes_num == ele, :] = self.sub_meshes[ele][1](
                self.sub_meshes[ele][0][pts])

        return coordinates


    def volume(self, indices):
        ''' Returns the volume betwen 'point' and 'neig'

        Parameters
        -----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the volume

        neigs: list of interger or list of n lists of interger - default None

            Index of the fneig of 'indices' from which to find
            the separating volume.

            If indices is a int -> neig is a list of integers
            If indices is a list -> neig is a list of list of integers

            Default behavior is to find all separating surfaces. The volume
            indexing uses the same order as the one in 'self.neighbours'

        Returns
        -------

        list or list of n floats
        '''
        return self._voronoi_property(
            indices=indices, _property='volume')


    def _voronoi_property(self, indices, _property, neigs=None):
        ''' Internal function to recover a information about
        the local voronoi cell of 'indices'

        Parameters
        -----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the surface

        _property: str
            'surface', 'distance', 'riridgedges', 'volume'

        neigs: list of interger or list of n lists of interger - default None

            Index of the fneig of 'indices' from which to find
            the separating surface.

            If indices is a int -> neig is a list of integers
            If indices is a list -> neig is a list of list of integers

            Default behavior is to find all separating surfaces. The surface
            indexing uses the same order as the one in 'self.neighbours'

        Returns
        -------

        list or list of n floats

        '''
        print(indices)
        print(_property)
        
        vor_cell, neigs_list = self.voronoi(indices=indices)
        values = [[] for i in range(len(vor_cell))]

        for i, (cell, neig_idx) in enumerate(zip(vor_cell, neigs_list)):

            # Always in the same order as self.neighbours[index]
            if neigs is None:
                order_neig = np.arange(len(neig_idx))
            else:
                order_neig = neig_idx.searchsorted(np.sort(neigs[i]))

                if len(order_neig) != len(neigs[i]):
                    wrong_neig = np.setdiff1d(
                        neigs[i], neig_idx)
                    raise ValueError(
                        '{0} do not share a surface with {1}'.format(
                            wrong_neig, indices[i]))

            if _property == 'volume':
                values[i] = cell.volume
            elif _property == 'surface':
                values[i] = cell.surface(neig_idx=order_neig)
            elif _property == 'distance':
                values[i] = cell.distance(neig_idx=order_neig)
            elif _property == 'ridge':
                values[i] = [
                    cell.vertices[cell.ridges[neig]]
                    for neig in order_neig]
            else:
                raise ValueError('Can"t find property {0}'.format(
                    _property))

        return values


    def _pattern_voronoi(self, indices):
        ''' Return the local voronoi cell given by the pattern
        for each index in 'indices'.

        Parameters
        -----------

        indices: interger or sequence n of interger

        Returns
        --------
        instace of mesher.voronoi.VoronoiCell or a sequence of
        such instances
        '''

        vor_list = list()

        for index in indices:
            sub_mesh_number = self.sub_mesh_number([index, ])[0]
            pat = self.sub_meshes[sub_mesh_number][1]
            vor_list.append(
                pat.voronoi(self.sub_meshes[sub_mesh_number][0][
                    index - (self.sub_meshes_cumul_size[sub_mesh_number])]))

        return vor_list

    
    def _voronoi(self, indices):
        ''' Return the local voronoi cell with the neighbour indices
        updated to match the global mesh indexing

        Paramters
        ----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the voronoi cell

        Returns
        -------

        list or list of n instances of pescado.mesher.voronoi.VoronoiCell
        '''

        vor_cell_list = [[] for i in range(len(indices))]
        pat_vor_cell_list = self._pattern_voronoi(indices=indices)

        for i, index in enumerate(indices):

            vor_cell = pat_vor_cell_list[i]

            # neig ordering and index updating
            (vor_cell.neighbours_tags, vor_cell.neighbours,
             vor_cell.ridges) = self._update_voronoi_neighbour(
                 index=index, vor_cell=vor_cell, sub_mesh_number=None)

            # Update index
            vor_cell.point_tag = index
            vor_cell_list[i] = copy.deepcopy(vor_cell)

        del pat_vor_cell_list
        return vor_cell_list


    def voronoi(self, indices, return_neig=True):
        ''' Return a the real space coordinates for a given index

        Paramters
        ----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the voronoi cell

        return_neig = boolean (optional) - default is False
            If True returns the neighbours index for the voronoi cell

        Returns
        -------

        vor_cell_list: list or list of n instances of pescado.mesher.voronoi.VoronoiCell

        -- if return_neig is True, then also returns :

        neig_list : np.ndarray or list of np.ndarray of floats with shape(n, )
                with n the number of first neighbours for the corresponding
                voronoi cell in the list.
        '''

        vor_cell_list = [[] for i in range(len(indices))]
        neig_list = [[] for i in range(len(indices))]

        for i, vor_cell in enumerate(self._voronoi(indices=indices)):
            neig_list[i] = vor_cell.neighbours_tags

            del vor_cell.neighbours_tags
            del vor_cell.point_tag

            vor_cell_list[i] = vor_cell

        if return_neig:
            return vor_cell_list, neig_list
        else:
            return vor_cell_list
