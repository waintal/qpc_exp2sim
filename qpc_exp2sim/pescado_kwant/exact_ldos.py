#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 11:39:27 2022

@author: thellemans
"""
import numpy as np
from numpy.polynomial.chebyshev import chebval

from .extension_pes import indices_from_coordinates

class ExactLdos:
    
    def __init__(self, spectral_density, indices=None, potential=None, 
                 conversion_unit=None):
        """
        
        Parameters
        ----------
        indices : float or sequence of floats
            Pescado indices for which the spectral density is defined. The 
            indices should be provided in the same order as the moments 
            are ordered within the spectral_density.
            Shape (n,)
        
        spectral_density : kwant.kpm.SpectralDensity
            Spectral density including m moments for n vectors.
            
        conversion_unit : float, optional
            Provides the conversion from Pescado voltages to Kwant energy
            (kwant_energy = pescado_voltage * conversion_unit).
            e.g. when the kwant energy is normalized with respect to `t` 
            this would be equal to scipy.constants.elementary_charge/t.
            Only necessary when the quantum functional needs to be calculated
            later on.
        
        potential : sequence of floats, optional
            The potential (kwant energy units) for which the spectral_density
            is calculated. Serves a reference for when the quantum functional
            is evaluated at a certain voltage. Shape (n, )
            Only necessary when the quantum functional needs to be calculated
            later on.
        """        
        moments = spectral_density._moments()
        
        if indices is None:
            indices = np.arange(len(moments[0]))
        elif isinstance(indices, int):
            indices = np.array([indices], dtype=int)
        else:
            assert len(indices) == len(moments[0]), (
                'The same number of indices as the number of vectors in kpm'
                'should be provided.') 
            indices = np.asarray(indices, dtype=int)
        
        # We will sort the indices and moments according to Pescado.
        sorting = np.argsort(indices)
        self.indices = indices[sorting]
        self.moments = moments[:,sorting]
        
        # Save the parameters for rescaling the energy
        self.a = spectral_density._a
        self.b = spectral_density._b
        
        # Save the parameters that link kwant and pescado
        if potential is not None:
            potential = np.asarray(potential)[sorting]

        self.potential = potential
        self.conversion_unit = conversion_unit
    
    
    def _local_index(self, index):
        """Obtain local index from pescado index (to be able to index arrays)
        """
        if isinstance(index, int):
            index = np.array([index])
        else:
            index = np.asarray(index)

        local_index = indices_from_coordinates(self.indices[:,None], 
                                               index[:,None])
        
        # To avoid faulty behaviour we raise an error if certain indices
        # are not defined.
        if -1 in local_index:
            raise Exception("The following indices are not defined in the"
                             " exact ldos: {}.".format(
                             index[np.where(local_index == -1)]))
        return local_index
    
    
    def delete(self, index):
        """Delete the ExactLdos on certain indices.
        """
        local_index = self._local_index(index)
        
        self.indices = np.delete(self.indices, local_index)
        self.potential = np.delete(self.potential, local_index)
        self.moments = np.delete(self.moments, local_index, axis=1)
        
    
    def extend(self, index, moments, potential):
        """Extend the ExactLdos for new indices.
        
        Note
        ----
        Old data is overwritten if you provide new data for a certain index.
        """
        if isinstance(index, int):
            index = np.array([index], dtype=int)
        else:
            index = np.asarray(index, dtype=int)
        
        # Find the indices present in 'self.indices' that are not in 'index'
        index_old = np.setdiff1d(self.indices, index)
        index_old_local = indices_from_coordinates(self.indices[:,None], 
                                                   index_old[:,None])
        
        self.indices = np.concatenate([index_old, index]).astype(int)
        sorting = np.argsort(self.indices)
        
        self.indices = self.indices[sorting]
        self.moments = np.hstack(
                [self.moments[:,index_old_local], moments])[:,sorting]
        
        self.potential = np.concatenate(
                [self.potential[index_old_local], potential])[sorting]
    
    
    def _moments(self, index=None):
        """Filter the moments for the asked indices
        """
        if index is None:
            return self.moments
        else:
            if isinstance(index, (int, np.int64, np.int32)):
                return self.moments[:, np.where(self.indices == index)[0]]
            else:
                index = np.asarray(index)
                local_indices = indices_from_coordinates(self.indices[:,None], 
                                                         index[:,None])
                
                # To avoid faulty behaviour we raise an error if certain 
                # indices are not defined.
                if -1 in local_indices:
                    raise Exception("The following indices are not defined in "
                                     "the exact ldos: {}.".format(
                                     index[np.where(local_indices == -1)]))
                
                return self.moments[:,local_indices]
    
    
    def __call__(self, energy=0, index=None):
        """Exact evaluation of the ldos at zero temperature.
        
        Parameters
        ----------
        energy : float or sequence of floats
            Energy at which the ldos has to be evaluated.
            
        index : int or sequence of ints, optional
            Pescado indices for which the ldos has to be evaluated.
            
        Returns
        -------
        densities : float or array of floats
            single ``float`` if the ``energy`` parameter is a single
            ``float``, else an array of ``float``.
        """
        # Rescale energy.
        energy = np.asarray(energy)            
        energy_scaled = (energy - self.b) / self.a

        # Obtain necessary moments from the requested indices.
        moments = self._moments(index)
        
        # Factor 2 comes from the norm of the Chebyshev polynomials.
        factor = np.zeros(len(self.moments))[:,None] + 2
        factor[0] = 1        
        g_e = (np.pi * np.sqrt(1 - energy_scaled) * np.sqrt(1 + energy_scaled))
        
        return np.transpose(chebval(energy_scaled, factor*moments) / g_e)
    
    
    def _F(self, e, moments):
        """Exact primitive function of the chebyshev expansion of the ldos.
        
        Evaluate the primitive function of the LDOS at a speficic energy for 
        every vector in the moments.
        
        Parameters
        ----------
        e : np.ndarray or float
            Energy at which the primitive function has to be evaluated.
            Rescaled value!
        
        moments : np.ndarray
            As provided by kpm: SpectralDensity._moments(),  shape (m,n)
            m = number of moments
            n = number of vectors
            
        """
        n = np.arange(1,len(moments))[:,None]
        
        return -(np.outer(np.arccos(e), moments[0]) + 
                 2* (np.matmul(np.sin(np.outer(np.arccos(e), n)), 
                               moments[1:]/n))
                 )/np.pi
    
    
    def integrate(self, energy=0, index=None):
        """ Exact integration of the ldos at zero temperature.
        
        This is the equivalent of SpectralDensity.integrate, but with exact 
        integration of the Chebyshev expansion of the ldos at zero temp instead 
        of using Chebyshev-Gauss quadrature. This avoids a stepwise result.
        
        Parameters
        ----------
        energy : float or sequence of floats
            Fermi energy, the energy up to which the ldos has to be integrated.
            
        index : int or sequence of ints, optional
            Pescado indices for which the integral has to be evaluated.
            
        Returns
        -------
        ildos : np.ndarray
            shape (p, n) with p the number of energies and n the number of 
            vectors/indices.
        
        """
        # Rescale energy.
        energy = np.asarray(energy)
        energy_scaled = (energy - self.b) / self.a
        
        # Obtain necessary moments from the requested indices.
        moments = self._moments(index)
    
        return self.a *(self._F(energy_scaled, moments) - self._F(-1, moments))


    def call_site_specific(self, energy, index=None):
        """
        special call to the ldos in which energy is an array with the energies
        for each vector. This means that not all sites are evaluated at the 
        same energy
        """
        # Rescale energy.
        energy = np.asarray(energy)
        energy_scaled = (energy - self.b) / self.a
    
        # Obtain necessary moments from the requested indices.
        moments = self._moments(index)
    
        # Factor 2 comes from the norm of the Chebyshev polynomials
        factor = np.zeros(len(self.moments)) + 2
        factor[0] = 1
        g_e = (np.pi * np.sqrt(1 - energy_scaled) * np.sqrt(1 + energy_scaled))
        
        
        return np.array([chebval(energy_scaled[i], factor*moments[:,i]) 
                         for i in range(len(moments[0]))])/ g_e


    def _F_site_specific(self, e, moments):
        """
        Evaluate the primitive function of the LDOS at a speficic energy for every
        vector in the moments.
        
        e: np.ndarray or float
            Energy up to which has to be integrated, can be different for different
            vectors by using a np.ndarray. If np.ndarray shape (n,) with n the 
            number of vectors.
            Rescaled value!
        
        moments: np.ndarray
            As provided by kpm: SpectralDensity._moments(),  shape (m,n)
            m = number of moments
            n = number of vectors
            
        n: np.ndarray
            Always: np.arange(1,len(moments))[:,None]
            
        Returns
        -------
        
        . : np.ndarray
            shape (n,) --> correct ? 
        """
        n = np.arange(1,len(moments))[:,None]
        
        return -(moments[0] * np.arccos(e) + 
                 2*np.sum(moments[1:] 
                          * np.sin(np.outer(n, 
                                            np.arccos(e)))/n, axis=0)
                 )/np.pi


    def integrate_site_specific(self, energy, index=None):
        """Integrate the ldos up to a specific energy for each index.
        
        Parameters
        ----------
        energy : float or np.ndarray
            If the integrand needs to be evaluated at the same energy for all
            indices, energy can be a float. Otherwise the length of energy
            should correspond to the number of specified indices or the 
            number of indices present in self.indices (if index is None).
              
              
        index : np.ndarray
            The indices for which the integral must be evaluated. 
            
        Returns
        -------
        ildos : np.ndarray
            shape (q,) with q the number of provided indices or the number of 
            total indices in self.
        """
        # Rescale energy.
        energy = np.asarray(energy)
        energy_scaled = (energy - self.b) / self.a
    
        # Obtain necessary moments from the requested indices.
        moments = self._moments(index)
    
        return self.a * (self._F_site_specific(energy_scaled, moments)
                          - self._F_site_specific(-1, moments))


    def _origin_slope(self, ldos, ildos, mu, inside_band, above_band):
        """Obtain the origin & slope from ldos and ildos.
    
        ldos : np.ndarray
            The ldos in kwant units, 1 dimensional array.
    
        ildos : np.ndarray
            The ildos in kwant units, 1 dimensional array.
    
        inside_band : np.ndarray
            Array with boolean values to indicate where the default
        """
        # Conversion to pescado units (2 for spin degeneracy)
        ldos *= 2 * self.conversion_unit
        ildos *= 2
        
        # Calculation origin
        origin = ildos - mu[inside_band] * ldos
    
        # By default the origin and ldos are zero for sites outside of the band        
        origin_res = np.zeros(len(inside_band))
        ldos_res = np.zeros(len(inside_band))
    
        origin_res[above_band] = 2
        ldos_res[above_band] = 0
    
        origin_res[inside_band] = origin
        ldos_res[inside_band] = ldos
    
        return origin_res, ldos_res
        

    def quantum_functional_kwant(self, mu):
        """
        Quantum functional from random ldos, used for the Newton Raphson solver
    
        With random we mean that the ldos can be calculated for any potential at
        any number of sites.
    
        Parameters
        ----------
    
        mu: np.ndarray
            Voltage as given in pescado for which the spectral density should be 
            evaluated, shape (n,)
    
        Returns
        -------
        origin: np.ndarray
            Pescado units, shape (n,)
    
        slope: np.ndarray
            Pescado units, shape (n,)
        """
        # Convert voltage to kwant units
        # Subtracting the potential for which the ldos is calculated is 
        # necessary, since the value of the potential shifts the energy bands
        # and thus the ldos. 
        energy = mu * self.conversion_unit - self.potential
    
        # Only do the evaluation if we are within the energy bands
        bottom_band =  -self.a + self.b
        top_band =  self.a + self.b
        inside_band = np.all([bottom_band <= energy, 
                              energy <= top_band], axis=0) 
        above_band = energy > top_band              
    
        energy_inside = energy[inside_band]
        index = self.indices[inside_band]
            
        ldos = self.call_site_specific(energy_inside, index)
        ildos = self.integrate_site_specific(energy_inside, index)
            
        return self._origin_slope(ldos, ildos, mu, inside_band, above_band)


    def quantum_functional_lat(self, mu):
        """
        Quantum functional from lattice ldos, used for the Newton Raphson solver.
        
        With lattice ildos we mean that the ldos is only calculated at one site 
        at the center of the lattice for a flat potential.
        
        Parameters
        ----------

        mu: np.ndarray
            Voltage (pescado units) for which the spectral density should be 
            evaluated, shape (n,)
            
        Returns
        -------
        origin: np.ndarray
            Pescado units, shape (n,)
        
        slope: np.ndarray
            Pescado units, shape (n,)
        """        
        # Convert voltage to kwant units
        energy = mu * self.conversion_unit
        
        # Only do the evaluation if we are within the energy bands
        bottom_band =  -self.a + self.b
        top_band =  self.a + self.b
        inside_band = np.all([bottom_band <= energy, 
                              energy <= top_band], axis=0) 
        above_band = energy > top_band              
        
        energy_inside = energy[inside_band]
    
        # Evaluate ldos and ildos from moments (kwant units)
        # Indexing because there is only 1 vector 
        ldos = self.__call__(energy_inside)[:,0]
        ildos = self.integrate(energy_inside)[:,0]
            
        return self._origin_slope(ldos, ildos, mu, inside_band, above_band)