#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 09:07:28 2022

@author: thellemans
"""

import pickle
import csv
import numpy as np
import sys
from os.path import dirname
from types import ModuleType, FunctionType
from gc import get_referents

# This delivers the correct path of the 'data_files' directory if the file structure is not changed 

#if os.name == 'posix':
#    my_path = '/home/thellemans/Documents/qpc_exp2sim/simulation/files_data/'
#else:
#    my_path = dirname(dirname(dirname(__file__))) + '/simulation/files_data/'


my_path = dirname(dirname(dirname(__file__)))

def read_file(file_name, file_type = 'simulation' ):
    """
    file_name: str
        Name of the pickled file
        
    file_type: str
        If equal to 'simulation' the function looks in the 
        simulation/files_data folder of the qpc_exp2sim repository if it can 
        find the file over there.
        
        If equal to 'experiment' the function looks in the 
        experiment/files_data folder of the qpc_exp2sim repository if it can 
        find the file over there.
        
        For any other value of the file_type parameter, file_name is considered
        as the path to the correct file.
    """
    if file_type in ['simulation', 'experiment']:
        file_name = my_path + '/{}/files_data/{}'.format(file_type, file_name)
        
    if '/experiment/' in file_name:
        return read_file_exp(file_name)
    else:
        with open(file_name, 'rb') as inp: #'r' for reading, 'b' for 'binary'
            data = pickle.load(inp)
        return data
    

def read_file_exp(file_name):
    """
    Parameters
    ----------
    file_name : str
        (path to) csv file with data to read.

    Returns
    -------
    dictionary : dict
        containing all the data in the csv file.
    """
    dictionary={}
    file = open(file_name)
    csvreader = csv.reader(file)
    next(csvreader)
    
    for row in csvreader:
        qpc=row[0].split('_')
        dice=qpc[0]+'_'+qpc[1]
                
        nb=int(qpc[2])
        measure=qpc[3]
    
        if dice not in dictionary:
            dictionary[dice]={}
        dictionary[dice]['qpc_type'] = row[3]
    
        if nb not in dictionary[dice]:
            dictionary[dice][nb]={}
        if measure not in dictionary[dice][nb]:
            dictionary[dice][nb][measure]={}
        dictionary[dice][nb][measure]['unit']=row[1]
        dictionary[dice][nb]['nb_sweep']=row[2]
        
        data=np.array(row[4:])
        data_2=[]
        for value in data:
            if value!='':
                data_2.append(float(value))
        data_2=np.array(data_2)
    
        dictionary[dice][nb][measure]['value']=data_2
    
    file.close()
    return dictionary


def write_file(data, file_name, file_type = 'simulation' ):
    """
    Write pickled data to a file with name 'file_name' and update overview file
    """
    if file_type in ['simulation', 'experiment']:
        file_name = my_path + '/{}/{}'.format(file_type, file_name)
        
    with open(file_name, 'wb') as outp: #'w' for writing, 'b' for 'binary'
        pickle.dump(data, outp)
            

# Based on https://stackoverflow.com/questions/449560/how-do-i-determine-the-size-of-an-object-in-python

# Custom objects know their class.
# Function objects seem to know way too much, including modules.
# Exclude modules as well.
BLACKLIST = type, ModuleType, FunctionType

def getsize(obj, analysis=False):
    """sum size of object & members."""
    if isinstance(obj, BLACKLIST):
        raise TypeError('getsize() does not take argument of type: '+ str(type(obj)))
    seen_ids = set()
    size = 0
    objects = [obj]
    while objects:
        need_referents = []
        for obj_elem in objects:
            if not isinstance(obj_elem, BLACKLIST) and id(obj_elem) not in seen_ids:
                seen_ids.add(id(obj_elem))
                size += sys.getsizeof(obj_elem)
                need_referents.append(obj_elem)
        objects = get_referents(*need_referents)
   
    if analysis:
        print('Total # bytes: ', size)
        for elem in obj.__dict__.keys():
            print('- ', elem, getsize(obj.__dict__[elem]))
           
    return size


def compare_dict(dict0, dict1, exclude=set(), verbose=True):
    """
    Return True if both dictionaries are the same with the exlusion of the 
    key value pairs for which the keys are expressed in the exclude set.
    """
    
    for key in set(dict0.keys()) - exclude:
        val0 = dict0[key]
        val1 = dict1[key]
        if isinstance(val0, np.ndarray):
            val0 = val0.tolist()
            val1 = val1.tolist()
        if val0 != val1:
            if verbose:
                print('In compare_dictionaries: \
                      \n {key} not the same for both dictionaries: \
                      \n dict0.[{key}] = {val0} \
                      \n dict1.[{key}] = {val1}'.format(key = key,
                                                        val0 = dict0[key],
                                                        val1 = dict1[key]))
            return False
    return True


class Data:
    def __init__(self, p_geom=None, p_sim=None, file_geom=None, file_sim=None, 
                 coord=None, coord_kwant=None, coord_kpm=None,
                 voltage=None, charge=None, ldos=None, 
                 conductance=None, voltage_shift=None, dopant_dens=None,
                 V3=None, file=None):
        
        """Single data object containing all the information on a simulation. 
        
        Note: Could be extended with plotting functions for the data etc.
        """
        self.p_geom = p_geom
        self.p_sim = p_sim
        self.file_geom = file_geom
        self.file_sim = file_sim
        self.coord = coord
        self.coord_kwant = coord_kwant
        self.coord_kpm = coord_kpm
        self.voltage = voltage
        self.charge = charge
        self.ldos = ldos
        self.conductance = conductance
        self.voltage_shift = voltage_shift
        self.dopant_dens = dopant_dens
        self.V3 = V3
        
        if file is not None:
            with open(file, 'wb') as outp:
                pickle.dump(self, outp)
    
    def __str__(self):
        geometry_part = 'The geometry parameters are:\n\t' + \
                        '\n\t'.join([str(key) + ': ' + str(value) 
                                     for key, value in self.p_geom.items()])
        
        simulation_part = 'The simulation parameters are:\n\t' + \
                          '\n\t'.join([str(key) + ': ' + str(value) 
                                     for key, value in self.p_sim.items()])
        
        if self.file_geom is None:
            input_part_g = 'There is no stored geometry input file'
        else:
            input_part_g = 'The geometry input file is: ' + self.file_geom
        if self.file_sim is None:
            input_part_s = 'There is no stored simulation input file'
        else:
            input_part_s = 'The simulation input file is: ' + self.file_sim
        input_part = '\n'.join([input_part_g, input_part_s])
        
        return '\n\n'.join([geometry_part, simulation_part, input_part])
    
#    def plot_voltage(self, **kwargs):
#        setting_data = 0
#        return plot_pescado(self, setting_data=setting_data, **kwargs)
#        
#    
#    def plot_density(self, charge_to_density=True, **kwargs):
#        setting_data = 2 if charge_to_density else 1
#        return plot_pescado(self, setting_data=setting_data, **kwargs)
#    
#    
#    def plot_conductance(self, **kwargs):
#        return conductance(self, **kwargs)