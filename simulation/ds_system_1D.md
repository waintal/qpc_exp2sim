---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# 1D system

```python
from types import SimpleNamespace
import time
import numpy as np
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.simulation import builder, solver
```

## Input parameters & problem builder

```python
p_geom = SimpleNamespace(grid_fine = [10],                  # Mesh in xyz direction in device (Pescado mesh + Kwant lattice)
                         grid_coarse = [20],              # Mesh in xyz direction around device (Pescado mesh)
                         section = [0, 0, None],                  # None if you don't take a cross section for this ax
                         L = 1000,                                  # Lenth total device in x direction = transport direction
                         W = 1000,                                  # Width total device in y direction = gate direction
                         L_reg = 1000, 
                         W_reg = 1000,
                         L_narrow_gate = 50,                        # Length of the narrow gate 
                         qpc_center = [0,400],                      # Widthh of the narrow gate
                         qpc_name = 'full_gate',
                         d0 = 30,                                   # Thickness GaAs layer bottom
                         d_2DEG = 10,                               # Thickness 2DEG 
                         d1 = 30,                                   # Thickness AlGaAs above 2DEG
                         d2 = 60,                                   # Thickness AlGaAs, Si doped
                         d3 = 10,                                   # Thickness AlGaAs 
                         d4 = 10,                                   # Thickness GaAs
                         d_contact = 10,                            # Thickness contact layer
                         scaling = 2,                               # Size of the region around the device to be taken into account in Pescado -> not implemented yet
                         eps_GaAs = 13,                             # Dielectric constant GaAs
                         eps_AlGaAs = 13,                           # Dielectric constant AlGaAs
                         eps_2DEG = 13,
                         eps_air = 1,                               # Dielectric constant air/vacuum (around gates)
                         eps_gates = 1e4,                           # Dielectric constant gates (metal)
                         effective_mass = 0.067,                    # Effective mass for 2DEG DOS calculation
                         setting_pescado_solver='sc', 
                         calibration_device=False)                  # Determines if the 2DEG is set to flexible or helmholtz

# Create the self consistent poisson problem
pp_problem, kwant_sys = builder.problem_builder(p_geom, kwant_sim=False)

# Get the coordinates from the pp_problem for later data analysis
coord = pp_problem.mesh_inst.coordinates()
```

## Poisson or self-consistent simulation

```python
# Setting the simulation parameters
p_sim = SimpleNamespace(# Boundary conditions
                        V_gates_list = np.linspace(-1,1,99), 
                        DEG_dens = 2.8e15,             # Electron density in 2DEG [/m²]
                        charge_sv = None,              # Charge sv should be a sparse vector, it overrules deg_dens

                        # Simulation parameters
                        calibration = False, 
                        calibration_method = 'd',
                        calc_V3 = True, 
                        calc_cond = False,
                        save_poisson = True)

# Start timer
start = time.time()

# Do the simulation
data = solver.problem_solver(p_geom = p_geom,
                             p_sim = p_sim,
                             rpp_problem = pp_problem,
                             sys = None)  

# End timer
end = time.time()
data.p_sim.processing_time = end-start

# Write the necessary output to a file that can be interpreted later on
dh.write_file(data, 'system_{}D/'.format(len(p_geom.grid_fine)) \
              + p_geom.setting_pescado_solver + '_cal_false')
```
