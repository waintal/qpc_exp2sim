---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# 3D system

```python
from types import SimpleNamespace
import time
import numpy as np
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.simulation import solver, builder
from qpc_exp2sim.simulation.pescado_extension import ReducedPoissonProblem
import kwant
import tkwant
```

## 1) Input parameters

```python
from_file = False
file_name = 'b3_grid200_singleV'
input_file = '../data_simulation/input_files/' + file_name
output_file = 'system_3D/' + file_name

p_geom = SimpleNamespace(grid_fine = [100, 100, 10],                  # Mesh in xyz direction in device (Pescado mesh + Kwant lattice)
                             grid_coarse = [40, 40, 10],                # Mesh in xyz direction around device (Pescado mesh)
                             section = [None, None, None],              # None if you don't take a cross section for this ax
                             L = 1000,                                  # Lenth total device in x direction = transport direction
                             W = 1000,                                  # Width total device in y direction = gate direction
                             L_reg = 1000, 
                             W_reg = 1000,
                             L_narrow_gate = 50,                        # Length of the narrow gate 
                             qpc_center = [0,400],                      # Widthh of the narrow gate
                             qpc_name = 'b3',
                             d0 = 20,                                   # Thickness GaAs layer bottom
                             d_2DEG = 10,                               # Thickness 2DEG 
                             d1 = 30,                                   # Thickness AlGaAs above 2DEG
                             d2 = 60,                                   # Thickness AlGaAs, Si doped
                             d3 = 10,                                   # Thickness AlGaAs 
                             d4 = 10,                                   # Thickness GaAs
                             d_contact = 10,                            # Thickness contact layer
                             scaling = 2,                               # Size of the region around the device to be taken into account in Pescado -> not implemented yet
                             eps_GaAs = 13,                             # Dielectric constant GaAs
                             eps_AlGaAs = 13,                           # Dielectric constant AlGaAs
                             eps_2DEG = 13,                             # Dielectric constant 2DEG
                             eps_air = 1,                               # Dielectric constant air/vacuum (around gates)
                             eps_gates = 1e4,                           # Dielectric constant gates (metal)
                             effective_mass = 0.067,                    # Effective mass for 2DEG DOS calculation
                             setting_pescado_solver='sc',               # Determines if the 2DEG is set to flexible or helmholtz
                             calibration_device = False)

p_sim = SimpleNamespace(# Boundary conditions
                        V_gates_list = np.linspace(-2.7,0,20), 
                        energy_list = [0],                    
                        DEG_dens = 2.8e15,                # Electron density in 2DEG [/m²]
                        charge_sv = None, 
    
                        # Simulation parameters
                        calibration = True, 
                        calibration_method = 'v2',
                        calc_V3 = True, 
                        calc_cond = False,
                        save_poisson = True)

if from_file:
    input_data = dh.read_file(input_file)
    p_geom = input_data.p_geom
    p_sim = input_data.p_sim
    
display(p_geom.__dict__)
display(p_sim.__dict__)
```

## 2) Problem builder

```python
start = time.time()
# Create the self consistent poisson problem
pp_builder, kwant_sys = builder.problem_builder(p_geom, pescado_sim=True)
pp_problem = pp_builder.finalized(parallelize=True)
end = time.time()

print(end-start)

# Get the coordinates from the pp_problem for later data analysis
coord = pp_problem.mesh_inst.coordinates()

#rpp_problem = ReducedPoissonProblem(pp_problem)
```

```python
dh.getsize(pp_problem, analysis = True)
```

```python
dh.getsize(pp_problem.mesh_inst.coordinates())
```

```python
dh.write_file(r, 'trash')
```

```python
from scipy.sparse import csr_matrix

csr_matrix(a.shape, dtype=a.dtype)
```

## 3) Poisson or self-consistent + kwant simulation
Poisson or self consistent depends on the setting of 'setting_pescado_solver' in params above. This parameter also defines how the sites should be defined in the problem builder.

```python
start = time.time()

# Do the simulation
data = solver.problem_solver(p_geom = p_geom,
                             p_sim = p_sim,
                             pp_problem = pp_problem,
                             sys = kwant_sys)  

end = time.time()
print(end-start)

# Write the necessary output to a file that can be interpreted later on
dh.write_file(data, output_file)#'system_3D/' + p_geom.qpc_name + '_grid{}'.format(p_geom.grid_fine[0]))
```

**TO DO simulation**
* Give charge/cell as input for plot colormap and with setting = 2 do the conversion to charge/m² -> different for 2D or 3D!
* Return indices if the input for a cross section function is given as a sparse_vector, otherwise plotting later on may get dangerous by assuming the first indices correspond to the correct coordinates
* Make Device and Parts class
* Automate check overlap between shapes, especially when defining boundary conditions
* Do correct meshing of the layer thicknesses / Defining mesh based on location of interfaces 
* Update/filter overview file correctly
* Include a wider region around the device (also air around gate, etc.
* Set dielectric constant in the 2DEG
* Learn how to read a row in a dict csv file for which entry 'name' corresponds to given name. Than write the conductance to a csv file.
* Check influence of:
    * Mesh size
    * Size region around qpc
    * Dopant density -> this changes the fermi wavelength -> decrease mesh parameter for rising n?
* Calculate density in dopant layer such that the pinch of voltage for the experimental results and the simulations is the same
* Why calibration voltage the same for different geometries?
* data_cross_section include an assert statement to check that the None values in the possibly different section_elem are in the same place, otherwise the returned coordinates are not correct
* Indicate the lines of the qpc regions on the 1D cross section plots
* Create calibration solver that's efficient for the cluster, e.g. only use node 0 and 1 and bcast to other ranks. 
* Something wrong with density values in 1D plots
* Improve choice of maximum gated point for calibration -> include in qpc_data?
* Calculation V3 from density: Use a poisson simulation for linear extrapolation of the density curve and calculating the cross section with n = 0

Plan 04/04
Before lunch
* with save_poisson -> make sure that conductance is still calculated but charge/voltage removed after each interval -> ok?
* Start conductance calculation for a1 a2 a3 a4 with 144 and reduced interval -> ok
* Start V3 comparison

After lunch
* Create V3 from density, read out density sparse vector with index and add if density is still 0
* Increase region
* Decrease mesh size z direction

**TO DO experimental**

```python
file_name = 'b3_grid200_singleV'
output_file = 'system_3D/' + file_name

data = dh.read_file(output_file)
    
# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
coord = data.coord
voltage = data.voltage
charge = data.charge

display(p_geom.__dict__)
display(p_sim.__dict__)

kwant_sys = builder.make_kwant_system(p_geom)
kwant_indices = builder.get_kwant_indices(kwant_sys, coord)

cell_area = p_geom.grid_fine[0] * p_geom.grid_fine[1] * 1e-18

density_operator = kwant.operator.Density(kwant_sys, p_sim.DEG_dens * cell_area)
occup = tkwant.manybody.lead_occupation(chemical_potential=0, temperature=0)


tkwant_density = []
tkwant_error = []
for voltage_elem in voltage:
    start = time.time()

    params = {'voltage': voltage_elem, 'kwant_indices': kwant_indices}
    state = tkwant.manybody.State(kwant_sys, tmax=1, occupations=occup, comm=None, params=params)
    state.refine_intervals(rtol=1e-10, atol=1e-10)
    tkwant_density.append(state.evaluate(density_operator))
    tkwant_error.append(state.estimate_error())

    end = time.time()
    print('{} ready, this took {} s'.format(voltage_elem, end-start))
display(tkwant_error)
data.tkwant_density = tkwant_density
data.tkwant_error = tkwant_error

dh.write_file(data, output_file)
```

```python
data.charge[0].values
```

```python
tkwant_density
```

```python
51**2
```

```python
file_name = 'b3_grid200_singleV'
output_file = 'system_3D/' + file_name

data = dh.read_file(output_file)
```

```python
dh.getsize(data, analysis=True)
```

```python
r = data.p_geom
```

```python
r
```

```python
del r.L
```

```python
r
```

```python
file_name = 'b3_grid20'
output_file = 'system_3D/grid/' + file_name

data = dh.read_file(output_file)
```

```python
data.voltage[0].values
```

```python
r = data.voltage[0] * 1000
```

```python
r.values
```

```python
£
```
