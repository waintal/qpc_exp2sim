---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Data analysis 3D simulation

**Table of contents**

0. Loading data
1. Input parameters
2. Geometry of the device
3. Voltage and charge
4. Pinch of voltage
5. Conductance

```python
from __future__ import print_function
from types import SimpleNamespace
import scipy.constants as c
import matplotlib.pyplot as plt
import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim_old as plotting
from qpc_exp2sim.simulation import builder, solver
from qpc_exp2sim.pescado_kwant import plotting_pes

import numpy as np
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
%matplotlib notebook
```

## 0) Loading data

```python
#data = dh.read_file('a4_grid10_V64_a4_fsc_nr_m100_it5')
data = dh.read_file('b3_grid2_test2_V128_test_fsc_nr_m100_it5')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3

cell_area = p_geom['grid'][0] * p_geom['grid'][1] * 1e-18

# Get conversion unit from voltage to potential
conversion_unit = c.elementary_charge/p_geom['t']
```

## 1) Input paramaters

```python
print('The fermi wavelength: lambda_f = {} nm'.format(np.sqrt(2*np.pi/p_sim['DEG_dens']) * 1e9))
print(data)
```

## 2) Geometry of the device

```python
# Defining the several shape objects relevant for the problem
gates = builder.test_gates_shape(**p_geom)
DEG = builder.DEG_shape(**p_geom)
GaAs = builder.layer0_shape(**p_geom) | builder.layer4_shape(**p_geom)
AlGaAs_undoped = builder.layer1_shape(**p_geom) | builder.layer3_shape(**p_geom)
AlGaAs_doped = builder.layer2_shape(**p_geom)
    
```

```python
plotting_pes.shape(shapes_list=[GaAs, AlGaAs_doped, AlGaAs_undoped, DEG, gates], 
                    colors=['g', 'y', 'r', 'b','k'],  
                    coord = coord, 
                    section = [None, None, None], 
                    legend_label = ['GaAs', 'AlGaAs_doped', 'AlGaAs_undoped', 'DEG', 'gates'])
```

```python
shapes_list=[GaAs, AlGaAs_doped, AlGaAs_undoped, DEG, gates, air]

data = np.sum([shape(coord)*(index+1) for index, shape in 
                   enumerate(shapes_list)], axis=0)
```

```python
%matplotlib notebook
plotting_pes.shape(shapes_list=[GaAs, AlGaAs_doped, AlGaAs_undoped, DEG, gates], 
                    colors=['g', 'y', 'r', 'b', 'tab:blue'],  
                    coord=coord, 
                    section = [None, None, 120],
                    legend_label = ['GaAs', 'AlGaAs_doped', 'AlGaAs_undoped', 'DEG', 'gates'], 
                    aspect_equal=True, imshow=True)
```

## 3) Voltage and charge results


### 3.1) Colormap

```python
#charge_val = np.array([sparse_vec[np.arange(len(coord))] for sparse_vec in charge])

#max_colorbar_n = np.max(np.abs(charge_val))
#max_colorbar_v = np.max(np.abs(p.V_gates_list))
```

```python
# for compatibility with the plotting functions below: convert p_geom and p_sim for the moment to SimpleNamespaces
from types import SimpleNamespace
p_geom = SimpleNamespace(**p_geom)
p_sim = SimpleNamespace(**p_sim)
```

```python
widget_v = widgets.Dropdown(options=V_gates_list, description ='V_gates:')
widget_normal = widgets.Dropdown(options=[('x (along transport)',0),('y (along gates)',1),('z (thickness)',2)], description='normal:', value=2)
widget_vec = widgets.Dropdown(options=[('x (along transport)',0),('y (along gates)',1),('z (thickness)',2)], description='vec:')
widget_point = widgets.Dropdown(options=np.unique(coord[:,0]), description='point:', value=0)
widget_figsize = widgets.IntSlider(value=4, min=0, max=15, step=1, continuous_update=False, description='Figure size:')

def update_points(*args):
    widget_point.options = np.unique(coord[:,widget_normal.value])

widget_point.observe(update_points, 'value')
```

```python
# The following three lines show the easy relation between the density in the 2DEG and the voltage in the 2DEG when V_gates = 0V
hh = p_geom.effective_mass * c.electron_mass * c.elementary_charge/(c.pi * c.hbar **2) # Helmholtz coefficient in 1/(Vm^2)
V_2DEG_at_zero_gate_voltage = p_sim.DEG_dens/hh
print('Voltage in 2DEG at V_g = 0: ',V_2DEG_at_zero_gate_voltage, 'V (this follows from the linear helmholtz equation)')

def f(V_gates, normal, point, figsize):
    index = np.where(V_gates_list == V_gates)[0][0]
    voltage_sparse_vec = voltage[index]        
    charge_sparse_vec = charge[index]
    
    if isinstance(voltage_sparse_vec, (list, np.ndarray)):
        voltage_sparse_vec = voltage_sparse_vec[-1]
        charge_sparse_vec = charge_sparse_vec[-1]
    
    section = [None, None, None]
    section[normal] = point
    
    # Making a cross section of the data
    coord_2D, voltage_2D = plotting.data_cross_section(coord, voltage_sparse_vec, section=section)
    coord_2D, charge_2D = plotting.data_cross_section(coord, charge_sparse_vec, section=section)
    
    print('Standard deviation on charge in 2DEG: {:e} /m^2'.format(charge_2D.std()/(p_geom.grid[0] * p_geom.grid[1] * 1e-18)))
    print('voltage average: ', np.average(voltage_2D))
    
    # Getting the correct labels
    ax_labels = {0: 'x(nm)', 1:'y(nm)', 2:'z(nm)'}
    labels = [ax_labels[ax] for ax in {0,1,2} - {normal}]
    
    # Plotting the data    
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=[10,5])
    plotting.plot_colormap(coord_2D, voltage_2D, setting=0, aspect_equal=True, figsize=figsize, labels=labels, 
                          pcolormesh = True, ax=ax1)    
    plotting.plot_colormap(coord_2D, charge_2D, cell_area=cell_area, setting=2, aspect_equal=True, 
                           figsize=figsize, labels=labels, cbar_range= [None, None], 
                          pcolormesh = True, ax=ax2) #cbar_range= [0,2.8e15])
    ax1.set_title('Voltage Pescado')
    ax2.set_title('Density Pescado')
    plt.tight_layout()
    plt.show()

interact(f, V_gates=widget_v, 
             normal=widget_normal, 
             point=widget_point, 
             figsize=widget_figsize)

# To do: put shape of QPC in dotted line on top


```

```python
# TO DO Fix for certain value of V_gates the min and max values of the colorbar
```

### 3.2) Cross sections along line
#### 3.2.1) For fixed gate voltage and varying location

```python
widget_coordx = widgets.Dropdown(options=np.unique(coord[:,0]), description='coord x:', value=0)
widget_coordy = widgets.Dropdown(options=np.unique(coord[:,1]), description='coord y:', value=0)
widget_coordz = widgets.Dropdown(options=np.unique(coord[:,2]), description='coord z:', value=0)
widget_figsize2 = widgets.IntSlider(value=5, min=0, max=15, step=1, continuous_update=False, description='Figure size:')
```

```python
# pointsx = np.array([[None,0,0], [None,100,0], [None,200,0], [None,300,0]])
# pointsy = np.array([[0,None,0], [100,None,0], [200,None,0], [300,None,0]])
# pointsz = np.array([[500, 0, None], [0, 400, None]])

pointsx = np.array([[None,0,0], [None,10,0], [None,20,0], [None,40,0]])
pointsy = np.array([[0,None,0], [10,None,0], [20,None,0], [40,None,0]])

points_list = [pointsx, pointsy, pointsz]

def j(V_gates, vec):
    # Getting the sparse vectors containing the data for a certain gate voltage
    index = np.where(V_gates_list == V_gates)[0][0]
    voltage_sparse_vec = voltage[index]
    charge_sparse_vec = charge[index]
    
    points = points_list[vec]
    legend = [[['x=', 'y=', 'z='][i] + str(point[i]) for i in {0,1,2} - {vec}] for point in points]
    legend = [", ".join(elem) for elem in legend]
    
    # Making a cross section of the data
    coord_1D, voltage_1D = plotting.data_cross_section(coord, voltage_sparse_vec, section=points)
    coord_1D, charge_1D = plotting.data_cross_section(coord, charge_sparse_vec, section=points)

    # Sort the data such that we don't get crossing lines through the figure
    sort_indices = np.argsort(coord_1D[:,0])
    coord_1D = coord_1D[sort_indices]
    voltage_1D = voltage_1D[sort_indices]
    charge_1D = charge_1D[sort_indices]
    
    # Plotting the data    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,4))
    xlabel = ['x(nm)', 'y(nm)', 'z(nm)', r'$V_g [V]$'][vec]
    plotting_pes.line(coord_1D, voltage_1D*(-conversion_unit), xlabel=xlabel, ylabel= r'$U [t]$',
                      ax=ax1)
    plotting_pes.line(coord_1D, charge_1D/cell_area, xlabel=xlabel, ylabel= r'$n [/m^2]$', 
                      legend_label=legend, ax=ax2)
    plt.tight_layout()
    plt.show()
    
interact(j, V_gates=widget_v, 
            vec=widget_vec)
```

#### 3.2.1) For varying gate voltage and fixed location

```python
widget_v_min = widgets.Dropdown(value = min(V_gates_list), options=V_gates_list, description ='V_gates min:')
widget_v_max = widgets.Dropdown(value = max(V_gates_list), options=V_gates_list, description ='V_gates max:')
```

```python

def f(v_min, v_max, vec):
    index_v = np.where(np.array([v_min <= np.array(V_gates_list), np.array(V_gates_list) <= v_max]).all(axis=0))[0]
    
    point = np.array([[0,0,0]], dtype=object)
    point[0,vec] = None
    
    # Making a cross section of the data
    coord_1D, voltage_1D = plotting.data_cross_section(coord, voltage[index_v], section=point)
    coord_1D, charge_1D = plotting.data_cross_section(coord, charge[index_v], section=point)

    # Sort the data such that we don't get crossing lines through the figure
    sort_indices = np.argsort(coord_1D[:,0])
    coord_1D = coord_1D[sort_indices]
    voltage_1D = voltage_1D[sort_indices]
    charge_1D = charge_1D[sort_indices]

    # Plotting the data    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,4))
    xlabel = ['x(nm)', 'y(nm)', 'z(nm)', r'$V_g [V]$'][vec]
    plotting_pes.line(coord_1D, voltage_1D*(-conversion_unit), xlabel=xlabel, ylabel= r'$U [t]$',
                      ax=ax1)
    plotting_pes.line(coord_1D, charge_1D, xlabel=xlabel, ylabel= r'$n [/m^2]$', 
                      ax=ax2, legend_label = [r'$V_g$ = {}V'.format(round(elem, 3)) for elem in V_gates_list[index_v]])
    
    plt.tight_layout()
    plt.show()
        
    

interact(f, v_min = widget_v_min, 
            v_max = widget_v_max, 
            vec = widget_vec)
```

### 3.3) Value at point in function of V_gates

```python
points = np.array([[0,0,0],[0,250,0], [250,0,0], [500,0,0]])
import matplotlib.pyplot as plt
def j(figsize):
    legend = [str(point) for point in points]
    
    # Making a cross section of the data
    coord_0D, voltage_0D = plotting.data_cross_section(coord, voltage, section=points)
    coord_0D, charge_0D = plotting.data_cross_section(coord, charge, section=points)
    
    V = np.array(V_gates_list)
    
    voltage_shift = 0.263336079027824

    # Plotting the data
    fig, ax = plt.subplots()
    plotting.plot_line(V, voltage_0D, p_geom, setting_x=3, setting_y=0, figsize=figsize, legend_label=legend, ax = ax, show=False)
    ax.plot([voltage_shift, voltage_shift], [0, V_2DEG_at_zero_gate_voltage], color = 'k', linestyle = '--', linewidth = 0.5)
    ax.set_ylim([-0.002, 0.04])
    plt.show()
    
    fig, ax = plt.subplots()
    plotting.plot_line(V, charge_0D, p_geom, setting_x=3, setting_y=2, figsize=figsize, legend_label=legend, ax = ax, show=False)
    setting_lines = {'color':'k', 'linestyle':'--', 'linewidth':0.5}
    
    plt.plot([voltage_shift, voltage_shift], [0, p_sim.DEG_dens], **setting_lines)
    plt.axhline(2.8e15, **setting_lines)
    plt.show
    
    # When displaying the line below, it's clear that the helmholtz coefficient is the ratio between the density and voltage
    # in the non depleted regions.
    # display(data.p_sim.helmholtz_coef, charge_0D/voltage_0D)

interact(j, figsize = widget_figsize2)

```

```python
points = np.array([[0,0,0],[0,250,0], [0,500,0], [250,0,0], [500,0,0]])

legend = [str(point) for point in points]
figsize = 8

# Making a cross section of the data
coord_0D, charge_0D = plotting.data_cross_section(coord, charge, section=points)

f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [2, 1]})
plotting.plot_line(V_gates_list, charge_0D, p_geom, setting_x=3, setting_y=2, ax=ax1, figsize=figsize, legend_label=legend, vline=False, show=False)
# Get the colors of the plotted lines such that we can reuse them for the indication points
colors = [line.get_color() for line in ax1.lines]

setting_lines = {'color':'k', 'linestyle':'--', 'linewidth':0.5}

if isinstance(p_sim.calibration, bool) and p_sim.calibration:
    ax1.plot([0, 0], [0, p_sim.DEG_dens], **setting_lines)
else:
    ax1.plot([voltage_shift, voltage_shift], [0, p_sim.DEG_dens], **setting_lines)
ax1.axhline(2.8e15, **setting_lines)

plotting.plot_overview_points(p_geom, points[:,0:2], ax2, color=colors)

asp = np.diff(ax1.get_xlim())[0] / np.diff(ax1.get_ylim())[0]
ax1.set_aspect(asp/2)
plt.show()

```

## 4) Pinch of voltage $V_3$
Pinch of voltage $V_3$ is defined by depletion of the center of the 2DEG, point (0,0,0). 

```python
print('The pinch of voltage resulting from: \n\
        * the density calculations is: {} V with an accuracy of {} V \n\
        * the conductance calculations is {} V'.format(np.round(V3[0][0],3) if V3[0][0] is not None else None, 
                                                       np.round(V3[0][1],3) if V3[0][1] is not None else None, 
                                                       np.round(V3[1],3) if V3[1] is not None else None ))
```

## 5) Conductance


With these lines of code you can calculate the conductance from the pescado results above and write it to a file:

```python
#params.energy_list = [0]
#sys = builder_3D.make_kwant_system(params)
#conductance = solver_3D.conductance_solver(sys, coord, voltage, params.V_gates_list)
#dh.write_file([params, coord, voltage, charge, V3, conductance], 'system_3D/' + params.qpc_name)
```

The cell below allows to plot the onsite potential in the scattering region that kwant is using:

```python
# widget_v = widgets.Dropdown(options=p.V_gates_list, description ='V_gates:')

# sys = builder_3D.make_kwant_system(params)
# index_2DEG = np.array(np.where(coord[:,2] == 0))[0]
# coord_2DEG = coord[index_2DEG][:,0:2]

# def f(V_gates):
#     index = np.where(p.V_gates_list == V_gates)[0][0]
#     voltage_V = voltage[index]
#     voltage_2DEG = voltage_V[index_2DEG]

#     # onsite_values = [sys.hamiltonian(sys.id_by_site[site], sys.id_by_site[site], params = dict(voltage=voltage_2DEG, coord=coord_2DEG)) for site in sys.sites]
#     onsite_values = np.array([float(sys.onsites[i][0](sys.sites[i], voltage=voltage_2DEG, coord=coord_2DEG)) for i in range(len(sys.onsites))])
    
#     # scaling of the onsite values:
#     onsite_values *= p.t / c.elementary_charge
#     kwant.plotter.map(sys, onsite_values)

# interact(f, V_gates=widget_v)
```

```python
plotting.plot_conductance(V_gates_list, conductance.transpose(), linestyle='.--', hlines=np.arange(6))
```

```python
plotting.linear_fit_conductance(V_gates_list, conductance[:,0])
```

```python
# Use -1.2, -0.8 for the test2 
```

```python

```

```python
import math
from matplotlib.patches import Arc

%matplotlib notebook

def arrowed_spines(fig, ax):

    xmin, xmax = ax.get_xlim() 
    ymin, ymax = ax.get_ylim()

    # removing the default axis on all sides:
    for side in ['bottom','right','top','left']:
        ax.spines[side].set_visible(False)

    # removing the axis ticks
    plt.xticks([]) # labels 
    plt.yticks([], rotation=90)
    ax.xaxis.set_ticks_position('none') # tick markers
    ax.yaxis.set_ticks_position('none')
    
    ax.spines['left'].set_position('zero')

    # get width and height of axes object to compute 
    # matching arrowhead length and width
    dps = fig.dpi_scale_trans.inverted()
    bbox = ax.get_window_extent().transformed(dps)
    width, height = bbox.width, bbox.height

    # manual arrowhead width and length
    hw = 1./20.*(ymax-ymin)  * 0.5
    hl = 1./20.*(xmax-xmin) * 0.5
    lw = 0.7 # axis line width
    ohg = 0.3 # arrow overhang

    # compute matching arrowhead length and width
    yhw = hw/(ymax-ymin)*(xmax-xmin)* height/width 
    yhl = hl/(xmax-xmin)*(ymax-ymin)* width/height 
    # draw x and y axis
    ax.arrow(xmin, 0, xmax-xmin, 0., fc='k', ec='k', lw = lw, 
             head_width=hw, head_length=hl, overhang = ohg, 
             length_includes_head= True, clip_on = False) 

    ax.arrow(0, ymin, 0., ymax-ymin, fc='k', ec='k', lw = lw, 
             head_width=yhw, head_length=yhl, overhang = ohg, 
             length_includes_head= True, clip_on = False)

def get_angle_plot(line, ax, offset = 0.5, color = None, origin = [0,0], label=None):

    l1xy = np.array(line.get_xydata()).transpose()    
    
    # Angle between line1 and x-axis
    slope = (l1xy[1][1] - l1xy[0][2]) / float(l1xy[1][0] - l1xy[0][0])
    angle = abs(math.degrees(math.atan(slope))) # Taking only the positive angle
        
    xmin, xmax = ax.get_xlim() 
    ymin, ymax = ax.get_ylim()

    if color is None:
        color = line1.get_color() # Uses the color of line 1 if color parameter is not passed.

    if label is None:
        label = str(angle)+u"\u00b0"
    
    width = (xmax-xmin)*offset
    height = (ymax-ymin)*offset
    
    angle_plot = Arc(origin, width = width, 
                       height = height, 
                       angle = 0, 
                       theta1 = 0, 
                       theta2 = math.degrees(math.atan(p_sim.helmholtz_coef)), 
                       color=color, 
                       label = label)
    
    angle_text = [width/2, height/2, label]
    
    return angle_plot, angle_text


fig = plt.figure()
ax = plt.gca()

ax.plot([-1, 0, 1], [0, 0, p_sim.helmholtz_coef], linewidth = 3)
plt.ylim([0,p_sim.helmholtz_coef*1.2])

# Getting the curve with indication of rho
line = plt.Line2D([-1, 0, 1], [-p_sim.helmholtz_coef, 0, p_sim.helmholtz_coef], linewidth = 3)
angle_plot, angle_text = get_angle_plot(line, ax, 0.3, color = 'k', label=r'$\rho$')
ax.add_patch(angle_plot) # To display the angle arc
ax.text(x = angle_text[0], y = angle_text[1], s = angle_text[2], size='large') # To display the angle value

plt.xlabel(r'$\mu$', loc='right', size = 15)
plt.ylabel('$n$', loc='top', rotation = 0, size=15)

arrowed_spines(fig, ax)
plt.show()
```

```python

```

```python

```
