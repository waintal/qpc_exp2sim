#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 13 18:08:23 2022

@author: helle
"""
import numpy as np
from copy import deepcopy
import kwant
import tinyarray as ta

from pescado.tools import SparseVector

from .reduced_poisson import ReducedPoissonProblem
from .timing import timing
from .extension_pes import indices_from_coordinates
from .exact_ldos import ExactLdos

import pickle


def extend_sys_coord(builder, coord):
    """
    Leads are removed from the system and sites are added at the edges of the system
    The onsite potential of the added sites is equal to the onsite potential of the
    edge site next to it. The hopping from the edge site to the added site is equal 
    to the hopping towards the edge site in the same direction. 
    
    flex coord, the coordinates of the flexible points. Same dimension as the kwant/kpm system
    """
    
    builder.leads = []
    
    reference_site = list(builder.H.keys())[0]
    lat = reference_site.family
    vectors = reference_site.family.transf
    neighbor_vectors = {ta.array(elem) for elem in np.vstack([vectors, -vectors])}
    neighbor_nb = len(vectors) * 2

    hoppings = dict(builder.hopping_value_pairs())
    
    edge_sites = [site for site, val in builder.H.items() if len(val)/2 - 1 < neighbor_nb]
    
    while edge_sites != []:
        #print(type(edge_sites), len(edge_sites))
        edge_site = edge_sites[0]
        edge_sites = edge_sites[1:]
        #print(edge_site)
        # Check which neighbours are alreay present for this edge site
        local_neighbors_vectors = {neighbor.tag - edge_site.tag
                                   for neighbor in builder.H[edge_site][2::2]}
        
        # Subtract these neighbours from the sites we need to add
        vectors_to_add = neighbor_vectors - local_neighbors_vectors
        
        for vector in vectors_to_add:
            # Adding the extra site with onsite potential next to the edge site
            new_site = lat(*(edge_site.tag + vector))
            
            if new_site.pos in coord:
                builder[new_site] = builder.H[edge_site][1] #[1] for the onsite potential
                edge_sites.append(new_site)

                # Adding hoppings between the new site and his neighbors
                hopping_vectors = neighbor_vectors - {vector}

                for hopping_vector in hopping_vectors:
                    hopping_site = lat(*(new_site.tag + hopping_vector))

                    if hopping_site in builder.H.keys():
                        hopping_site_ref = lat(*(edge_site.tag + hopping_vector))
                        if (edge_site, hopping_site_ref) in hoppings.keys():
                            builder[new_site, hopping_site] = hoppings[(edge_site, hopping_site_ref)]
                            hoppings[new_site, hopping_site] = hoppings[(edge_site, hopping_site_ref)]
                        elif (hopping_site_ref, edge_site) in hoppings.keys():
                            builder[hopping_site, new_site] = hoppings[(hopping_site_ref, edge_site)]
                            hoppings[hopping_site, new_site] = hoppings[(hopping_site_ref, edge_site)]    
    return builder



def extend_sys_nb(builder, nb=1):
    """
    Leads are removed from the system and sites are added at the edges of the system
    The onsite potential of the added sites is equal to the onsite potential of the
    edge site next to it. The hopping from the edge site to the added site is equal 
    to the hopping towards the edge site in the same direction. 
    """
    
    builder.leads = []
    
    reference_site = list(builder.H.keys())[0]
    lat = reference_site.family
    vectors = reference_site.family.transf
    neighbor_vectors = {ta.array(elem) for elem in np.vstack([vectors, -vectors])}
    neighbor_nb = len(vectors) * 2

    hoppings = dict(builder.hopping_value_pairs())
    
    for i in range(nb):
        edge_sites = [site for site, val in builder.H.items() if len(val)/2 - 1 < neighbor_nb]

        for edge_site in edge_sites:
            local_neighbors_vectors = {neighbor.tag - edge_site.tag
                                       for neighbor in builder.H[edge_site][2::2]}

            vectors_to_add = neighbor_vectors - local_neighbors_vectors
            for vector in vectors_to_add:
                # Adding the extra site with onsite potential next to the edge site
                new_site = lat(*(edge_site.tag + vector))
                builder[new_site] = builder.H[edge_site][1] #[1] for the onsite potential
                
                # Adding hoppings between the new site and his neighbors
                hopping_vectors = neighbor_vectors - {vector}

                for hopping_vector in hopping_vectors:
                    hopping_site = lat(*(new_site.tag + hopping_vector))
                    
                    if hopping_site in builder.H.keys():
                        hopping_site_ref = lat(*(edge_site.tag + hopping_vector))
                        if (edge_site, hopping_site_ref) in hoppings.keys():
                            builder[new_site, hopping_site] = hoppings[(edge_site, hopping_site_ref)]
                            hoppings[new_site, hopping_site] = hoppings[(edge_site, hopping_site_ref)]
                        elif (hopping_site_ref, edge_site) in hoppings.keys():
                            builder[hopping_site, new_site] = hoppings[(hopping_site_ref, edge_site)]
                            hoppings[hopping_site, new_site] = hoppings[(hopping_site_ref, edge_site)]
    
    return builder
    

def extend_data(coord_large, coord_small, data_small=None):
    """
    coord small should form a rectangle or cube
    coord small and data_small should have the same length
    
    Data returned according to the ordering of coord_large
    """    
    if data_small is None:
        data_small = np.arange(len(coord_small))[:,None]
    elif len(data_small.shape) == 1:
        data_small = data_small.transpose()
    
    data_large = np.empty((len(coord_large), *data_small.shape[1:]), dtype=data_small.dtype)
    
    data_dict = dict(zip([tuple(elem) for elem in coord_small], data_small))   
    
    bbox_small = [np.min(coord_small, axis=0), 
                  np.max(coord_small, axis=0)]
        
    for index, coord in enumerate(coord_large):
        coord = tuple(coord)
        if coord in data_dict.keys():
            data_large[index] = data_dict[coord]
        else:
            projection = []
            for ax, ax_val in enumerate(coord):
                if ax_val < bbox_small[0][ax]:
                    projection.append(bbox_small[0][ax])
                elif ax_val > bbox_small[1][ax]:
                    projection.append(bbox_small[1][ax])
                else:
                    projection.append(ax_val)
            data_large[index] = data_dict[tuple(projection)]
            
    return data_large


def conversion_index(pp_problem, kwant_builder, kpm_builder, extra_dim=[], 
                       coord_extra_dim=[]):
    """Conversion indices for a kpm builder that extends beyond the pp_problem.
    
    The kwant_builder should be rectangular in shape. 
    """
    coord_pp = pp_problem.mesh_inst.coordinates()
    coord_kwant = np.array([site.pos for site in kwant_builder.sites()])
    coord_kpm = [site.pos for site in kpm_builder.sites()]
    
    coord_kwant_ext = np.insert(coord_kwant, extra_dim, coord_extra_dim, axis=1)
    
    # Boolean index to get the coordinates of the kpm_system that are not
    # within the poisson problem
    print(coord_pp.shape, coord_kwant_ext.shape)
    indices_kwant = indices_from_coordinates(coord_pp, coord_kwant_ext)
    
    indices_kpm = extend_data(coord_kpm, coord_kwant, indices_kwant)
    
    return dict(zip(coord_kpm, indices_kpm))
    

class PescadoKwantProblem:
    
    def __init__(self, poisson_problem, kwant_builder, conversion_unit, 
                 conversion_index=None, kpm_builder=None, kpm_extension='pes', 
                 file = None, extra_dim=[], coord_extra_dim=[]):
        """
        file: str
            if save_to_file is True:
                A pickled file containing on the first index p_geom
                
        The expanded kwant system, kpm_sys, should be a sub element of
        the pescado builder.
        
        The kwant_sys is a sub element of the kpm_sys
        
        The potential mentioned in different methods of the PescadoKwantProblem
        is defined as: voltage*e/t with e the elementary charge (>0) and t 
        the energy unit in kwant (normalisation of the energy)
        
        kwant_builder: 
            All the sites in the kwant_builder should be within the flexible
            sites of the poisson problem
            
        poisson_problem: 
            Should only be provided on rank 0 if mpi parallelisation is used
            
        conversion_unit : float
            Provides the conversion from Pescado voltages to Kwant energy
            (kwant_energy = pescado_voltage * conversion_unit).
            e.g. when the kwant energy is normalized with respect to `t` 
            this would be equal to scipy.constants.elementary_charge/t.
            
        conversion_index : dict, optional
            As keys all the kpm coordinates in tinyarray format.
            As values the corresponding pescado indices.
            Only needs to be provided when a kpm_builder is provided and 
            the kpm builder extends beyond the boundaries of the 
            poisson_builder.
        
        extra_dim: list of int
            Integers representing the dimensions that need to be added in
            the kwant_builder: 0=x, 1=y, ...
        
        coord_extra_dim: list of int/float
            Value(s) that the kwant_builder coordinates must get in the 
            extra dimension(s). 
            NOTE: This could be expanded to a list of functions which
            take the initial coordinates as variable. e.g. to create
            the possibility of inserting a diagonal plane in a 3D device
            
        requirement:
            All flexible pescado sites have to be contained within the region 
            covered by the data expansion of the kwant system
            
            The kwant system should be rectangular or cubic to be able to have
            a well defined data expansion (radial expansion with given center, 
            could be implemented, but will be slower)
            
            The kpm_system should cover all flexible sites.
            
        NOTES:
            only create kwant system when necessary? e.g. when fsc calculation
            is started -> add decorator on some functions that checks whether
            the kwant sys is calculated and does it otherwise with the provided
            potential -> would only provide very little improvement since for
            pk_problem.redext() the kwant system is necessary as well
            
            Renaming of the kwant_sys to transport_sys? In fact the kpm_sys and
            kwant_sys are both kwant systems
        """
        # Coordinates are usefull for later data analysis
        coord = poisson_problem.mesh_inst.coordinates()
        
        # The coordinates of the flexible sites in the dimension of the 
        # kwant/kpm system
        coord_flex_limited = [ta.array(elem) for elem in 
                              np.delete(coord[poisson_problem.flexible_indices], 
                                        extra_dim, axis=1)]
        
        # kpm and kwant indices dictionaries for easy interfacing between
        # kwant and pescado. One could limit the conversion indices to only
        # the kpm sites, but in that case one needs to perform a search
        # operation on all the kpm coord within the pescado coord
        if conversion_index is None:
            if poisson_problem._kwant_indices is not None:
                conversion_index = poisson_problem._kwant_indices()
                conversion_index = {site.pos : value for site, value in 
                                    conversion_index.items()}
            else:
                conversion_index = dict(zip(coord_flex_limited, 
                                      poisson_problem.flexible_indices))
    
        # Reduced poisson problem to save memory
        rpp_problem = ReducedPoissonProblem(poisson_problem)
        del poisson_problem
        
        # Create the kpm system:
        if kpm_builder is None:
            kpm_builder = deepcopy(kwant_builder)
            # By expanding the kwant sys with n layers
            if isinstance(kpm_extension, int):
                kpm_builder = extend_sys_nb(kpm_builder, kpm_extension)
            # By expanding the kwant sys over all flexible sites
            else:
                kpm_builder = extend_sys_coord(kpm_builder, coord_flex_limited)
        
        kwant_sys = kwant_builder.finalized()
        kpm_sys = kpm_builder.finalized()
        
        self.conversion_unit = conversion_unit
        # Depleted only contains the depleted sites in the kwant_sys, not the 
        # depleted sites in the kpm_sys
        self.depleted = set()
        self.coord = coord
        self.conversion_index = conversion_index
        self.rpp_problem = rpp_problem
        self.kwant_builder = kwant_builder
        self.kwant_sys = kwant_sys
        self.kpm_builder = kpm_builder        
        self.kpm_sys = kpm_sys
        
        # Saving the pk_problem to an external file
        if file is not None:
            with open(file, 'rb') as inp:
                inp = pickle.load(inp)
            if isinstance(inp, (tuple, list)):
                params_geom = inp[0]
            else:
                params_geom = inp
            with open(file, 'wb') as outp:
                pickle.dump([params_geom, self], outp)
    
    
    def kpm_indices(self, coordinates, tol=1e-12):
        coord_kpm = np.array([site.pos for site in self.kpm_sys.sites])
        return indices_from_coordinates(coord_kpm, coordinates, tol)
    
    
    def kwant_indices(self, coordinates, tol=1e-12):
        coord_kwant = np.array([site.pos for site in self.kwant_sys.sites])
        return indices_from_coordinates(coord_kwant, coordinates, tol)

    
    def pes_indices(self, coordinates, tol=1e-12):
        return indices_from_coordinates(self.coord, coordinates, tol)
    
    
    @timing
    def reduce_system(self, potential, threshold=-8):
        """
        Potential is a sparse vector from pescado in kwant energy units
        Threshold is a float in kwant energy units
    
        You may not remove sites connected to the leads!
        If there is a full cut through the system (e.g. leads separated from 
        each other) the kwant/kpm system can still be build, but e.g. the 
        conductance will go to zero (Kwant still calculates this without error)
        
        Threshold of -8 since this is the band width expressed in units of t 
        for 2D systems
        
        Afwegen tussen het kwant systeem reducen na elke spannings berekening
        (tijd besparen op berekening transport properties, ldos) en de tijd
        die het neemt om individuele kwant systemen te creëren elke keer. 
        
        Reducing the system is usefull to avoid the kwant spectrum to increase
        too much when using kpm!
        
        Not checked if the transport properties also remain the same!!
        e.g. conductance

        """
        # Reset the depleted sites before calculating the new ones
        self.depleted = set()
        
        kpm_builder = deepcopy(self.kpm_builder)
        kwant_builder = deepcopy(self.kwant_builder)
        
        for site in list(kpm_builder.sites()):
            if potential[self.conversion_index[site.pos]] < threshold:
                del kpm_builder[site]
                if site in kwant_builder.sites():
                    # self.depleted only needs to keep track of the depleted 
                    # kwant sites since it will be used to reconstruct data
                    # from a reduced kwant system to the original kwant system
                    self.depleted.add(self.conversion_index[site.pos])
                    del kwant_builder[site]
            
        self.kwant_sys = kwant_builder.finalized()
        self.kpm_sys = kpm_builder.finalized()
    
    
    @timing
    def exact_ldos(self, potential=None, num_moments=100, where=None, 
                   params={}):
        """Calculate ExactLdos.
        
        Returns by default the bulk spectral density calculated for a flat 
        potential and for the location [0,0]. 
        Any other vector factory can be constructed by using the 'where' 
        argument.
        """
        if potential is None:
            potential = SparseVector(default=0)
        
        params.update({'potential': potential, 
                       'conversion_index' : self.conversion_index})
        
        if where is None:
            where = lambda s: s.pos == ta.array([0,0])
        vector_factory = kwant.kpm.LocalVectors(self.kpm_sys, where)
        
        ldos = kwant.kpm.SpectralDensity(self.kpm_sys, 
                                          num_vectors=None, 
                                          num_moments=num_moments,
                                          vector_factory=vector_factory,
                                          mean=False,
                                          accumulate_vectors=False,
                                          params=params)
        
        # Determine the pescado indices corresponding to the kwant sites for
        # which we calculated the ldos
        
        coord_ldos = [self.kpm_sys.sites[orb].pos for orb in vector_factory.orbs]
        indices_ldos = [self.conversion_index[elem] for elem in coord_ldos]
        
        ldos = ExactLdos(ldos, indices=indices_ldos, 
                         potential=potential[indices_ldos], 
                         conversion_unit=self.conversion_unit)
        return ldos
        
    
    def _lat_ildos_dos_continuous(self, num_moments=100, temp=0, 
        center=ta.array([0,0]), params={}, return_info=False):
        """
        Returns the quantum functional, which is a function that 
        returns the (origin, slope) for a given functional point (mu).
        The (origin, slope) defines the tangent around the functional point,
        respecting the equation :

                charge = origin + slope * potential
        
        This would be needed by a Newton Raphson solver or the ildos
        discretizer in pescado
        """
        # Calculate the ldos at the center of the 2DEG
        ldos = self.exact_ldos(num_moments=num_moments, params=params)
        
        # The top/bottom of the energy spectrum can be obtained from the ldos
        # by using the internal rescaling parameters (kwant units)
        # The ldos function is defined in this interval
        bottom_band =  -ldos.a + ldos.b
        top_band =  ldos.a + ldos.b

        def ildos_dos(mu):
            # Convert the mu from pescado to kwant units
            mu *= self.conversion_unit
            
            if mu <= bottom_band:
                slope = 0
                charge = 0
            # In the case the energy is higher than the top of the band,
            # the discretization is not fine enough.
            elif mu >= top_band:
                slope = 0
                charge = 2 # spin degeneracy
            else:
                charge = ldos.integrate(energy=mu) * 2 # 2 for spin degeneracy
                slope = ldos(mu) * 2 # 2 for spin degeneracy
            
            origin = charge - slope * mu
            
            # The origin is the same in kwant as in pescado units 
            # since it's a charge per site. The slope has to be converted 
            # from kwant to pescado units.            
            return origin, slope * self.conversion_unit
        
        if return_info:
            # Limits of the bands/ildos in pescado units        
            mu_limits = [bottom_band/ self.conversion_unit, 
                         top_band/ self.conversion_unit]
            
            return ildos_dos, mu_limits
        return ildos_dos
    
    
    @timing
    def solve_fsc_nr(self,  voltage={}, charge_electrons={}, charge_density={},
                 helmholtz_coef={}, initial_potential={}, center=None, 
                 num_moments=100, iterations=1, 
                 params={}, save_iteration=True):
        
        poisson_input = {'voltage' : voltage, 
                         'charge_electrons' : charge_electrons, 
                         'charge_density' : charge_density, 
                         'helmholtz_coef' : helmholtz_coef}
        
        ldos = self.exact_ldos(num_moments=num_moments, 
                               where = lambda s: s.pos == center, 
                               params=params)
                
        flexible_index = self.rpp_problem.linear_problem_inst.regions_index[3]
        sites_ildos = SparseVector(indices = flexible_index, 
                                   values = np.zeros(len(flexible_index)))
        flexible_index = set(flexible_index)
        
        voltage_res, charge_res = self.rpp_problem.solve_sc_continuous(
            initial_potential = initial_potential,
            quantum_functional = [ldos.quantum_functional_lat], 
            sites_ildos = sites_ildos, 
            **poisson_input)
                
        if save_iteration:
            voltage_vec, charge_vec, ldos_vec = (
                    [voltage_res], [charge_res], [ldos])
        
        for iteration in range(iterations):
            # Calculation of the potential in kwant energy units
            potential = voltage_res * self.conversion_unit 
        
            # Based on this potential we reduce the kpm/kwant system
            self.reduce_system(potential, threshold=-4)
        
            # With the reduced system we calculate the ldos using kpm       
            ldos = self.exact_ldos(potential = potential,
                                   num_moments = num_moments, 
                                   where = self.kwant_sys.sites, 
                                   params = params)
            
            # Set the depleted sites to Neumann, by default the charge
            # is zero so we should not set it in the poisson_input
            helmholtz_subtype_index = list(flexible_index - self.depleted)
            self.rpp_problem.assign_flexible(
                neumann_index=list(self.depleted), 
                helmholtz_index=helmholtz_subtype_index)
            self.rpp_problem.freeze()
            
            # Adapt sites_ildos to the possible Neumann sites.
            sites_ildos = SparseVector(
                indices = helmholtz_subtype_index, 
                values = np.zeros(len(helmholtz_subtype_index)))
                        
            # As initial potential we can use voltage_res since it is always
            # defined on all flexible sites
            voltage_res, charge_res = self.rpp_problem.solve_sc_continuous(
                    initial_potential = voltage_res,
                    quantum_functional = [ldos.quantum_functional_kwant],
                    sites_ildos = sites_ildos, 
                    **poisson_input)
        
            if save_iteration:
                voltage_vec.append(voltage_res)
                charge_vec.append(charge_res)
                ldos_vec.append(ldos)
        
        if save_iteration:
            return voltage_vec, charge_vec, ldos_vec
        return voltage_res, charge_res, ldos


    @timing
    def conductance(self, potential, energy = [0], params={}):
        """
        potential = list of sparse vectors or sparse vector   
            
            Potential inside the 2DEG, as should be included in the discretized 
            hamiltonian, with energy respective to [t]
        
        energy: int, float or list 
            
            Energies for which the conductance needs to be calculated
        """
        
        if isinstance(potential, SparseVector):
            potential = [potential]
            
        if isinstance(energy, (float, int)):
            energy = [energy]
        
        conductance = np.empty((len(potential), len(energy)))
        
        for index_p, potential_p in enumerate(potential):
            
            params.update({'potential':potential_p, 
                           'conversion_index' : self.conversion_index})
            
            for index_e, energy_e in enumerate(energy):
                smatrix = kwant.smatrix(self.kwant_sys, energy_e, 
                                        params = params)
                conductance[index_p, index_e] = smatrix.transmission(0, 1)
        return conductance
    
    
    @timing
    def ldos_kwant(self, potential, energy = [0], params={}):
        """
        Parameters
        ----------
        potential: SparseVector or list of SparseVectors   
            Potential inside the 2DEG, as should be included in the discretized 
            hamiltonian, with energy respective to [t]
        
        energy: float or list of floats
            Energies for which the ldos needs to be calculated
            
        Calculated over the kwant system since kwant otherwise protests that
        kpm_sys has no leads
        """
    
        if isinstance(potential, SparseVector):
            potential = [potential]
            
        if isinstance(energy, (float, int)):
            energy = [energy]
        
        ldos = np.empty((len(potential), len(energy), 
                         len(self.kwant_sys.sites)))
        
        for index_p, potential_p in enumerate(potential):
            params.update({'potential':potential_p, 
                           'conversion_index' : self.conversion_index})
        
            for index_e, energy_e in enumerate(energy):
                ldos[index_p, index_e] = kwant.ldos(self.kwant_sys , energy_e, 
                                                    params = params)
        return ldos
    
    
    @timing
    def density_tkwant(self, potential, chemical_potential=0, temp=0, 
                       rtol=1e-3, atol=1e-3, calc_error = True, params={}):
        """
        params is an extra dictionary with input parameters to pass to 
        the kwant_sys
        """
        import tkwant

        params.update({'potential':potential, 
                       'conversion_index' : self.conversion_index})

        density_operator = kwant.operator.Density(self.kwant_syst)
        occupations = tkwant.manybody.lead_occupation(
                chemical_potential=chemical_potential, temp=temp)
        state = tkwant.manybody.State(self.kwant_sys, tmax=1, 
                occupations=occupations, params=params)
        state.refine_intervals(rtol=rtol, atol=atol)
        density = state.evaluate(density_operator)

        if calc_error:
            error = state.estimate_error()
            print('Density at T=', temp, 'K: ', density, ' with error ', error)
            return density, error
        else:
            print('Density at T=', temp, 'K: ', density)
            return density
    
    
    def get_relevant_energy(self, potential, nb_energies=100, margin=1.1, 
                            lead=0, params={}):
        
        """
        Get the extrema of the band structure using the kwantspectrum package
        and return an energy spectrum (array)
        """
        import kwantspectrum
        
        params.update({'potential':potential, 
                       'conversion_index' : self.conversion_index})
        
        # Finding the top and bottom of the band_structure
        spec = kwantspectrum.spectrum(self.kwant_sys.leads[lead], params=params)
        extrema = []
        for band in range(spec.nbands):
            velocity_zeros = spec.intersect(0, band, derivative_order=1)
            extrema.append(spec(velocity_zeros, band))
        
        E_max = np.max(extrema)
        E_min = np.min(extrema)
        
        # Adding the margin
        E_range = E_max - E_min
        E_mid = (E_max + E_min)/2
        E_min = E_mid - margin * E_range/2
        E_max = E_mid + margin * E_range/2
        
        return np.linspace(E_min, E_max, nb_energies)
    
    
    def reset(self):
        """
        Reset the PescadoKwantProblem to the state just after initialisation.
        """
        self.depleted = set()
        self.kwant_sys = self.kwant_builder.finalized()
        self.kpm_sys = self.kpm_builder.finalized()
        self.rpp_problem.reset()